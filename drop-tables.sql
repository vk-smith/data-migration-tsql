USE <EcpCustomer>;

ALTER TABLE [Authentication].[AuthToInfo] DROP CONSTRAINT [FK_AuthToInfo_Authentication];
ALTER TABLE [Authentication].[AuthToInfo] DROP CONSTRAINT [FK_AuthToInfo_AuthInfo];
ALTER TABLE [Customer].[AddressPhoneLookup] DROP CONSTRAINT [FK_Profile_AddressPhoneLookup];
ALTER TABLE [Customer].[MyLocation] DROP CONSTRAINT [FK_MyLocation_Profile];
ALTER TABLE [Customer].[TINData] DROP CONSTRAINT [FK_TINData_Profile];
ALTER TABLE [Notification].[Feedback] DROP CONSTRAINT [FK_Feedback_Profile];
ALTER TABLE [Customer].[Address] DROP CONSTRAINT [FK_Address_Profile];
ALTER TABLE [Customer].[CustomerLookup] DROP CONSTRAINT [FK_CustomerLookup_Profile];
ALTER TABLE [Customer].[CustomerPaymentMethod] DROP CONSTRAINT [FK_CustomerPaymentMethod_Profile];
ALTER TABLE [Customer].[AuthToken] DROP CONSTRAINT [FK_AuthToken_Profile];
ALTER TABLE [Customer].[LoyaltyMembershipNumber] DROP CONSTRAINT [FK_LoyaltyMembershipNumber_Profile];
ALTER TABLE [Customer].[Favorite] DROP CONSTRAINT [FK_Favorite_Profile];
ALTER TABLE [Customer].[SocialNetwork] DROP CONSTRAINT [FK_SocialNetwork_Profile];
ALTER TABLE [Customer].[Preference] DROP CONSTRAINT [FK_Preference_Profile];
ALTER TABLE [Customer].[MobileDevices] DROP CONSTRAINT [FK_MobileDevices_Profile];
ALTER TABLE [Customer].[DynamicOptin] DROP CONSTRAINT [FK_DynamicOptIn_Profile];
ALTER TABLE [Customer].[Optin] DROP CONSTRAINT [FK_OptIn_Profile];
ALTER TABLE [Customer].[Acceptance] DROP CONSTRAINT [FK_Acceptance_Profile];
ALTER TABLE [Customer].[Subscription] DROP CONSTRAINT [FK_Subscription_Profile];

DROP TABLE [Customer].[MyLocation];
DROP TABLE [Customer].[MobileDevices];
DROP TABLE [Customer].[DynamicOptin];
DROP TABLE [Customer].[Acceptance];
DROP TABLE [Customer].[Subscription] 
DROP TABLE [Customer].[Preference];
DROP TABLE [Customer].[CustomerPaymentMethod];
DROP TABLE [Customer].[Favorite];
DROP TABLE [Customer].[AuthToken];
DROP TABLE [Customer].[Address];
DROP TABLE [Customer].[TINData];
DROP TABLE [Customer].[Profile];
DROP TABLE [Customer].[Optin];
DROP TABLE [Notification].[Feedback];
DROP TABLE [Customer].[SocialNetwork];
DROP TABLE [Customer].[SocialNetworkAccessToken]; 
DROP TABLE [Customer].[LoyaltyMembershipNumber];
DROP TABLE [Authentication].[Authentication];
DROP TABLE [Authentication].[AuthInfo];



USE <EcpOrderDB>
DROP TABLE [OrderTaking].[SerializedData]; 