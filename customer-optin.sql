DECLARE @Start_CustomerOptinID int
		,@SizeOfChunk int = 1000000 -- the number of rows to be copied in one transaction

IF NOT EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Optin_tmp]') AND [type]='U')
Begin
 print 'Create table [Customer].[Optin_tmp]'
CREATE TABLE [Customer].[Optin_tmp](
	[CustomerOptinID] [int] IDENTITY(1,1) NOT NULL,
    [CustomerID] [int] NOT NULL,
    [OptinType] [varchar](50) NULL,
    [OptinFlag] [bit] NULL,
    [LastModification] [datetime] NOT NULL,
    [SerializedEncryptedValues] [varchar](2000) NULL,
 CONSTRAINT [PK_Optin_tmp] PRIMARY KEY CLUSTERED 
(
	[CustomerOptinID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [fg1]
)


End

-- find out the last copied row:
SET @Start_CustomerOptinID = (SELECT ISNULL(MAX(CustomerOptinID), 0) FROM [Customer].[Optin_tmp]);


-- copying rows ordered by [CustomerOptinID]:
WHILE @Start_CustomerOptinID < (SELECT MAX(CustomerOptinID) FROM [Customer].[Optin])
BEGIN

Print CONVERT(char(20), GETDATE(), 120) + 'insert into [Customer].[Optin_tmp] ' + CAST(@SizeOfChunk AS varchar(11)) + ' rows starting from [CustomerOptinID] = ' + CAST(@Start_CustomerOptinID AS varchar(11));
set identity_insert [Customer].[Optin_tmp] ON
begin tran t1
insert into [Customer].[Optin_tmp]
([CustomerOptinID]
      ,[CustomerID]
      ,[OptinType]
      ,[OptinFlag]
      ,[LastModification]
      ,[SerializedEncryptedValues])
SELECT [CustomerOptinID]
      ,[CustomerID]
      ,[OptinType]
      ,[OptinFlag]
      ,[LastModification]
      ,[SerializedEncryptedValues]
  FROM [Customer].[Optin]
 WHERE [CustomerOptinID] > @Start_CustomerOptinID
  ORDER BY [CustomerOptinID] 
  OFFSET 0 ROWS FETCH NEXT @SizeOfChunk ROWS ONLY
commit tran t1
set identity_insert [Customer].[Optin_tmp] OFF
SET @Start_CustomerOptinID = (SELECT MAX([CustomerOptinID]) FROM [Customer].[Optin_tmp])
END;


ALTER TABLE [Customer].[OptIn_tmp] ADD  CONSTRAINT [DF_OptIn_tmp_LastModification]  DEFAULT (getutcdate()) FOR [LastModification]


Print CONVERT(char(20), GETDATE(), 120) + 'Create index [IX_OptIn_CustomerID] ON [Customer].[Optin_tmp]'
CREATE NONCLUSTERED INDEX [IX_OptIn_CustomerID] ON [Customer].[Optin_tmp]
(
	[CustomerID] ASC
)
INCLUDE ( 	[CustomerOptinID],
	[OptInType],
	[OptinFlag],
	[LastModification],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) 
--	ON [Customer_Index]
Print CONVERT(char(20), GETDATE(), 120) + 'Index [IX_OptIn_CustomerID] ON [Customer].[Optin_tmp] created'


Print CONVERT(char(20), GETDATE(), 120) + 'Create index [IX_OptIn_LastModification_CustomerID] ON [Customer].[Optin_tmp]'
CREATE NONCLUSTERED INDEX [IX_OptIn_LastModification_CustomerID] ON [Customer].[Optin_tmp]
(
	[LastModification] ASC,
	[CustomerID] ASC
)
INCLUDE ( 	[CustomerOptinID],
	[OptInType],
	[OptinFlag],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) 
--	ON [Customer_Index]
Print CONVERT(char(20), GETDATE(), 120) + 'Index [IX_OptIn_LastModification_CustomerID] ON [Customer].[Optin_tmp] created'

