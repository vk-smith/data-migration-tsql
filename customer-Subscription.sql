DECLARE @Start_CustomerSubscriptionID int
		,@SizeOfChunk int = 1000000 -- the number of rows to be copied in one transaction

IF NOT EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Subscription_tmp]') AND [type]='U')
Begin
 print 'Create table [Customer].[Subscription_tmp]'
CREATE TABLE [Customer].[Subscription_tmp](
	[CustomerSubscriptionID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[SubscriptionTypeID] [tinyint] NULL,
	[Description] [nvarchar](max) NULL,
	[SubscribedAt] [datetime] NULL DEFAULT (getutcdate()),
	[SubscriptionFlag] [bit] NULL,
	[LastModification] [datetime] NOT NULL DEFAULT (getutcdate()),
	[SerializedEncryptedValues] [varchar](2000) NULL,
 CONSTRAINT [PK_Subscription_tmp] PRIMARY KEY CLUSTERED 
(
	[CustomerSubscriptionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 


End

-- find out the last copied row:
SET @Start_CustomerSubscriptionID = (SELECT ISNULL(MAX(CustomerSubscriptionID), 0) FROM [Customer].[Subscription_tmp]);


-- copying rows ordered by [CustomerSubscriptionID]:
WHILE @Start_CustomerSubscriptionID < (SELECT MAX(CustomerSubscriptionID) FROM [Customer].[Subscription])
BEGIN

Print CONVERT(char(20), GETDATE(), 120) + 'insert into [Customer].[Subscription_tmp] ' + CAST(@SizeOfChunk AS varchar(11)) + ' rows starting from [CustomerSubscriptionID] = ' + CAST(@Start_CustomerSubscriptionID AS varchar(11));
set identity_insert [Customer].[Subscription_tmp] ON
begin tran t1
insert into [Customer].[Subscription_tmp]
([CustomerSubscriptionID]
      ,[CustomerID]
      ,[SubscriptionTypeID]
      ,[Description]
      ,[SubscribedAt]
      ,[SubscriptionFlag]
      ,[LastModification]
      ,[SerializedEncryptedValues])
SELECT [CustomerSubscriptionID]
      ,[CustomerID]
      ,[SubscriptionTypeID]
      ,[Description]
      ,[SubscribedAt]
      ,[SubscriptionFlag]
      ,[LastModification]
      ,[SerializedEncryptedValues]
  FROM [Customer].[Subscription]
 WHERE [CustomerSubscriptionID] > @Start_CustomerSubscriptionID
  ORDER BY [CustomerSubscriptionID] 
  OFFSET 0 ROWS FETCH NEXT @SizeOfChunk ROWS ONLY
commit tran t1
set identity_insert [Customer].[Subscription_tmp] OFF
SET @Start_CustomerSubscriptionID = (SELECT MAX(CustomerSubscriptionID) FROM [Customer].[Subscription_tmp])
END;



Print CONVERT(char(20), GETDATE(), 120) + 'Create Index [IX_Subscription_CustomerID] ON [Customer].[Subscription_tmp]'
CREATE NONCLUSTERED INDEX [IX_Subscription_CustomerID] ON [Customer].[Subscription_tmp]
(
	[CustomerID] ASC
)
INCLUDE ( 	[CustomerSubscriptionID],
	[SubscriptionTypeID],
	[Description],
	[SubscribedAt],
	[SubscriptionFlag],
	[LastModification],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	on Customer_Index
Print CONVERT(char(20), GETDATE(), 120) + 'Index [IX_Subscription_CustomerID] ON [Customer].[Subscription_tmp] created'


Print CONVERT(char(20), GETDATE(), 120) + 'Create Index IX_Subscription_LastModification_CustomerID ON [Customer].[Subscription_tmp]'
	CREATE NONCLUSTERED INDEX [IX_Subscription_LastModification_CustomerID] ON [Customer].[Subscription_tmp]
	(
		[LastModification] ASC,
		[CustomerID] ASC
	)
	INCLUDE ( 	[CustomerSubscriptionID],
		[SubscriptionTypeID],
		[Description],
		[SubscribedAt],
		[SubscriptionFlag],
		[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) 
--	ON [Customer_Index]
Print CONVERT(char(20), GETDATE(), 120) + 'Index IX_Subscription_LastModification_CustomerID ON [Customer].[Subscription_tmp] created'