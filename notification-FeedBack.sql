DECLARE @Start_FeedbackId int
		,@SizeOfChunk int = 1000000 -- the number of rows to be copied in one transaction

IF NOT EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Notification].[Feedback_tmp]') AND [type]='U')
Begin
 print 'Create table [Notification].[Feedback_tmp]'
CREATE TABLE [Notification].[Feedback_tmp](
	[FeedbackId] [int] IDENTITY(1,1) NOT NULL,
	[FeedbackTypeID] [tinyint] NULL,
	[CustomerId] [int] NOT NULL,
	[Rating] [tinyint] NULL,
	[Comments] [nvarchar](500) NULL,
	[Received] [datetime] NULL,
	[SendDate] [datetime] NULL,
	[LastModification] [datetime] NOT NULL,
	[SerializedEncryptedValues] [varchar](2000) NULL,
 CONSTRAINT [PK_Feedback_tmp] PRIMARY KEY CLUSTERED 
(
	[FeedbackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [FG1]
)


End

-- find out the last copied row:
SET @Start_FeedbackId = (SELECT ISNULL(MAX(FeedbackId), 0) FROM [Notification].[Feedback_tmp]);


-- copying rows ordered by [FeedbackId]:
WHILE @Start_FeedbackId < (SELECT MAX(FeedbackId) FROM [Notification].[Feedback])
BEGIN

Print CONVERT(char(20), GETDATE(), 120) + 'insert into [Notification].[Feedback_tmp] ' + CAST(@SizeOfChunk AS varchar(11)) + ' rows starting from [FeedbackId] = ' + CAST(@Start_FeedbackId AS varchar(11));
set identity_insert [Notification].[Feedback_tmp] ON
begin tran t1
INSERT INTO [Notification].[Feedback_tmp]
           (FeedbackId
		   ,[FeedbackTypeID]
           ,[CustomerId]
           ,[Rating]
           ,[Comments]
           ,[Received]
           ,[SendDate]
           ,[LastModification]
           ,[SerializedEncryptedValues])
SELECT 		   
           FeedbackId
		   ,[FeedbackTypeID]
           ,[CustomerId]
           ,[Rating]
           ,[Comments]
           ,[Received]
           ,[SendDate]
           ,[LastModification]
           ,[SerializedEncryptedValues]
FROM [Notification].[Feedback]	
 WHERE [FeedbackId] > @Start_FeedbackId
  ORDER BY [FeedbackId] 
  OFFSET 0 ROWS FETCH NEXT @SizeOfChunk ROWS ONLY
commit tran t1
set identity_insert [Notification].[Feedback_tmp] OFF
SET @Start_FeedbackId = (SELECT MAX(FeedbackId) FROM [Notification].[Feedback_tmp])
END;

ALTER TABLE [Notification].[Feedback_tmp] ADD  CONSTRAINT [DF_Feedback_tmp_LastModification]  DEFAULT (getutcdate()) FOR [LastModification]