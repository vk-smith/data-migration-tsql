DECLARE @Start_SocialNetworkID nvarchar(128)
		,@Start_InternalID nvarchar(256)
		,@SizeOfChunk int = 1000000 -- the number of rows to be copied in one transaction

IF NOT EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[SocialNetworkAccessToken_tmp]') AND [type]='U')
Begin
 print 'Create table [Customer].[SocialNetworkAccessToken_tmp]'
CREATE TABLE [Customer].[SocialNetworkAccessToken_tmp](
	[SocialNetworkID] [nvarchar](128) NOT NULL,
	[InternalID] [nvarchar](256) NOT NULL,
	[AccessToken] [nvarchar](512) NULL,
	[RefreshToken] [nvarchar](512) NULL,
	[LastModification] [datetime] NULL,
	[SerializedEncryptedValues] [varchar](max) NULL,
 CONSTRAINT [PK_SocialNetworkAccessToken_tmp] PRIMARY KEY NONCLUSTERED 
(
	[SocialNetworkID] ASC,
	[InternalID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [FG1]
) ON [FG1] TEXTIMAGE_ON [FG1]


End

-- find out the last copied row:
SET @Start_SocialNetworkID = (SELECT ISNULL(MAX([SocialNetworkID]), 0) FROM [Customer].[SocialNetworkAccessToken_tmp]);
SET @Start_InternalID = (SELECT  ISNULL(MAX([InternalID]), '') FROM [Customer].[SocialNetworkAccessToken_tmp] WHERE [SocialNetworkID] = @Start_SocialNetworkID)


-- copying rows ordered by [SocialNetworkID] and [InternalID]:
WHILE @Start_SocialNetworkID < (SELECT MAX([SocialNetworkID]) FROM [Customer].[SocialNetworkAccessToken]) 
		OR (@Start_SocialNetworkID = (SELECT MAX([SocialNetworkID]) FROM [Customer].[SocialNetworkAccessToken]) AND @Start_InternalID < (SELECT MAX([InternalID]) FROM [Customer].[SocialNetworkAccessToken] WHERE [SocialNetworkID] = @Start_SocialNetworkID))

BEGIN

Print CONVERT(char(20), GETDATE(), 120) + 'insert into [Customer].[SocialNetworkAccessToken_tmp] ' + CAST(@SizeOfChunk AS varchar(11)) + ' rows starting from [SocialNetworkID] = ' + @Start_SocialNetworkID;
begin tran t1
INSERT INTO [Customer].[SocialNetworkAccessToken_tmp]
           ([SocialNetworkID]
           ,[InternalID]
           ,[AccessToken]
           ,[RefreshToken]
           ,[LastModification]
           ,[SerializedEncryptedValues])
SELECT
           [SocialNetworkID]
           ,[InternalID]
           ,[AccessToken]
           ,[RefreshToken]
           ,[LastModification]
           ,[SerializedEncryptedValues]
FROM [Customer].[SocialNetworkAccessToken]		
 WHERE [SocialNetworkID] > @Start_SocialNetworkID
	OR ([SocialNetworkID] = @Start_SocialNetworkID AND [InternalID] > @Start_InternalID)
  ORDER BY [SocialNetworkID], [InternalID] 
  OFFSET 0 ROWS FETCH NEXT @SizeOfChunk ROWS ONLY
commit tran t1

SET @Start_SocialNetworkID = (SELECT MAX([SocialNetworkID]) FROM [Customer].[SocialNetworkAccessToken_tmp]);
SET @Start_InternalID = (SELECT  MAX([InternalID]) FROM [Customer].[SocialNetworkAccessToken_tmp] WHERE [SocialNetworkID] = @Start_SocialNetworkID);

END;

