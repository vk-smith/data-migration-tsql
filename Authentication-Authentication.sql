/*
Source table:
USE [perf8EcpCustomer]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [Authentication].[Authentication](
	[Token] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastAuthenticationTime] [datetime] NULL,
	[LoginSourceType] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AuthenticationRetries] [int] NOT NULL,
	[SerializedEncryptedValues] [nvarchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MarketName] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Authentication] PRIMARY KEY CLUSTERED 
(
	[Token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [FG1]
) ON [FG1]

SET ANSI_PADDING ON

CREATE UNIQUE NONCLUSTERED INDEX [IX_Authentication_UserName_MarketName] ON [Authentication].[Authentication]
(
	[UserName] ASC,
	[MarketName] ASC
)
INCLUDE ( 	[LastAuthenticationTime],
	[LoginSourceType],
	[AuthenticationRetries],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [FG1]
ALTER TABLE [Authentication].[Authentication] ADD  CONSTRAINT [DF_Authentication_LoginSourceType]  DEFAULT ('0') FOR [LoginSourceType]

*/
DECLARE @Start_Token uniqueidentifier
		,@SizeOfChunk int = 1000000 -- the number of rows to be copied in one transaction

IF NOT EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Authentication].[Authentication_tmp]') AND [type]='U')
Begin
 print 'Create table [Authentication].[Authentication_tmp]'
CREATE TABLE [Authentication].[Authentication_tmp](
	[Token] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](128) NULL,
	[LastAuthenticationTime] [datetime] NULL,
	[LoginSourceType] [nvarchar](50) NULL,
	[AuthenticationRetries] [int] NOT NULL,
	[SerializedEncryptedValues] [varchar](2000) NULL,
	[MarketName] [nvarchar](128) NULL,
 CONSTRAINT [PK_Authentication_tmp] PRIMARY KEY CLUSTERED 
(
	[Token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90)
/*,
 CONSTRAINT [UQ_User_Market_Name] UNIQUE NONCLUSTERED 
(
	[UserName] ASC,
	[MarketName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) 
-- ON [Authentication_index]*/
)

ALTER TABLE [Authentication].[Authentication_tmp] ADD  CONSTRAINT [DF_Authentication_tmp_LoginSourceType]  DEFAULT ('0') FOR [LoginSourceType]

End

-- find out the last copied row:
SET @Start_Token = (SELECT ISNULL(MAX(Token), '00000000-0000-0000-0000-000000000000') FROM [Authentication].[Authentication_tmp]);


-- copying rows ordered by [Token]:
WHILE @Start_Token < (SELECT MAX(Token) FROM [Authentication].[Authentication])
BEGIN

Print CONVERT(char(20), GETDATE(), 120) + 'insert into [Authentication].[Authentication_tmp] ' + CAST(@SizeOfChunk AS varchar(11)) + ' rows starting from [Token] = ' + CAST(@Start_Token AS varchar(36));

begin tran t1
INSERT INTO [Authentication].[Authentication_tmp]
           ([Token]
           ,[UserName]
           ,[LastAuthenticationTime]
           ,[LoginSourceType]
           ,[AuthenticationRetries]
           ,[SerializedEncryptedValues]
           ,[MarketName])
SELECT
           [Token]
           ,[UserName]
           ,[LastAuthenticationTime]
           ,[LoginSourceType]
           ,[AuthenticationRetries]
           ,[SerializedEncryptedValues]
           ,[MarketName]
FROM [Authentication].[Authentication]
 WHERE [Token] > @Start_Token
  ORDER BY [Token] 
  OFFSET 0 ROWS FETCH NEXT @SizeOfChunk ROWS ONLY
commit tran t1

SET @Start_Token = (SELECT MAX(Token) FROM [Authentication].[Authentication_tmp])
END;



Print CONVERT(char(20), GETDATE(), 120) + 'Create index [IX_Authentication_UserName_MarketName] ON [Authentication].[Authentication_tmp]'
CREATE NONCLUSTERED INDEX [IX_Authentication_UserName_MarketName] ON [Authentication].[Authentication_tmp]
(
	[UserName] ASC,
	[MarketName] ASC
)
INCLUDE 
( 	[LastAuthenticationTime],
	[LoginSourceType],
	[AuthenticationRetries],
	[SerializedEncryptedValues]
) 
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) 
--	ON [Authentication_Index]
Print CONVERT(char(20), GETDATE(), 120) + 'Index [IX_Authentication_UserName_MarketName] ON [Authentication].[Authentication_tmp] created'


Print CONVERT(char(20), GETDATE(), 120) + 'Create unique index [UQ_User_Market_Name] on [Authentication].[Authentication_tmp]'
ALTER TABLE [Authentication].[Authentication_tmp] ADD  CONSTRAINT [Authentication_tmp_UQ_User_Market_Name] UNIQUE NONCLUSTERED 
(
	[UserName] ASC,
	[MarketName] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) 
--	ON [Authentication_Index]
Print CONVERT(char(20), GETDATE(), 120) + 'Index UQ_User_Market_Name ON [Authentication].[Authentication_tmp] created'
