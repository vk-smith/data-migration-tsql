DECLARE @Start_CustomerSocialNetworkID int
		,@SizeOfChunk int = 1000000 -- the number of rows to be copied in one transaction

IF NOT EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[SocialNetwork_tmp]') AND [type]='U')
Begin
 print 'Create table [Customer].[SocialNetwork_tmp]'
CREATE TABLE [Customer].[SocialNetwork_tmp](
	[CustomerSocialNetworkID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[InternalID] [nvarchar](255) NULL,
	[SocialNetworkID] [int] NULL,
	[SerializedEncryptedValues] [varchar](2000) NULL,
 CONSTRAINT [PK_SocialNetwork_tmp] PRIMARY KEY CLUSTERED 
(
	[CustomerSocialNetworkID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90)
)


End

-- find out the last copied row:
SET @Start_CustomerSocialNetworkID = (SELECT ISNULL(MAX(CustomerSocialNetworkID), 0) FROM [Customer].[SocialNetwork_tmp]);


-- copying rows ordered by [CustomerSocialNetworkID]:
WHILE @Start_CustomerSocialNetworkID < (SELECT MAX(CustomerSocialNetworkID) FROM [Customer].[SocialNetwork])
BEGIN

Print CONVERT(char(20), GETDATE(), 120) + 'insert into [Customer].[SocialNetwork_tmp] ' + CAST(@SizeOfChunk AS varchar(11)) + ' rows starting from [CustomerSocialNetworkID] = ' + CAST(@Start_CustomerSocialNetworkID AS varchar(11));
set identity_insert [Customer].[SocialNetwork_tmp] ON
begin tran t1
INSERT INTO [Customer].[SocialNetwork_tmp]
           (CustomerSocialNetworkID
		   ,[CustomerID]
           ,[InternalID]
           ,[SocialNetworkID]
           ,[SerializedEncryptedValues])
SELECT
           CustomerSocialNetworkID
		   ,[CustomerID]
           ,[InternalID]
           ,[SocialNetworkID]
           ,[SerializedEncryptedValues]
FROM Customer.SocialNetwork
 WHERE [CustomerSocialNetworkID] > @Start_CustomerSocialNetworkID
  ORDER BY [CustomerSocialNetworkID] 
  OFFSET 0 ROWS FETCH NEXT @SizeOfChunk ROWS ONLY
commit tran t1
set identity_insert [Customer].[SocialNetwork_tmp] OFF
SET @Start_CustomerSocialNetworkID = (SELECT MAX(CustomerSocialNetworkID) FROM [Customer].[SocialNetwork_tmp])
END;



Print CONVERT(char(20), GETDATE(), 120) + 'Create index [IX_SocialNetwork_CustomerID] ON [Customer].[SocialNetwork_tmp]'
CREATE NONCLUSTERED INDEX [IX_SocialNetwork_CustomerID] ON [Customer].[SocialNetwork_tmp]
(
	[CustomerID] ASC
)
INCLUDE ( 	[CustomerSocialNetworkID],
	[InternalID],
	[SocialNetworkID],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = On, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) 
--	ON [Customer_Index]
Print CONVERT(char(20), GETDATE(), 120) + 'Index [IX_SocialNetwork_CustomerID] ON [Customer].[SocialNetwork_tmp] created'


Print CONVERT(char(20), GETDATE(), 120) + 'Create index [IX_SocialNetwork_InternalID_CustomerID] ON [Customer].[SocialNetwork_tmp]'
CREATE NONCLUSTERED INDEX [IX_SocialNetwork_InternalID_CustomerID] ON [Customer].[SocialNetwork_tmp]
(
	[InternalID] ASC,
	[CustomerID] ASC
)
INCLUDE ( 	[CustomerSocialNetworkID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = On, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) 
--	ON [Customer_Index]
Print CONVERT(char(20), GETDATE(), 120) + 'Index IX_SocialNetwork_InternalID_CustomerID ON [Customer].[SocialNetwork_tmp] created'
