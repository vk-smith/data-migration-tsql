DECLARE @Start_CustomerLoyaltyMembershipNumberID int
		,@SizeOfChunk int = 1000000 -- the number of rows to be copied in one transaction

IF NOT EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[LoyaltyMembershipNumber_tmp]') AND [type]='U')
Begin
 print 'Create table [Customer].[LoyaltyMembershipNumber_tmp]'
CREATE TABLE [Customer].[LoyaltyMembershipNumber_tmp](
	[CustomerLoyaltyMembershipNumberID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[LoyaltyMembershipNumber] [nvarchar](128) NOT NULL,
	[LoyaltyMembershipType] [nvarchar](16) NOT NULL,
	[LastModificationUTC] [datetime] NOT NULL,
	[SerializedEncryptedValues] [varchar](2000) NULL,
 CONSTRAINT [PK_LoyaltyMembershipNumber_tmp] PRIMARY KEY CLUSTERED 
(
	[CustomerLoyaltyMembershipNumberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [FG1]
)


End

-- find out the last copied row:
SET @Start_CustomerLoyaltyMembershipNumberID = (SELECT ISNULL(MAX(CustomerLoyaltyMembershipNumberID), 0) FROM [Customer].[LoyaltyMembershipNumber_tmp]);


-- copying rows ordered by [CustomerLoyaltyMembershipNumberID]:
WHILE @Start_CustomerLoyaltyMembershipNumberID < (SELECT MAX(CustomerLoyaltyMembershipNumberID) FROM [Customer].[LoyaltyMembershipNumber])
BEGIN

Print CONVERT(char(20), GETDATE(), 120) + 'insert into [Customer].[LoyaltyMembershipNumber_tmp] ' + CAST(@SizeOfChunk AS varchar(11)) + ' rows starting from [CustomerLoyaltyMembershipNumberID] = ' + CAST(@Start_CustomerLoyaltyMembershipNumberID AS varchar(11));
set identity_insert [Customer].[LoyaltyMembershipNumber_tmp] ON
begin tran t1
INSERT INTO [Customer].[LoyaltyMembershipNumber_tmp]
           (CustomerLoyaltyMembershipNumberID
		   ,[CustomerID]
           ,[LoyaltyMembershipNumber]
           ,[LoyaltyMembershipType]
           ,[LastModificationUTC]
           ,[SerializedEncryptedValues])
SELECT
           CustomerLoyaltyMembershipNumberID
		   ,CustomerID
           ,[LoyaltyMembershipNumber]
           ,[LoyaltyMembershipType]
           ,[LastModificationUTC]
           ,[SerializedEncryptedValues]
FROM [Customer].[LoyaltyMembershipNumber]
 WHERE [CustomerLoyaltyMembershipNumberID] > @Start_CustomerLoyaltyMembershipNumberID
  ORDER BY [CustomerLoyaltyMembershipNumberID] 
  OFFSET 0 ROWS FETCH NEXT @SizeOfChunk ROWS ONLY
commit tran t1
set identity_insert [Customer].[LoyaltyMembershipNumber_tmp] OFF
SET @Start_CustomerLoyaltyMembershipNumberID = (SELECT MAX(CustomerLoyaltyMembershipNumberID) FROM [Customer].[LoyaltyMembershipNumber_tmp])
END;

ALTER TABLE [Customer].[LoyaltyMembershipNumber_tmp] ADD  CONSTRAINT [DF_LoyaltyMembershipNumber_tmp_LastModificationUTC]  DEFAULT (getutcdate()) FOR [LastModificationUTC]
