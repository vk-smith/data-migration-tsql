DECLARE @Start_CustomerPreferenceID int
		,@SizeOfChunk int = 1000000 -- the number of rows to be copied in one transaction

IF NOT EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Preference_tmp]') AND [type]='U')
Begin
 print 'Create table [Customer].[Preference_tmp]'
CREATE TABLE [Customer].[Preference_tmp](
 [CustomerPreferenceID] [int] IDENTITY(1,1) NOT NULL,
 [CustomerID] [int] NOT NULL,
 [PreferenceTypeID] [tinyint] NULL,
 [NotificationMediumID] [tinyint] NULL,
 [PreferenceValue] [varchar](50) NULL,
 [PreferenceFlag] [bit] NULL,
 [LastModification] [datetime] NOT NULL ,
 [SerializedEncryptedValues] [varchar](3400) NULL,
 CONSTRAINT [PK_Preference_tmp] PRIMARY KEY CLUSTERED 
(
 [CustomerPreferenceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 


ALTER TABLE [Customer].[Preference_tmp] ADD  CONSTRAINT [DF_Preference_tmp_LastModification]  DEFAULT (getutcdate()) FOR [LastModification]

End

-- find out the last copied row:
SET @Start_CustomerPreferenceID = (SELECT ISNULL(MAX(CustomerPreferenceID), 0) FROM [Customer].[Preference_tmp]);


-- copying rows ordered by CustomerPreferenceID:
WHILE @Start_CustomerPreferenceID < (SELECT MAX(CustomerPreferenceID) FROM [Customer].[Preference])
BEGIN

Print CONVERT(char(20), GETDATE(), 120) + 'insert into [Customer].[Preference_tmp] ' + CAST(@SizeOfChunk AS varchar(11)) + ' rows starting from [CustomerPreferenceID] = ' + CAST(@Start_CustomerPreferenceID AS varchar(11));
set identity_insert [Customer].[Preference_tmp] ON
begin tran t1
insert into [Customer].[Preference_tmp]
([CustomerPreferenceID]
      ,[CustomerID]
      ,[PreferenceTypeID]
      ,[NotificationMediumID]
      ,[PreferenceValue]
      ,[PreferenceFlag]
      ,[LastModification]
      ,[SerializedEncryptedValues])
SELECT [CustomerPreferenceID]
      ,[CustomerID]
      ,[PreferenceTypeID]
      ,[NotificationMediumID]
      ,[PreferenceValue]
      ,[PreferenceFlag]
      ,[LastModification]
      ,[SerializedEncryptedValues]
  FROM [Customer].[Preference]
  WHERE [CustomerPreferenceID] > @Start_CustomerPreferenceID
  ORDER BY [CustomerPreferenceID] 
  OFFSET 0 ROWS FETCH NEXT @SizeOfChunk ROWS ONLY
commit tran t1
set identity_insert [Customer].[Preference_tmp] OFF
SET @Start_CustomerPreferenceID = (SELECT MAX(CustomerPreferenceID) FROM [Customer].[Preference_tmp])
END;


Print CONVERT(char(20), GETDATE(), 120) + 'Create index [IX_Preference_CustomerID] ON [Customer].[Preference_tmp]'
CREATE NONCLUSTERED INDEX [IX_Preference_CustomerID] ON [Customer].[Preference_tmp]
(
 [CustomerID] ASC
)
INCLUDE (  [PreferenceTypeID],
 [NotificationMediumID],
 [PreferenceValue],
 [PreferenceFlag],
 [LastModification],
 [SerializedEncryptedValues],
 [CustomerPreferenceID]) -- WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
Print CONVERT(char(20), GETDATE(), 120) + 'Index IX_Preference_CustomerID created'


Print CONVERT(char(20), GETDATE(), 120) + 'Create index [Preference_Customerid_INCL1_NU_NC] ON [Customer].[Preference_tmp]'
CREATE NONCLUSTERED INDEX [Preference_Customerid_INCL1_NU_NC] ON [Customer].[Preference_tmp]
(
 [CustomerID] ASC
)
INCLUDE (  [SerializedEncryptedValues]) --WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
Print CONVERT(char(20), GETDATE(), 120) + 'Index Preference_Customerid_INCL1_NU_NC created'


Print CONVERT(char(20), GETDATE(), 120) + 'Create index [IX_Preference_LastModification_CustomerID] ON [Customer].[Preference_tmp]'
CREATE NONCLUSTERED INDEX [IX_Preference_LastModification_CustomerID] ON [Customer].[Preference_tmp]
(
 [LastModification] ASC,
 [CustomerID] ASC
)
INCLUDE (  [CustomerPreferenceID],
 [PreferenceTypeID],
 [NotificationMediumID],
 [PreferenceValue],
 [PreferenceFlag],
 [SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) 
-- ON [Customer_Index]
Print CONVERT(char(20), GETDATE(), 120) + 'Index IX_Preference_LastModification_CustomerID created'