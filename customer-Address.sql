DECLARE @Start_CustomerAddressID int
		,@SizeOfChunk int = 1000000 -- the number of rows to be copied in one transaction

IF NOT EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Address_tmp]') AND [type]='U')
Begin
 print 'Create table [Customer].[Address_tmp]'
CREATE TABLE [Customer].[Address_tmp](
	[CustomerAddressID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[AddressTypeID] [nvarchar](10) NULL,
	[AllowPromotions] [bit] NULL,
	[Preference] [int] NULL,
	[Address] [nvarchar](max) NULL,
	[Phones] [nvarchar](max) NULL,
	[Version] [nvarchar](50) NULL,
	[SerializedEncryptedValues] [varchar](2000) NULL,
 CONSTRAINT [PK_Address_tmp] PRIMARY KEY CLUSTERED 
(
	[CustomerAddressID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


End

-- find out the last copied row:
SET @Start_CustomerAddressID = (SELECT ISNULL(MAX(CustomerAddressID), 0) FROM [Customer].[Address_tmp]);


-- copying rows ordered by [CustomerAddressID]:
WHILE @Start_CustomerAddressID < (SELECT MAX(CustomerAddressID) FROM [Customer].[Address])
BEGIN

Print CONVERT(char(20), GETDATE(), 120) + 'insert into [Customer].[Address_tmp] ' + CAST(@SizeOfChunk AS varchar(11)) + ' rows starting from [CustomerAddressID] = ' + CAST(@Start_CustomerAddressID AS varchar(11));
set identity_insert [Customer].[Address_tmp] ON
begin tran t1
insert into [Customer].[Address_tmp]
([CustomerAddressID]
      ,[CustomerID]
      ,[AddressTypeID]
      ,[AllowPromotions]
      ,[Preference]
      ,[Address]
      ,[Phones]
      ,[Version]
      ,[SerializedEncryptedValues])
SELECT [CustomerAddressID]
      ,[CustomerID]
      ,[AddressTypeID]
      ,[AllowPromotions]
      ,[Preference]
      ,[Address]
      ,[Phones]
      ,[Version]
      ,[SerializedEncryptedValues]
  FROM [Customer].[Address]
 WHERE [CustomerAddressID] > @Start_CustomerAddressID
  ORDER BY [CustomerAddressID] 
  OFFSET 0 ROWS FETCH NEXT @SizeOfChunk ROWS ONLY
commit tran t1
set identity_insert [Customer].[Address_tmp] OFF
SET @Start_CustomerAddressID = (SELECT MAX(CustomerAddressID) FROM [Customer].[Address_tmp])
END;



Print CONVERT(char(20), GETDATE(), 120) + 'Create Index Address_CustomeridCustomeraddressid_INCL7_NU_NC ON [Customer].[Address_tmp]'
	CREATE NONCLUSTERED INDEX [Address_CustomeridCustomeraddressid_INCL7_NU_NC] ON [Customer].[Address_tmp]
	(
		[CustomerID] ASC,
		[CustomerAddressID] ASC
	)
	INCLUDE ( 	[AddressTypeID],
		[AllowPromotions],
		[Preference],
		[Address],
		[Phones],
		[Version],
		[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	on customer_index
Print CONVERT(char(20), GETDATE(), 120) + 'Index Address_CustomeridCustomeraddressid_INCL7_NU_NC ON [Customer].[Address_tmp] created'


Print CONVERT(char(20), GETDATE(), 120) + 'Create Index Address_CustomerID_NU_NC ON [Customer].[Address_tmp]'

	CREATE NONCLUSTERED INDEX [Address_CustomerID_NU_NC] ON [Customer].[Address_tmp]
	(
		[CustomerID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90)
--	ON Customer_Index
Print CONVERT(char(20), GETDATE(), 120) + 'Index Address_CustomerID_NU_NC ON [Customer].[Address_tmp] created'
