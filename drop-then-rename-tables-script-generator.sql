-- #x table for loggin foreign keys 
-- DROP TABLE IF EXISTS #x;
CREATE TABLE #x -- feel free to use a permanent table
(
  drop_script NVARCHAR(MAX),
  create_script NVARCHAR(MAX),
  referencing_tables NVARCHAR(MAX)
);
  
DECLARE @drop   NVARCHAR(MAX) = N'',
        @create NVARCHAR(MAX) = N'',
		@reft	NVARCHAR(MAX) = N'';

-- drop is easy, just build a simple concatenated list from sys.foreign_keys:
SELECT @drop += N'
ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name) 
    + ' DROP CONSTRAINT ' + QUOTENAME(fk.name) + ';'
FROM sys.foreign_keys AS fk
INNER JOIN sys.tables AS ct
  ON fk.parent_object_id = ct.[object_id]
INNER JOIN sys.schemas AS cs 
  ON ct.[schema_id] = cs.[schema_id]
INNER JOIN sys.tables AS ct1
  ON fk.referenced_object_id = ct1.[object_id]
INNER JOIN sys.schemas AS cs1 
  ON ct1.[schema_id] = cs1.[schema_id]
WHERE (cs1.name = 'Customer' AND ct1.name = 'MyLocation')
	OR (cs1.name = 'Customer' AND ct1.name = 'MobileDevices')
	OR (cs1.name = 'Customer' AND ct1.name = 'DynamicOptin')
	OR (cs1.name = 'Customer' AND ct1.name = 'Acceptance')
	OR (cs1.name = 'Customer' AND ct1.name = 'Subscription')
	OR (cs1.name = 'Customer' AND ct1.name = 'Preference')
	OR (cs1.name = 'Customer' AND ct1.name = 'CustomerPaymentMethod')
	OR (cs1.name = 'Customer' AND ct1.name = 'Favorite')
	OR (cs1.name = 'Customer' AND ct1.name = 'AuthToken')
	OR (cs1.name = 'Customer' AND ct1.name = 'Address')
	OR (cs1.name = 'Customer' AND ct1.name = 'TINData')
	OR (cs1.name = 'Customer' AND ct1.name = 'Profile')
	OR (cs1.name = 'Customer' AND ct1.name = 'Optin')
	OR (cs1.name = 'Notification' AND ct1.name = 'Feedback')
	OR (cs1.name = 'Customer' AND ct1.name = 'SocialNetwork')
	OR (cs1.name = 'Customer' AND ct1.name = 'SocialNetworkAccessToken') 
	OR (cs1.name = 'Customer' AND ct1.name = 'LoyaltyMembershipNumber')
	OR (cs1.name = 'Authentication' AND ct1.name = 'Authentication')
	OR (cs1.name = 'Authentication' AND ct1.name = 'AuthInfo');

INSERT #x(drop_script) SELECT @drop;


SELECT @reft += N'
ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name) 
    + ' WITH CHECK CHECK CONSTRAINT ' + QUOTENAME(fk.name) + ';'
FROM sys.foreign_keys AS fk
INNER JOIN sys.tables AS ct
  ON fk.parent_object_id = ct.[object_id]
INNER JOIN sys.schemas AS cs 
  ON ct.[schema_id] = cs.[schema_id]
INNER JOIN sys.tables AS ct1
  ON fk.referenced_object_id = ct1.[object_id]
INNER JOIN sys.schemas AS cs1 
  ON ct1.[schema_id] = cs1.[schema_id]
WHERE (cs1.name = 'Customer' AND ct1.name = 'MyLocation')
	OR (cs1.name = 'Customer' AND ct1.name = 'MobileDevices')
	OR (cs1.name = 'Customer' AND ct1.name = 'DynamicOptin')
	OR (cs1.name = 'Customer' AND ct1.name = 'Acceptance')
	OR (cs1.name = 'Customer' AND ct1.name = 'Subscription')
	OR (cs1.name = 'Customer' AND ct1.name = 'Preference')
	OR (cs1.name = 'Customer' AND ct1.name = 'CustomerPaymentMethod')
	OR (cs1.name = 'Customer' AND ct1.name = 'Favorite')
	OR (cs1.name = 'Customer' AND ct1.name = 'AuthToken')
	OR (cs1.name = 'Customer' AND ct1.name = 'Address')
	OR (cs1.name = 'Customer' AND ct1.name = 'TINData')
	OR (cs1.name = 'Customer' AND ct1.name = 'Profile')
	OR (cs1.name = 'Customer' AND ct1.name = 'Optin')
	OR (cs1.name = 'Notification' AND ct1.name = 'Feedback')
	OR (cs1.name = 'Customer' AND ct1.name = 'SocialNetwork')
	OR (cs1.name = 'Customer' AND ct1.name = 'SocialNetworkAccessToken') 
	OR (cs1.name = 'Customer' AND ct1.name = 'LoyaltyMembershipNumber')
	OR (cs1.name = 'Authentication' AND ct1.name = 'Authentication')
	OR (cs1.name = 'Authentication' AND ct1.name = 'AuthInfo');

UPDATE #x SET referencing_tables = @reft;


-- create is a little more complex. We need to generate the list of 
-- columns on both sides of the constraint, even though in most cases
-- there is only one column.
SELECT @create += N'
ALTER TABLE WITH NOCHECK ' 
   + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name) 
   + ' ADD CONSTRAINT ' + QUOTENAME(fk.name) 
   + ' FOREIGN KEY (' + STUFF((SELECT ',' + QUOTENAME(c.name)
   -- get all the columns in the constraint table
    FROM sys.columns AS c 
    INNER JOIN sys.foreign_key_columns AS fkc 
    ON fkc.parent_column_id = c.column_id
    AND fkc.parent_object_id = c.[object_id]
    WHERE fkc.constraint_object_id = fk.[object_id]
    ORDER BY fkc.constraint_column_id 
    FOR XML PATH(N''), TYPE).value(N'.[1]', N'nvarchar(max)'), 1, 1, N'')
  + ') REFERENCES ' + QUOTENAME(rs.name) + '.' + QUOTENAME(rt.name)
  + '(' + STUFF((SELECT ',' + QUOTENAME(c.name)
   -- get all the referenced columns
    FROM sys.columns AS c 
    INNER JOIN sys.foreign_key_columns AS fkc 
    ON fkc.referenced_column_id = c.column_id
    AND fkc.referenced_object_id = c.[object_id]
    WHERE fkc.constraint_object_id = fk.[object_id]
    ORDER BY fkc.constraint_column_id 
    FOR XML PATH(N''), TYPE).value(N'.[1]', N'nvarchar(max)'), 1, 1, N'') + ');'
FROM sys.foreign_keys AS fk
INNER JOIN sys.tables AS rt -- referenced table
  ON fk.referenced_object_id = rt.[object_id]
INNER JOIN sys.schemas AS rs 
  ON rt.[schema_id] = rs.[schema_id]
INNER JOIN sys.tables AS ct -- constraint table
  ON fk.parent_object_id = ct.[object_id]
INNER JOIN sys.schemas AS cs 
  ON ct.[schema_id] = cs.[schema_id]
INNER JOIN sys.tables AS ct1
  ON fk.referenced_object_id = ct1.[object_id]
INNER JOIN sys.schemas AS cs1 
  ON ct1.[schema_id] = cs1.[schema_id]
WHERE rt.is_ms_shipped = 0 AND ct.is_ms_shipped = 0 
AND (
		(cs.name = 'Customer' AND ct.name = 'MyLocation') OR (cs1.name = 'Customer' AND ct1.name = 'MyLocation')
		OR (cs.name = 'Customer' AND ct.name = 'MobileDevices') OR (cs1.name = 'Customer' AND ct1.name = 'MobileDevices')
		OR (cs.name = 'Customer' AND ct.name = 'DynamicOptin') OR (cs1.name = 'Customer' AND ct1.name = 'DynamicOptin')
		OR (cs.name = 'Customer' AND ct.name = 'Acceptance') OR (cs1.name = 'Customer' AND ct1.name = 'Acceptance')
		OR (cs.name = 'Customer' AND ct.name = 'Subscription') OR (cs1.name = 'Customer' AND ct1.name = 'Subscription')
		OR (cs.name = 'Customer' AND ct.name = 'Preference') OR (cs1.name = 'Customer' AND ct1.name = 'Preference')
		OR (cs.name = 'Customer' AND ct.name = 'CustomerPaymentMethod') OR (cs1.name = 'Customer' AND ct1.name = 'CustomerPaymentMethod')
		OR (cs.name = 'Customer' AND ct.name = 'Favorite') OR (cs1.name = 'Customer' AND ct1.name = 'Favorite')
		OR (cs.name = 'Customer' AND ct.name = 'AuthToken') OR (cs1.name = 'Customer' AND ct1.name = 'AuthToken')
		OR (cs.name = 'Customer' AND ct.name = 'Address') OR (cs1.name = 'Customer' AND ct1.name = 'Address')
		OR (cs.name = 'Customer' AND ct.name = 'TINData') OR (cs1.name = 'Customer' AND ct1.name = 'TINData')
		OR (cs.name = 'Customer' AND ct.name = 'Profile') OR (cs1.name = 'Customer' AND ct1.name = 'Profile')
		OR (cs.name = 'Customer' AND ct.name = 'Optin') OR (cs1.name = 'Customer' AND ct1.name = 'Optin')
		OR (cs.name = 'Notification' AND ct.name = 'Feedback') OR (cs1.name = 'Notification' AND ct1.name = 'Feedback')
		OR (cs.name = 'Customer' AND ct.name = 'SocialNetwork') OR (cs1.name = 'Customer' AND ct1.name = 'SocialNetwork')
		OR (cs.name = 'Customer' AND ct.name = 'SocialNetworkAccessToken') OR (cs1.name = 'Customer' AND ct1.name = 'SocialNetworkAccessToken')
		OR (cs.name = 'Customer' AND ct.name = 'LoyaltyMembershipNumber') OR (cs1.name = 'Customer' AND ct1.name = 'LoyaltyMembershipNumber')
		OR (cs.name = 'Authentication' AND ct.name = 'Authentication') OR (cs1.name = 'Authentication' AND ct1.name = 'Authentication')
		OR (cs.name = 'Authentication' AND ct.name = 'AuthInfo') OR (cs1.name = 'Authentication' AND ct1.name = 'AuthInfo')


	);

UPDATE #x SET create_script = @create;

-- PRINT @drop;
-- PRINT @create;
-- PRINT @reft

-- removing foreign keys referencing on deleting tables
EXEC sp_executesql @drop

-- drop tables part:
DROP TABLE [Customer].[MyLocation];
DROP TABLE [Customer].[MobileDevices];
DROP TABLE [Customer].[DynamicOptin];
DROP TABLE [Customer].[Acceptance];
DROP TABLE [Customer].[Subscription] 
DROP TABLE [Customer].[Preference];
DROP TABLE [Customer].[CustomerPaymentMethod];
DROP TABLE [Customer].[Favorite];
DROP TABLE [Customer].[AuthToken];
DROP TABLE [Customer].[Address];
DROP TABLE [Customer].[TINData];
DROP TABLE [Customer].[Profile];
DROP TABLE [Customer].[Optin];
DROP TABLE [Notification].[Feedback];
DROP TABLE [Customer].[SocialNetwork];
DROP TABLE [Customer].[SocialNetworkAccessToken]; 
DROP TABLE [Customer].[LoyaltyMembershipNumber];
DROP TABLE [Authentication].[Authentication];
DROP TABLE [Authentication].[AuthInfo];


EXEC sp_rename 'Customer.MyLocation_tmp', 'MyLocation';
EXEC sp_rename 'PK_MyLocation_tmp', 'PK_MyLocation';
EXEC sp_rename 'Customer.MobileDevices_tmp', 'MobileDevices';
EXEC sp_rename 'PK_MobileDevices_tmp', 'PK_MobileDevices';
EXEC sp_rename 'Customer.DynamicOptin_tmp', 'DynamicOptin';
EXEC sp_rename 'PK_DynamicOptIn_tmp', 'PK_DynamicOptIn';
EXEC sp_rename 'Customer.Acceptance_tmp', 'Acceptance';
EXEC sp_rename 'PK_Acceptance_tmp', 'PK_Acceptance';
EXEC sp_rename 'Customer.Subscription_tmp', 'Subscription';
EXEC sp_rename 'PK_Subscription_tmp', 'PK_Subscription';
EXEC sp_rename 'Customer.Preference_tmp', 'Preference';
EXEC sp_rename 'PK_Preference_tmp', 'PK_Preference';
EXEC sp_rename 'DF_Preference_tmp_LastModification', 'DF_Preference_LastModification';
EXEC sp_rename 'Customer.CustomerPaymentMethod_tmp', 'CustomerPaymentMethod';
EXEC sp_rename 'PK_CustomerPaymentMethod_tmp', 'PK_CustomerPaymentMethod';
EXEC sp_rename 'Customer.Favorite_tmp', 'Favorite';
EXEC sp_rename 'PK_Favorite_tmp', 'PK_Favorite';
EXEC sp_rename 'Customer.AuthToken_tmp', 'AuthToken';
EXEC sp_rename 'PK_AuthToken_tmp', 'PK_AuthToken';
EXEC sp_rename 'DF_AuthTOken_tmp_LastModification', 'DF_AuthTOken_LastModification';
EXEC sp_rename 'Customer.Address_tmp', 'Address';
EXEC sp_rename 'PK_Address_tmp', 'PK_Address';
EXEC sp_rename 'Customer.TINData_tmp', 'TINData';
EXEC sp_rename 'PK_TINData_tmp', 'PK_TINData';
EXEC sp_rename 'DF_TINData_tmp_LastModification', 'DF_TINData_LastModification';
EXEC sp_rename 'Customer.Profile_tmp', 'Profile';
EXEC sp_rename 'PK_Profile_tmp', 'PK_Profile';
EXEC sp_rename 'Customer.Optin_tmp', 'Optin';
EXEC sp_rename 'PK_Optin_tmp', 'PK_Optin';
EXEC sp_rename 'Notification.Feedback_tmp', 'Feedback';
EXEC sp_rename 'PK_Feedback_tmp', 'PK_Feedback';
EXEC sp_rename 'DF_Feedback_tmp_LastModification', 'DF_Feedback_LastModification';
EXEC sp_rename 'Customer.SocialNetwork_tmp', 'SocialNetwork';
EXEC sp_rename 'PK_SocialNetwork_tmp', 'PK_SocialNetwork';
EXEC sp_rename 'Customer.SocialNetworkAccessToken_tmp', 'SocialNetworkAccessToken';
EXEC sp_rename 'PK_SocialNetworkAccessToken_tmp', 'PK_SocialNetworkAccessToken';
EXEC sp_rename 'Customer.LoyaltyMembershipNumber_tmp', 'LoyaltyMembershipNumber';
EXEC sp_rename 'PK_LoyaltyMembershipNumber_tmp', 'PK_LoyaltyMembershipNumber';
EXEC sp_rename 'DF_LoyaltyMembershipNumber_tmp_LastModificationUTC', 'DF_LoyaltyMembershipNumber_LastModificationUTC';
EXEC sp_rename 'Authentication.Authentication_tmp', 'Authentication';
EXEC sp_rename 'PK_Authentication_tmp', 'PK_Authentication';
EXEC sp_rename 'DF_Authentication_tmp_LoginSourceType', 'DF_Authentication_LoginSourceType';
EXEC sp_rename 'Authentication.AuthInfo_tmp', 'AuthInfo';
EXEC sp_rename 'PK_AuthInfo_tmp', 'PK_AuthInfo';
EXEC sp_rename 'DF_AuthInfo_tmp_PasswordEncrypterType', 'DF_AuthInfo_PasswordEncrypterType';




EXEC sp_executesql @create;


-- The query optimizer does not consider constraints that are defined WITH NOCHECK. Such constraints are ignored until they are re-enabled by using ALTER TABLE <table> WITH CHECK CHECK CONSTRAINT ALL.

EXEC sp_executesql @reft;

ALTER TABLE [Customer].[MyLocation] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[MobileDevices] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[DynamicOptin] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[Acceptance] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[Subscription] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[Preference] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[CustomerPaymentMethod] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[Favorite] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[AuthToken] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[Address] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[TINData] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[Profile] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[Optin] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Notification].[Feedback] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[SocialNetwork] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[SocialNetworkAccessToken] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[LoyaltyMembershipNumber] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Authentication].[Authentication] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Authentication].[AuthInfo] WITH CHECK CHECK CONSTRAINT ALL;