DECLARE @Start_CustomerID int
		,@SizeOfChunk int = 1000000 -- the number of rows to be copied in one transaction

IF NOT EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Profile_tmp]') AND [type]='U')
Begin
 print 'Create table [Customer].[Profile_tmp]'
CREATE TABLE [Customer].[Profile_tmp](
	[CustomerID] [int] IDENTITY(1,1) NOT NULL,
	[ExternalID] [int] NULL,
	[FirstName] [nvarchar](128) NULL,
	[MiddleName] [nvarchar](128) NULL,
	[LastName] [nvarchar](128) NULL,
	[Title] [nvarchar](128) NULL,
	[YearOfBirth] [int] NULL,
	[DefaultPhoneNumber] [nvarchar](128) NULL,
	[TaxNumber] [nvarchar](128) NULL,
	[CancellationReason] [nvarchar](max) NULL,
	[CreationDate] [datetime] NOT NULL,
	[Remark] [nvarchar](max) NULL,
	[LastModification] [datetime] NOT NULL DEFAULT (getutcdate()),
	[NickName] [nvarchar](128) NULL,
	[GenderID] [tinyint] NULL,
	[EthnicityID] [tinyint] NULL,
	[MarketName] [nvarchar](128) NULL,
	[MonthOfBirth] [tinyint] NULL,
	[DayOfBirth] [tinyint] NULL,
	[StatusID] [tinyint] NULL,
	[DisplayNamePreference] [nvarchar](32) NULL,
	[ZipCode] [nvarchar](32) NULL,
	[EmailAddress] [nvarchar](128) NULL,
	[ActivationTime] [datetime] NULL,
	[RegistrationHashCode] [nvarchar](256) NULL,
	[IsGuest] [bit] NOT NULL DEFAULT ((0)),
	[EArchCardLastLoadAmount] [decimal](18, 2) NULL,
	[DefaultAddressType] [nvarchar](10) NULL,
	[OrderSource] [nvarchar](32) NULL,
	[PlatformSource] [nvarchar](32) NULL,
	[PhoneVerificationHashCode] [nvarchar](256) NULL,
	[PhoneStatus] [int] NULL,
	[IsValuedCustomer] [bit] NOT NULL DEFAULT ((0)),
	[SerializedEncryptedValues] [varchar](3700) NULL,
	[SpecialAttentionRequired] [bit] NOT NULL DEFAULT ((0)),
	[SpecialAttentionRequiredReason] [nvarchar](128) NULL,
	[MSAlarmEnabled] [bit] NULL,
	EmailVerificationHashCode nvarchar(256) null,
	EmailStatus int null,
	ExternalPaymentId nvarchar(128) null,
	DCSID nchar(50) null
 CONSTRAINT [PK_Profile_tmp] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)


End

-- find out the last copied row:
SET @Start_CustomerID = (SELECT ISNULL(MAX(CustomerID), 0) FROM [Customer].[Profile_tmp]);


-- copying rows ordered by [CustomerID]:
WHILE @Start_CustomerID < (SELECT MAX(CustomerID) FROM [Customer].[Profile])
BEGIN

Print CONVERT(char(20), GETDATE(), 120) + 'insert into [Customer].[Profile_tmp] ' + CAST(@SizeOfChunk AS varchar(11)) + ' rows starting from [CustomerID] = ' + CAST(@Start_CustomerID AS varchar(11));
set identity_insert [Customer].[Profile_tmp] ON
begin tran t1
Insert Into [Customer].[Profile_tmp] (
	[CustomerID]
	,[ExternalID]
	,[FirstName]
	,[MiddleName]
	,[LastName]
	,[Title]
	,[YearOfBirth]
	,[DefaultPhoneNumber]
	,[TaxNumber]
	,[CancellationReason]
	,[CreationDate]
	,[Remark]
	,[LastModification]
	,[NickName]
	,[GenderID]
	,[EthnicityID]
	,[MarketName]
	,[MonthOfBirth]
	,[DayOfBirth]
	,[StatusID]
	,[DisplayNamePreference]
	,[ZipCode]
	,[EmailAddress]
	,[ActivationTime]
	,[RegistrationHashCode]
	,[IsGuest]
	,[EArchCardLastLoadAmount]
	,[DefaultAddressType]
	,[OrderSource]
	,[PlatformSource]
	,[PhoneVerificationHashCode]
	,[PhoneStatus]
	,[IsValuedCustomer]
	,[SerializedEncryptedValues] 
	,[SpecialAttentionRequired] 
	,[SpecialAttentionRequiredReason]
	,[MSAlarmEnabled] 
	,EmailVerificationHashCode
	,EmailStatus
	,ExternalPaymentId
	,DCSID)
SELECT 
	[CustomerID]
	,[ExternalID]
	,[FirstName]
	,[MiddleName]
	,[LastName]
	,[Title]
	,[YearOfBirth]
	,[DefaultPhoneNumber]
	,[TaxNumber]
	,[CancellationReason]
	,[CreationDate]
	,[Remark]
	,[LastModification]
	,[NickName]
	,[GenderID]
	,[EthnicityID]
	,[MarketName]
	,[MonthOfBirth]
	,[DayOfBirth]
	,[StatusID]
	,[DisplayNamePreference]
	,[ZipCode]
	,[EmailAddress]
	,[ActivationTime]
	,[RegistrationHashCode]
	,[IsGuest]
	,[EArchCardLastLoadAmount]
	,[DefaultAddressType]
	,[OrderSource]
	,[PlatformSource]
	,[PhoneVerificationHashCode]
	,[PhoneStatus]
	,[IsValuedCustomer]
	,[SerializedEncryptedValues] 
	,[SpecialAttentionRequired] 
	,[SpecialAttentionRequiredReason]
	,[MSAlarmEnabled] 
	,EmailVerificationHashCode
	,EmailStatus
	,ExternalPaymentId
	,DCSID
FROM [Customer].[Profile]
 WHERE [CustomerID] > @Start_CustomerID
  ORDER BY [CustomerID] 
  OFFSET 0 ROWS FETCH NEXT @SizeOfChunk ROWS ONLY
commit tran t1
set identity_insert [Customer].[Profile_tmp] OFF
SET @Start_CustomerID = (SELECT MAX(CustomerID) FROM [Customer].[Profile_tmp])
END;



Print CONVERT(char(20), GETDATE(), 120) + 'Create Index Profile_Statusid_NU_NC ON [Customer].[Profile_tmp]'
	CREATE NONCLUSTERED INDEX [Profile_Statusid_NU_NC] ON [Customer].[Profile_tmp]
	(
		[StatusID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90)
--	ON Customer_Index
Print CONVERT(char(20), GETDATE(), 120) + 'Index Profile_Statusid_NU_NC ON [Customer].[Profile_tmp] created'


Print CONVERT(char(20), GETDATE(), 120) + 'Create Index Profile_Creationdate_INCL1_NU_NC ON [Customer].[Profile_tmp]'
	CREATE NONCLUSTERED INDEX [Profile_Creationdate_INCL1_NU_NC] ON [Customer].[Profile_tmp]
	(
		[CreationDate] ASC
	)
	INCLUDE ( 	[CustomerID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90)
--	ON Customer_Index
Print CONVERT(char(20), GETDATE(), 120) + 'Index Profile_Creationdate_INCL1_NU_NC ON [Customer].[Profile_tmp] created'


Print CONVERT(char(20), GETDATE(), 120) + 'Create Index Profile_Defaultphonenumber_NU_NC ON [Customer].[Profile_tmp]'
	CREATE NONCLUSTERED INDEX [Profile_Defaultphonenumber_NU_NC] ON [Customer].[Profile_tmp]
	(
		[DefaultPhoneNumber] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90)
--	ON Customer_Index
Print CONVERT(char(20), GETDATE(), 120) + 'Index Profile_Defaultphonenumber_NU_NC ON [Customer].[Profile_tmp] created'


Print CONVERT(char(20), GETDATE(), 120) + 'Create Index Profile_Emailverificationhashcode_INCL1_NU_NC ON [Customer].[Profile_tmp]'
	CREATE NONCLUSTERED INDEX [Profile_Emailverificationhashcode_INCL1_NU_NC] ON [Customer].[Profile_tmp]
	(
		[EmailVerificationHashCode] ASC
	)
	INCLUDE ( 	[CustomerID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90)
--	ON Customer_Index
Print CONVERT(char(20), GETDATE(), 120) + 'Index Profile_Emailverificationhashcode_INCL1_NU_NC ON [Customer].[Profile_tmp] created'


Print CONVERT(char(20), GETDATE(), 120) + 'Create Index Profile_MarketnameLastmodificationCustomerid_NU_NC ON [Customer].[Profile_tmp]'
	CREATE NONCLUSTERED INDEX [Profile_MarketnameLastmodificationCustomerid_NU_NC] ON [Customer].[Profile_tmp]
(
	[MarketName] ASC,
	[LastModification] ASC,
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	ON Customer_Index
Print CONVERT(char(20), GETDATE(), 120) + 'Index Profile_MarketnameLastmodificationCustomerid_NU_NC ON [Customer].[Profile_tmp] created'


Print CONVERT(char(20), GETDATE(), 120) + 'Create Index Profile_Registrationhascode_INCL1_NU_NC ON [Customer].[Profile_tmp]'
	CREATE NONCLUSTERED INDEX [Profile_Registrationhascode_INCL1_NU_NC] ON [Customer].[Profile_tmp]
	(
		[RegistrationHashCode] ASC
	)
	INCLUDE ( 	[PhoneVerificationHashCode]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	ON Customer_Index
Print CONVERT(char(20), GETDATE(), 120) + 'Index Profile_Registrationhascode_INCL1_NU_NC ON [Customer].[Profile_tmp] created'