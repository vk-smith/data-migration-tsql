DECLARE @Start_AuthInfoID int
		,@SizeOfChunk int = 1000000 -- the number of rows to be copied in one transaction

IF NOT EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Authentication].[AuthInfo_tmp]') AND [type]='U')
Begin
 print 'Create table [Authentication].[AuthInfo_tmp]'
CREATE TABLE [Authentication].[AuthInfo_tmp](
	[AuthInfoID] [int] IDENTITY(1,1) NOT NULL,
	[Password] [nvarchar](128) NULL,
	[RegistrationTime] [datetime] NULL,
	[MustChangePassword] [bit] NULL,
	[LastPasswordChangeTime] [datetime] NULL,
	[SerializedEncryptedValues] [varchar](2000) NULL,
	[PasswordEncrypterType] [int] NULL,
 CONSTRAINT [PK_AuthInfo_tmp] PRIMARY KEY CLUSTERED 
(
	[AuthInfoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [FG1]
)

ALTER TABLE [Authentication].[AuthInfo_tmp] ADD  CONSTRAINT [DF_AuthInfo_tmp_PasswordEncrypterType]  DEFAULT ((5)) FOR [PasswordEncrypterType]

End

-- find out the last copied row:
SET @Start_AuthInfoID = (SELECT ISNULL(MAX(AuthInfoID), 0) FROM [Authentication].[AuthInfo_tmp]);


-- copying rows ordered by [AuthInfoID]:
WHILE @Start_AuthInfoID < (SELECT MAX(AuthInfoID) FROM [Authentication].[AuthInfo])
BEGIN

Print CONVERT(char(20), GETDATE(), 120) + 'insert into [Authentication].[AuthInfo_tmp] ' + CAST(@SizeOfChunk AS varchar(11)) + ' rows starting from [AuthInfoID] = ' + CAST(@Start_AuthInfoID AS varchar(11));
set identity_insert [Authentication].[AuthInfo_tmp] ON
begin tran t1
INSERT INTO [Authentication].[AuthInfo_tmp]
           (AuthInfoID
		   ,[Password]
           ,[RegistrationTime]
           ,[MustChangePassword]
           ,[LastPasswordChangeTime]
           ,[SerializedEncryptedValues]
           ,[PasswordEncrypterType])
SELECT 
           AuthInfoID
		   ,[Password]
           ,[RegistrationTime]
           ,[MustChangePassword]
           ,[LastPasswordChangeTime]
           ,[SerializedEncryptedValues]
           ,[PasswordEncrypterType]
FROM [Authentication].[AuthInfo]
 WHERE [AuthInfoID] > @Start_AuthInfoID
  ORDER BY [AuthInfoID] 
  OFFSET 0 ROWS FETCH NEXT @SizeOfChunk ROWS ONLY
commit tran t1
set identity_insert [Authentication].[AuthInfo_tmp] OFF
SET @Start_AuthInfoID = (SELECT MAX(AuthInfoID) FROM [Authentication].[AuthInfo_tmp])
END;



