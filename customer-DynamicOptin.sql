DECLARE @Start_CustomerDynamicOptinID int
		,@SizeOfChunk int = 1000000 -- the number of rows to be copied in one transaction

IF NOT EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[DynamicOptin_tmp]') AND [type]='U')
Begin
 print 'Create table [Customer].[DynamicOptin_tmp]'
CREATE TABLE [Customer].[DynamicOptin_tmp](
	[CustomerDynamicOptinID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[DynamicOptInType] [varchar](32) NULL,
	[Status] [bit] NULL,
	[AcceptanceTimestamp] [datetime] NULL,
	[LastModification] [datetime] NULL DEFAULT (getutcdate()),
	[SerializedEncryptedValues] [varchar](2000) NULL,
	[EffectiveDate] [datetime] NULL,
	[ExpirationDate] [datetime] NULL,
 CONSTRAINT [PK_DynamicOptIn_tmp] PRIMARY KEY CLUSTERED 
(
	[CustomerDynamicOptinID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [FG1]
)


End

-- find out the last copied row:
SET @Start_CustomerDynamicOptinID = (SELECT ISNULL(MAX(CustomerDynamicOptinID), 0) FROM [Customer].[DynamicOptin_tmp]);


-- copying rows ordered by [CustomerDynamicOptinID]:
WHILE @Start_CustomerDynamicOptinID < (SELECT MAX(CustomerDynamicOptinID) FROM [Customer].[DynamicOptin])
BEGIN

Print CONVERT(char(20), GETDATE(), 120) + 'insert into [Customer].[CustomerDynamicOptinID] ' + CAST(@SizeOfChunk AS varchar(11)) + ' rows starting from [CustomerDynamicOptinID] = ' + CAST(@Start_CustomerDynamicOptinID AS varchar(11));
set identity_insert [Customer].[DynamicOptin_tmp] ON
begin tran t1
insert into [Customer].[DynamicOptin_tmp]
	 ([CustomerDynamicOptinID]
      ,[CustomerID]
      ,[DynamicOptInType]
      ,[Status]
      ,[AcceptanceTimestamp]
      ,[LastModification]
      ,[SerializedEncryptedValues]
      ,[EffectiveDate]
      ,[ExpirationDate])
SELECT [CustomerDynamicOptinID]
      ,[CustomerID]
      ,[DynamicOptInType]
      ,[Status]
      ,[AcceptanceTimestamp]
      ,[LastModification]
      ,[SerializedEncryptedValues]
      ,[EffectiveDate]
      ,[ExpirationDate]
FROM [Customer].[DynamicOptin]
 WHERE [CustomerDynamicOptinID] > @Start_CustomerDynamicOptinID
  ORDER BY [CustomerDynamicOptinID] 
  OFFSET 0 ROWS FETCH NEXT @SizeOfChunk ROWS ONLY
commit tran t1
set identity_insert [Customer].[DynamicOptin_tmp] OFF
SET @Start_CustomerDynamicOptinID = (SELECT MAX([CustomerDynamicOptinID]) FROM [Customer].[DynamicOptin_tmp])
END;


--ALTER TABLE [Customer].[DynamicOptin_tmp] ADD  CONSTRAINT [DF_DynamicOptIn_tmp_LastModification]  DEFAULT (getutcdate()) FOR [LastModification]


Print CONVERT(char(20), GETDATE(), 120) + 'Create index IX_DynamicOptIn_CustomerId on Customer.DynamicOptin_tmp'
CREATE NONCLUSTERED INDEX [IX_DynamicOptIn_CustomerId] ON [Customer].[DynamicOptin_tmp]
(
	[CustomerID] ASC,
	[DynamicOptInType] ASC
)
INCLUDE ( 	[Status],
	[AcceptanceTimestamp],
	[LastModification],
	[EffectiveDate],
	[ExpirationDate],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) 
--	ON [Customer_Index]
Print CONVERT(char(20), GETDATE(), 120) + 'Index IX_DynamicOptIn_CustomerId on Customer.DynamicOptin_tmp created'


