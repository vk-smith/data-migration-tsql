/* set the FG1 to default filegroup
Unfortunately the dbproject change didn't go to release yet, 
so we cannot use the Customer_index and Authentication_Index filegroups.
Therefore the filegroup reference has been commented in case of index creation statements.
If you can create the above filegroups, please uncomment the filegroup references */
ALTER DATABASE <EcpCustomer> MODIFY FILEGROUP FG1 DEFAULT
GO

/* in customer db !!!!! */
USE <EcpCustomer>


/* MyLocation */
if exists (select 1 from sys.objects where name = 'FK_MyLocation_Profile' and type='F')
Begin
	print 'Drop FK_MyLocation_Profile foreign key'
	ALTER TABLE [Customer].[MyLocation] DROP CONSTRAINT [FK_MyLocation_Profile]
End	
GO

declare @constraint_name nvarchar(128) = N''
SELECT @constraint_name = constraint_name
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
AND TABLE_NAME = 'MyLocation' AND TABLE_SCHEMA = 'Customer'

if isnull(len(@constraint_name),0) > 0
begin
	print 'Drop Primary key of Customer.MyLocation'
	declare @csql nvarchar(1000) = N'ALTER TABLE Customer.MyLocation DROP CONSTRAINT ['+@constraint_name+N']'
	exec sp_executesql @stmt = @cSql 
end	
GO

CREATE TABLE [Customer].[MyLocation_tmp](
	[CustomerMyLocationID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[Name] [nvarchar](128) NULL,
	[POSStoreNumber] [nvarchar](128) NULL,
	[IsValid] [bit] NOT NULL,
	[LastModification] [datetime] NOT NULL DEFAULT (getutcdate()),
	[SerializedEncryptedValues] [varchar](2000) NULL,
 CONSTRAINT [PK_MyLocation] PRIMARY KEY CLUSTERED 
(
	[CustomerMyLocationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON FG1
) ON [FG1] 
GO

Print 'insert into [Customer].[MyLocation_tmp]'
set identity_insert [Customer].[MyLocation_tmp] ON
GO
insert into [Customer].[MyLocation_tmp]
	  ([CustomerMyLocationID]
      ,[CustomerID]
      ,[Name]
      ,[POSStoreNumber]
      ,[IsValid]
      ,[LastModification]
      ,[SerializedEncryptedValues])
SELECT [CustomerMyLocationID]
      ,[CustomerID]
      ,[Name]
      ,[POSStoreNumber]
      ,[IsValid]
      ,[LastModification]
      ,[SerializedEncryptedValues]
  FROM [Customer].[MyLocation]
set identity_insert [Customer].[MyLocation_tmp] OFF
GO

sp_rename 'Customer.MyLocation', 'MyLocation_old'
GO
sp_rename 'Customer.MyLocation_tmp', 'MyLocation'
GO

ALTER TABLE [Customer].[MyLocation]  WITH CHECK ADD  CONSTRAINT [FK_MyLocation_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[MyLocation] CHECK CONSTRAINT [FK_MyLocation_Profile]
GO
------------------------------------------------------------------------------------------------------------------
/* MobilDevices */
if exists (select 1 from sys.objects where name = 'FK_MobileDevices_Profile' and type='F')
Begin
	print 'Drop FK_MobileDevices_Profile foreign key'
	ALTER TABLE [Customer].[MobileDevices] DROP CONSTRAINT [FK_MobileDevices_Profile]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[MobileDevices]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_MobileDevices_LastModification_CustomerID' 
	AND [object_id]=OBJECT_ID(N'[Customer].[MobileDevices]')))
Begin	
	Print 'Drop Index IX_MobileDevices_LastModification_CustomerID ON [Customer].[MobilDevices]'
	DROP INDEX [IX_MobileDevices_LastModification_CustomerID] ON [Customer].[MobileDevices]
End	
GO

declare @constraint_name nvarchar(128) = N''
SELECT @constraint_name = constraint_name
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
AND TABLE_NAME = 'MobileDevices' AND TABLE_SCHEMA = 'Customer'

if isnull(len(@constraint_name),0) > 0
begin
	print 'Drop Primary key of Customer.MobileDevices'
	declare @csql nvarchar(1000) = N'ALTER TABLE Customer.MobileDevices DROP CONSTRAINT ['+@constraint_name+N']'
	exec sp_executesql @stmt = @cSql 
end	
GO

CREATE TABLE [Customer].[MobileDevices_tmp](
	[CustomerID] [int] NOT NULL,
	[DeviceTokenHash] [nvarchar](32) NOT NULL,
	[DeviceToken] [nvarchar](max) NULL,
	[DeviceOperatingSystem] [nvarchar](64) NULL,
	[LastModification] [datetime] NOT NULL,
	[SerializedEncryptedValues] [varchar](2000) NULL,
 CONSTRAINT [PK_MobileDevices] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC,
	[DeviceTokenHash] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [FG1]
) ON [FG1]
GO

Print 'insert into [Customer].[MobileDevices_tmp]'
insert into [Customer].[MobileDevices_tmp]
	  ([CustomerID]
      ,[DeviceTokenHash]
      ,[DeviceToken]
      ,[DeviceOperatingSystem]
      ,[LastModification]
      ,[SerializedEncryptedValues])
SELECT distinct [CustomerID]
      ,[DeviceTokenHash]
      ,[DeviceToken]
      ,[DeviceOperatingSystem]
      ,[LastModification]
      ,[SerializedEncryptedValues]
  FROM [Customer].[MobileDevices]
GO

sp_rename 'Customer.MobileDevices', 'MobileDevices_old'
GO
sp_rename 'Customer.MobileDevices_tmp', 'MobileDevices'
GO

ALTER TABLE [Customer].[MobileDevices] ADD  DEFAULT (getutcdate()) FOR [LastModification]
GO

ALTER TABLE [Customer].[MobileDevices]  WITH CHECK ADD  CONSTRAINT [FK_MobileDevices_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[MobileDevices] CHECK CONSTRAINT [FK_MobileDevices_Profile]
GO

Print 'Create index IX_MobileDevices_LastModification_CustomerID on Customer.MobileDevices'
CREATE NONCLUSTERED INDEX [IX_MobileDevices_LastModification_CustomerID] ON [Customer].[MobileDevices]
(
	[LastModification] ASC,
	[CustomerID] ASC
)
INCLUDE ( 	[DeviceTokenHash],
	[DeviceToken],
	[DeviceOperatingSystem],
	[SerializedEncryptedValues]) 
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	ON Customer_Index
GO
------------------------------------------------------------------------------------------------
/* DynamicOptIn */
if exists (select 1 from sys.objects where name = 'FK_DynamicOptIn_Profile' and type='F')
Begin
	print 'Drop FK_DynamicOptIn_Profile foreign key'
	ALTER TABLE [Customer].[DynamicOptin] DROP CONSTRAINT [FK_DynamicOptIn_Profile]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Acceptance]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_DynamicOptIn_CustomerId' 
	AND [object_id]=OBJECT_ID(N'[Customer].[DynamicOptin]')))
Begin	
	Print 'Drop Index IX_DynamicOptIn_CustomerId ON [Customer].[DynamicOptin]'
	DROP INDEX [IX_DynamicOptIn_CustomerId] ON [Customer].[DynamicOptin]
End	
GO

declare @constraint_name nvarchar(128) = N''
SELECT @constraint_name = constraint_name
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
AND TABLE_NAME = 'DynamicOptin' AND TABLE_SCHEMA = 'Customer'

if isnull(len(@constraint_name),0) > 0
begin
	print 'Drop Primary key of Customer.DynamicOptin'
	declare @csql nvarchar(1000) = N'ALTER TABLE Customer.DynamicOptin DROP CONSTRAINT ['+@constraint_name+N']'
	exec sp_executesql @stmt = @cSql 
end	
GO

CREATE TABLE [Customer].[DynamicOptin_tmp](
	[CustomerDynamicOptinID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[DynamicOptInType] [varchar](32) NULL,
	[Status] [bit] NULL,
	[AcceptanceTimestamp] [datetime] NULL,
	[LastModification] [datetime] NULL DEFAULT (getutcdate()),
	[SerializedEncryptedValues] [varchar](2000) NULL,
	[EffectiveDate] [datetime] NULL,
	[ExpirationDate] [datetime] NULL,
 CONSTRAINT [PK_DynamicOptIn] PRIMARY KEY CLUSTERED 
(
	[CustomerDynamicOptinID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [FG1]
) ON [FG1]
GO

Print 'insert into [Customer].[DynamicOptin_tmp]'
set identity_insert [Customer].[DynamicOptin_tmp] ON
insert into [Customer].[DynamicOptin_tmp]
	 ([CustomerDynamicOptinID]
      ,[CustomerID]
      ,[DynamicOptInType]
      ,[Status]
      ,[AcceptanceTimestamp]
      ,[LastModification]
      ,[SerializedEncryptedValues]
      ,[EffectiveDate]
      ,[ExpirationDate])
SELECT [CustomerDynamicOptinID]
      ,[CustomerID]
      ,[DynamicOptInType]
      ,[Status]
      ,[AcceptanceTimestamp]
      ,[LastModification]
      ,[SerializedEncryptedValues]
      ,[EffectiveDate]
      ,[ExpirationDate]
FROM [Customer].[DynamicOptin]
set identity_insert [Customer].[DynamicOptin_tmp] OFF
GO

sp_rename 'Customer.DynamicOptin', 'DynamicOptin_old'
GO
sp_rename 'Customer.DynamicOptin_tmp', 'DynamicOptin'
GO

ALTER TABLE [Customer].[DynamicOptin]  WITH CHECK ADD  CONSTRAINT [FK_DynamicOptIn_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO

ALTER TABLE [Customer].[DynamicOptin] CHECK CONSTRAINT [FK_DynamicOptIn_Profile]
GO

Print 'Create index IX_DynamicOptIn_CustomerId on Customer.DynamicOptin'
CREATE NONCLUSTERED INDEX [IX_DynamicOptIn_CustomerId] ON [Customer].[DynamicOptin]
(
	[CustomerID] ASC,
	[DynamicOptInType] ASC
)
INCLUDE ( 	[Status],
	[AcceptanceTimestamp],
	[LastModification],
	[EffectiveDate],
	[ExpirationDate],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) 
--	ON [Customer_Index]
GO
--------------------------------------------------------------------------------------
/* Acceptance */
if exists (select 1 from sys.objects where name = 'FK_Profile_Acceptance' and type='F')
Begin
	print 'Drop FK_Profile_Acceptance foreign key'
	ALTER TABLE [Customer].[Acceptance] DROP CONSTRAINT [FK_Profile_Acceptance]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_Acceptance_Profile' and type='F')
Begin
	print 'Drop FK_Acceptance_Profile foreign key'
	ALTER TABLE [Customer].[Acceptance] DROP CONSTRAINT [FK_Acceptance_Profile]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Acceptance]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_Acceptance_CustomerID' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Acceptance]')))
Begin	
	Print 'Drop Index IX_Acceptance_CustomerID ON [Customer].[Acceptance]'
	DROP INDEX [IX_Acceptance_CustomerID] ON [Customer].[Acceptance]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Acceptance]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'Acceptance_CustomeridCustomeracceptanceid_INCL7_NU_NC' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Acceptance]')))
Begin	
	Print 'Drop Index Acceptance_CustomeridCustomeracceptanceid_INCL7_NU_NC ON [Customer].[Acceptance]'
	DROP INDEX [Acceptance_CustomeridCustomeracceptanceid_INCL7_NU_NC] ON [Customer].[Acceptance]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Acceptance]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_Acceptance_LastModification_CustomerID' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Acceptance]')))
Begin	
	Print 'Drop Index IX_Acceptance_LastModification_CustomerID ON [Customer].[Acceptance]'
	DROP INDEX [IX_Acceptance_LastModification_CustomerID] ON [Customer].[Acceptance]
End	
GO

declare @constraint_name nvarchar(128) = N''
SELECT @constraint_name = constraint_name
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
AND TABLE_NAME = 'Acceptance' AND TABLE_SCHEMA = 'Customer'

if isnull(len(@constraint_name),0) > 0
begin
	print 'Drop Primary key of Customer.Acceptance'
	declare @csql nvarchar(1000) = N'ALTER TABLE Customer.Acceptance DROP CONSTRAINT ['+@constraint_name+N']'
	exec sp_executesql @stmt = @cSql 
end	
GO

CREATE TABLE [Customer].[Acceptance_tmp](
	[CustomerAcceptanceID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[AcceptanceTypeID] [tinyint] NULL,
	[Description] [nvarchar](max) NULL,
	[AcceptedAt] [datetime] NULL,
	[AcceptanceFlag] [bit] NULL,
	[LastModification] [datetime] NOT NULL DEFAULT (getutcdate()),
	[SerializedEncryptedValues] [varchar](2000) NULL,
	[ApplicationID] [int] NULL,
 CONSTRAINT [PK_Acceptance] PRIMARY KEY CLUSTERED 
(
	[CustomerAcceptanceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [FG1]
) ON [FG1] 
GO

Print 'insert into [Customer].[Acceptance_tmp]'
set identity_insert [Customer].[Acceptance_tmp] ON
insert into [Customer].[Acceptance_tmp]
([CustomerAcceptanceID]
      ,[CustomerID]
      ,[AcceptanceTypeID]
      ,[Description]
      ,[AcceptedAt]
      ,[AcceptanceFlag]
      ,[LastModification]
      ,[SerializedEncryptedValues]
      ,[ApplicationID])
SELECT [CustomerAcceptanceID]
      ,[CustomerID]
      ,[AcceptanceTypeID]
      ,[Description]
      ,[AcceptedAt]
      ,[AcceptanceFlag]
      ,[LastModification]
      ,[SerializedEncryptedValues]
      ,[ApplicationID]
  FROM [Customer].[Acceptance]
set identity_insert [Customer].[Acceptance_tmp] OFF
GO

sp_rename 'Customer.Acceptance', 'Acceptance_old'
GO
sp_rename 'Customer.Acceptance_tmp', 'Acceptance'
GO

ALTER TABLE [Customer].[Acceptance]  WITH CHECK ADD  CONSTRAINT [FK_Profile_Acceptance] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[Acceptance] CHECK CONSTRAINT [FK_Profile_Acceptance]
GO

Print 'Create Index IX_Acceptance_CustomerID on Customer.Acceptance'
CREATE NONCLUSTERED INDEX [IX_Acceptance_CustomerID] ON [Customer].[Acceptance]
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	on customer_index
GO

Print 'Create index Acceptance_CustomeridCustomeracceptanceid_INCL7_NU_NC on Customer.Acceptance'
CREATE NONCLUSTERED INDEX [Acceptance_CustomeridCustomeracceptanceid_INCL7_NU_NC] ON [Customer].[Acceptance]
(
	[CustomerID] ASC,
	[CustomerAcceptanceID] ASC
)
INCLUDE ( 	[AcceptanceTypeID],
	[Description],
	[AcceptedAt],
	[AcceptanceFlag],
	[LastModification],
	[SerializedEncryptedValues],
	[ApplicationID]) 
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	on customer_index

/* create it if it necessary, we didn't create it yet, currently not used in PERF9 
CREATE NONCLUSTERED INDEX [IX_Acceptance_LastModification_CustomerID] ON [Customer].[Acceptance]
(
	[LastModification] ASC,
	[CustomerID] ASC
)
INCLUDE ( 	[CustomerAcceptanceID],
	[AcceptanceTypeID],
	[Description],
	[AcceptedAt],
	[AcceptanceFlag],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [Customer_Index]
*/
----------------------------------------------------------------------------------------
/* Subscription */
if exists (select 1 from sys.objects where name = 'FK_Subscription_Profile' and type='F')
Begin
	print 'Drop FK_Subscription_Profile foreign key'
	ALTER TABLE [Customer].[Subscription] DROP CONSTRAINT [FK_Subscription_Profile]
End	
GO

declare @constraint_name nvarchar(128) = N''
declare curDefConstraints cursor for
	select name from sys.objects where parent_object_id = object_id('customer.Subscription') 
	and type ='D'
open curDefConstraints
fetch next from curDefConstraints into @constraint_name
while @@fetch_status = 0
begin
	print 'Drop '+@constraint_name +' of Customer.Subscription'
	declare @csql nvarchar(1000) = N'ALTER TABLE Customer.Subscription DROP CONSTRAINT ['+@constraint_name+N']'
--	print @csql
	fetch next from curDefConstraints into @constraint_name
	exec sp_executesql @stmt = @cSql 
end	
close curDefConstraints
deallocate curDefConstraints
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Subscription]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_Subscription_CustomerID' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Subscription]')))
Begin	
	Print 'Drop Index IX_Subscription_CustomerID ON [Customer].[Subscription]'
	DROP INDEX [IX_Subscription_CustomerID] ON [Customer].[Subscription]
End	
GO

declare @constraint_name nvarchar(128) = N''
SELECT @constraint_name = constraint_name
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
AND TABLE_NAME = 'Subscription' AND TABLE_SCHEMA = 'Customer'

if isnull(len(@constraint_name),0) > 0
begin
	print 'Drop Primary key of Customer.Subscription'
	declare @csql nvarchar(1000) = N'ALTER TABLE Customer.Subscription DROP CONSTRAINT ['+@constraint_name+N']'
	exec sp_executesql @stmt = @cSql 
end	
GO

CREATE TABLE [Customer].[Subscription_tmp](
	[CustomerSubscriptionID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[SubscriptionTypeID] [tinyint] NULL,
	[Description] [nvarchar](max) NULL,
	[SubscribedAt] [datetime] NULL DEFAULT (getutcdate()),
	[SubscriptionFlag] [bit] NULL,
	[LastModification] [datetime] NOT NULL DEFAULT (getutcdate()),
	[SerializedEncryptedValues] [varchar](2000) NULL,
 CONSTRAINT [PK_Subscription] PRIMARY KEY CLUSTERED 
(
	[CustomerSubscriptionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [FG1]
) ON [FG1]
GO

Print 'insert into [Customer].[Subscription_tmp]'
set identity_insert [Customer].[Subscription_tmp] ON
insert into [Customer].[Subscription_tmp]
([CustomerSubscriptionID]
      ,[CustomerID]
      ,[SubscriptionTypeID]
      ,[Description]
      ,[SubscribedAt]
      ,[SubscriptionFlag]
      ,[LastModification]
      ,[SerializedEncryptedValues])
SELECT [CustomerSubscriptionID]
      ,[CustomerID]
      ,[SubscriptionTypeID]
      ,[Description]
      ,[SubscribedAt]
      ,[SubscriptionFlag]
      ,[LastModification]
      ,[SerializedEncryptedValues]
  FROM [Customer].[Subscription]
set identity_insert [Customer].[Subscription_tmp] OFF
GO
sp_rename 'Customer.Subscription', 'Subscription_old'
GO
sp_rename 'Customer.Subscription_tmp', 'Subscription'
GO

ALTER TABLE [Customer].[Subscription]  WITH CHECK ADD  CONSTRAINT [FK_Subscription_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO

ALTER TABLE [Customer].[Subscription] CHECK CONSTRAINT [FK_Subscription_Profile]
GO

ALTER TABLE [Customer].[Subscription] ADD  CONSTRAINT [DF_Subscription_SubscribedAt]  DEFAULT (getutcdate()) FOR [SubscribedAt]
GO

ALTER TABLE [Customer].[Subscription] ADD  CONSTRAINT [DF_Subscription_LastModification]  DEFAULT (getutcdate()) FOR [LastModification]
GO

Print 'Create Index [IX_Subscription_CustomerID] ON [Customer].[Subscription]'
CREATE NONCLUSTERED INDEX [IX_Subscription_CustomerID] ON [Customer].[Subscription]
(
	[CustomerID] ASC
)
INCLUDE ( 	[CustomerSubscriptionID],
	[SubscriptionTypeID],
	[Description],
	[SubscribedAt],
	[SubscriptionFlag],
	[LastModification],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	on Customer_Index
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Subscription]') AND [type]='U')) AND 
	NOT (EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_Subscription_LastModification_CustomerID' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Subscription]')))
Begin	
	Print 'Create Index IX_Subscription_LastModification_CustomerID ON [Customer].[Subscription]'
	CREATE NONCLUSTERED INDEX [IX_Subscription_LastModification_CustomerID] ON [Customer].[Subscription]
	(
		[LastModification] ASC,
		[CustomerID] ASC
	)
	INCLUDE ( 	[CustomerSubscriptionID],
		[SubscriptionTypeID],
		[Description],
		[SubscribedAt],
		[SubscriptionFlag],
		[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) 
--	ON [Customer_Index]
End	
----------------------------------------------------------------------------------------
/* Preference */

if exists (select 1 from sys.objects where name = 'FK_Preference_Profile' and type='F')
Begin
	print 'Drop FK_Preference_Profile foreign key'
	ALTER TABLE [Customer].[Preference] DROP CONSTRAINT [FK_Preference_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_Profile_Preference' and type='F')
Begin
	print 'Drop FK_Profile_Preference foreign key'
	ALTER TABLE [Customer].[Preference] DROP CONSTRAINT [FK_Profile_Preference]
End	
GO

if exists (select 1 from sys.objects where name = 'DF_Preference_LastModification' and type='D')
Begin
	print 'Drop DF_Preference_LastModification constraint'
	ALTER TABLE [Customer].[Preference] DROP CONSTRAINT [DF_Preference_LastModification]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Preference]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_Preference_CustomerID' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Preference]')))
Begin	
	Print 'Drop Index IX_Preference_CustomerID ON [Customer].[Preference]'
	DROP INDEX [IX_Preference_CustomerID] ON [Customer].[Preference]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Preference]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_Preference_LastModification_CustomerID' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Preference]')))
Begin	
	Print 'Drop Index IX_Preference_LastModification_CustomerID ON [Customer].[Preference]'
	DROP INDEX [IX_Preference_LastModification_CustomerID] ON [Customer].[Preference]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Preference]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'Preference_Customerid_INCL1_NU_NC' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Preference]')))
Begin	
	Print 'Drop Index Preference_Customerid_INCL1_NU_NC ON [Customer].[Preference]'
	DROP INDEX [Preference_Customerid_INCL1_NU_NC] ON [Customer].[Preference]
End	
GO

declare @constraint_name nvarchar(128) = N''
SELECT @constraint_name = constraint_name
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
AND TABLE_NAME = 'Preference' AND TABLE_SCHEMA = 'Customer'

if isnull(len(@constraint_name),0) > 0
begin
	print 'Drop Primary key of Customer.Preference'
	declare @csql nvarchar(1000) = N'ALTER TABLE Customer.Preference DROP CONSTRAINT ['+@constraint_name+N']'
	exec sp_executesql @stmt = @cSql 
end	
GO

CREATE TABLE [Customer].[Preference_tmp](
	[CustomerPreferenceID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[PreferenceTypeID] [tinyint] NULL,
	[NotificationMediumID] [tinyint] NULL,
	[PreferenceValue] [nvarchar](50) NULL,
	[PreferenceFlag] [bit] NULL,
	[LastModification] [datetime] NOT NULL ,
	[SerializedEncryptedValues] [varchar](3400) NULL,
 CONSTRAINT [PK_Preference] PRIMARY KEY CLUSTERED 
(
	[CustomerPreferenceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [Customer_Data]
) ON [Customer_Data]
GO

ALTER TABLE [Customer].[Preference] ADD  CONSTRAINT [DF_Preference_LastModification]  DEFAULT (getutcdate()) FOR [LastModification]
GO

Print 'insert into [Customer].[Preference_tmp]'
set identity_insert [Customer].[Preference_tmp] ON
insert into [Customer].[Preference_tmp]
([CustomerPreferenceID]
      ,[CustomerID]
      ,[PreferenceTypeID]
      ,[NotificationMediumID]
      ,[PreferenceValue]
      ,[PreferenceFlag]
      ,[LastModification]
      ,[SerializedEncryptedValues])
SELECT [CustomerPreferenceID]
      ,[CustomerID]
      ,[PreferenceTypeID]
      ,[NotificationMediumID]
      ,[PreferenceValue]
      ,[PreferenceFlag]
      ,[LastModification]
      ,[SerializedEncryptedValues]
  FROM [Customer].[Preference]
set identity_insert [Customer].[Preference_tmp] OFF
GO

sp_rename 'Customer.Preference', 'Preference_old'
GO
sp_rename 'Customer.Preference_tmp', 'Preference'
GO

ALTER TABLE [Customer].[Preference]  WITH CHECK ADD  CONSTRAINT [FK_Preference_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[Preference] CHECK CONSTRAINT [FK_Preference_Profile]
GO

Print 'Create index [IX_Preference_CustomerID] ON [Customer].[Preference]'
CREATE NONCLUSTERED INDEX [IX_Preference_CustomerID] ON [Customer].[Preference]
(
	[CustomerID] ASC
)
INCLUDE ( 	[PreferenceTypeID],
	[NotificationMediumID],
	[PreferenceValue],
	[PreferenceFlag],
	[LastModification],
	[SerializedEncryptedValues],
	[CustomerPreferenceID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	ON Customer_Index	
GO

Print 'Create index [Preference_Customerid_INCL1_NU_NC] ON [Customer].[Preference]'
CREATE NONCLUSTERED INDEX [Preference_Customerid_INCL1_NU_NC] ON [Customer].[Preference]
(
	[CustomerID] ASC
)
INCLUDE ( 	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	ON Customer_Index
GO

Print 'Create index [IX_Preference_LastModification_CustomerID] ON [Customer].[Preference]'
CREATE NONCLUSTERED INDEX [IX_Preference_LastModification_CustomerID] ON [Customer].[Preference]
(
	[LastModification] ASC,
	[CustomerID] ASC
)
INCLUDE ( 	[CustomerPreferenceID],
	[PreferenceTypeID],
	[NotificationMediumID],
	[PreferenceValue],
	[PreferenceFlag],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) 
--	ON [Customer_Index]
GO
----------------------------------------------------------------------------------------------
/* CustomerPaymentMethod */

if exists (select 1 from sys.objects where name = 'FK_CustomerPaymentMethod_Profile' and type='F')
Begin
	print 'Drop FK_CustomerPaymentMethod_Profile foreign key'
	ALTER TABLE [Customer].[CustomerPaymentMethod] DROP CONSTRAINT [FK_CustomerPaymentMethod_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_Profile_CustomerPaymentMethod' and type='F')
Begin
	print 'Drop FK_Profile_CustomerPaymentMethod foreign key'
	ALTER TABLE [Customer].[CustomerPaymentMethod] DROP CONSTRAINT [FK_Profile_CustomerPaymentMethod]
End	
GO

if exists (select 1 from sys.objects where name = 'DF_CustomerPaymentMethod_LastModification' and type='D')
Begin
	print 'Drop DF_CustomerPaymentMethod_LastModification constraint'
	ALTER TABLE [Customer].[CustomerPaymentMethod] DROP CONSTRAINT [DF_CustomerPaymentMethod_LastModification]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[CustomerPaymentMethod]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_CustomerPayment_LastModification_CustomerID' 
	AND [object_id]=OBJECT_ID(N'[Customer].[CustomerPaymentMethod]')))
Begin	
	Print 'Drop Index IX_CustomerPayment_LastModification_CustomerID ON [Customer].[CustomerPaymentMethod]'
	DROP INDEX [IX_CustomerPayment_LastModification_CustomerID] ON [Customer].[CustomerPaymentMethod]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[CustomerPaymentMethod]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_CustomerPaymentMethod_CustomerID' 
	AND [object_id]=OBJECT_ID(N'[Customer].[CustomerPaymentMethod]')))
Begin	
	Print 'Drop Index IX_CustomerPaymentMethod_CustomerID ON [Customer].[CustomerPaymentMethod]'
	DROP INDEX [IX_CustomerPaymentMethod_CustomerID] ON [Customer].[CustomerPaymentMethod]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[CustomerPaymentMethod]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_CustomerPaymentMethod_OneTimePaymentIsValid' 
	AND [object_id]=OBJECT_ID(N'[Customer].[CustomerPaymentMethod]')))
Begin	
	Print 'Drop Index IX_CustomerPaymentMethod_OneTimePaymentIsValid ON [Customer].[CustomerPaymentMethod]'
	DROP INDEX [IX_CustomerPaymentMethod_OneTimePaymentIsValid] ON [Customer].[CustomerPaymentMethod]
End	
GO

declare @constraint_name nvarchar(128) = N''
SELECT @constraint_name = constraint_name
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
AND TABLE_NAME = 'CustomerPaymentMethod' AND TABLE_SCHEMA = 'Customer'

if isnull(len(@constraint_name),0) > 0
begin
	print 'Drop Primary key of Customer.CustomerPaymentMethod'
	declare @csql nvarchar(1000) = N'ALTER TABLE Customer.CustomerPaymentMethod DROP CONSTRAINT ['+@constraint_name+N']'
	exec sp_executesql @stmt = @cSql 
end	
GO

CREATE TABLE [Customer].[CustomerPaymentMethod_tmp](
	[CustomerPaymentMethodID] [int] IDENTITY(1,1) NOT NULL,
	[PaymentMethod] [nvarchar](128) NULL,
	[CustomerID] [int] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[LastModification] [datetime] NOT NULL,
	[OneTimePayment] [bit] NOT NULL,
	[IsDefault] [bit] NOT NULL,
	[NickName] [nvarchar](128) NULL,
	[PaymentIntegrator] [nvarchar](128) NULL,
	[Expiration] [nvarchar](128) NULL,
	[IsValid] [bit] NOT NULL,
	[CardAlias] [nvarchar](128) NULL,
	[CardExpiration] [nvarchar](128) NULL,
	[CardToken] [nvarchar](128) NULL,
	[CardHolderName] [nvarchar](128) NULL,
	[CVVConfirmed] [bit] NULL,
	[PaymentMode] [nvarchar](128) NOT NULL,
	[AccountID] [nvarchar](128) NULL,
	[SerializedEncryptedValues] [varchar](2000) NULL,
	ExternalPaymentId nvarchar(128) null,
 CONSTRAINT [PK_CustomerPaymentMethod] PRIMARY KEY CLUSTERED 
(
	[CustomerPaymentMethodID] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [fg1]
) ON [fg1] 
GO

Print 'insert into [Customer].[CustomerPaymentMethod_tmp]'
set identity_insert [Customer].[CustomerPaymentMethod_tmp] ON
insert into [Customer].[CustomerPaymentMethod_tmp]
	  ([CustomerPaymentMethodID]
      ,[PaymentMethod]
      ,[CustomerID]
      ,[IsEnabled]
      ,[LastModification]
      ,[OneTimePayment]
      ,[IsDefault]
      ,[NickName]
      ,[PaymentIntegrator]
      ,[Expiration]
      ,[IsValid]
      ,[CardAlias]
      ,[CardExpiration]
      ,[CardToken]
      ,[CardHolderName]
      ,[CVVConfirmed]
      ,[PaymentMode]
      ,[AccountID]
      ,[SerializedEncryptedValues]
	  ,ExternalPaymentId)
Select [CustomerPaymentMethodID]
      ,[PaymentMethod]
      ,[CustomerID]
      ,[IsEnabled]
      ,[LastModification]
      ,[OneTimePayment]
      ,[IsDefault]
      ,[NickName]
      ,[PaymentIntegrator]
      ,[Expiration]
      ,[IsValid]
      ,[CardAlias]
      ,[CardExpiration]
      ,[CardToken]
      ,[CardHolderName]
      ,[CVVConfirmed]
      ,[PaymentMode]
      ,[AccountID]
      ,[SerializedEncryptedValues]
	  ,ExternalPaymentId
  FROM [Customer].[CustomerPaymentMethod]
set identity_insert [Customer].[CustomerPaymentMethod_tmp] OFF
GO

sp_rename 'Customer.CustomerPaymentMethod', 'CustomerPaymentMethod_old'
GO
sp_rename 'Customer.CustomerPaymentMethod_tmp', 'CustomerPaymentMethod'
GO

ALTER TABLE [Customer].[CustomerPaymentMethod] ADD  CONSTRAINT [DF_CustomerPaymentMethod_LastModification]  DEFAULT (getutcdate()) FOR [LastModification]
GO

ALTER TABLE [Customer].[CustomerPaymentMethod]  WITH CHECK ADD  CONSTRAINT [FK_CustomerPaymentMethod_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO

ALTER TABLE [Customer].[CustomerPaymentMethod] CHECK CONSTRAINT [FK_CustomerPaymentMethod_Profile]
GO

Print 'Create index [IX_CustomerPayment_LastModification_CustomerID] ON [Customer].[CustomerPaymentMethod]'
CREATE NONCLUSTERED INDEX [IX_CustomerPayment_LastModification_CustomerID] ON [Customer].[CustomerPaymentMethod]
(
	[LastModification] ASC,
	[CustomerID] ASC
)
INCLUDE ( 	[CustomerPaymentMethodID],
	[PaymentMethod],
	[IsEnabled],
	[OneTimePayment],
	[IsDefault],
	[NickName],
	[PaymentIntegrator],
	[Expiration],
	[IsValid],
	[CardAlias],
	[CardExpiration],
	[CardToken],
	[CardHolderName],
	[CVVConfirmed],
	[PaymentMode],
	[AccountID],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	on customer_index
GO

Print 'Create index [IX_CustomerPaymentMethod_CustomerID] ON [Customer].[CustomerPaymentMethod]'
CREATE NONCLUSTERED INDEX [IX_CustomerPaymentMethod_CustomerID] ON [Customer].[CustomerPaymentMethod]
(
	[CustomerID] ASC
)
INCLUDE ( 	[CustomerPaymentMethodID],
	[PaymentMethod],
	[IsEnabled],
	[LastModification],
	[OneTimePayment],
	[IsDefault],
	[NickName],
	[PaymentIntegrator],
	[Expiration],
	[IsValid],
	[CardAlias],
	[CardExpiration],
	[CardToken],
	[CardHolderName],
	[CVVConfirmed],
	[PaymentMode],
	[AccountID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	on customer_index
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[CustomerPaymentMethod]') AND [type]='U')) AND 
	NOT (EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'CustomerPaymentMethod_OnetimepaymentIsvalid_INCL1_FLT_NU_NC' 
	AND [object_id]=OBJECT_ID(N'[Customer].[CustomerPaymentMethod]')))
Begin	
	Print 'Create Index CustomerPaymentMethod_OnetimepaymentIsvalid_INCL1_FLT_NU_NC ON [Customer].[CustomerPaymentMethod]'
	CREATE NONCLUSTERED INDEX [CustomerPaymentMethod_OnetimepaymentIsvalid_INCL1_FLT_NU_NC] ON [Customer].[CustomerPaymentMethod]
	(
		[OneTimePayment] ASC,
		[IsValid] ASC
	)
	INCLUDE ( 	[CustomerPaymentMethodID]) 
	WHERE ([OneTimePayment]=(1) AND [IsValid]=(0))
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	on customer_index
End	
GO

/* code has been changed, we didn't crete this index  
CREATE NONCLUSTERED INDEX [IX_CustomerPaymentMethod_OneTimePaymentIsValid] ON [Customer].[CustomerPaymentMethod]
(
	[OneTimePayment] ASC,
	[IsValid] ASC
)
INCLUDE ( 	[CustomerPaymentMethodID],
	[PaymentMethod],
	[CustomerID],
	[IsEnabled],
	[LastModification],
	[IsDefault],
	[NickName],
	[PaymentIntegrator],
	[Expiration],
	[CardAlias],
	[CardExpiration],
	[CardToken],
	[CardHolderName],
	[CVVConfirmed],
	[PaymentMode],
	[AccountID],
	[SerializedEncryptedValues]) 
WHERE ([OneTimePayment]=(1) AND [IsValid]=(0))
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) 
ON [Customer_Index]
*/
-----------------------------------------------------------------------------------------
/* Favorite */
if exists (select 1 from sys.objects where name = 'FK_Favorite_Profile' and type='F')
Begin
	print 'Drop FK_Favorite_Profile foreign key'
	ALTER TABLE [Customer].[Favorite] DROP CONSTRAINT [FK_Favorite_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_Favorite_FavoriteType' and type='F')
Begin
	print 'Drop FK_Favorite_FavoriteType foreign key'
	ALTER TABLE [Customer].[Favorite] DROP CONSTRAINT [FK_Favorite_FavoriteType]
End	
GO

if exists (select 1 from sys.objects where name = 'DF_Favorite_LastModifiction' and type='D')
Begin
	print 'Drop DF_Favorite_LastModifiction constraint'
	ALTER TABLE [Customer].[Favorite] DROP CONSTRAINT [DF_Favorite_LastModifiction]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Favorite]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_Favorite_LastModification_CustomerID' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Favorite]')))
Begin	
	Print 'Drop Index IX_Favorite_LastModification_CustomerID ON [Customer].[Favorite]'
	DROP INDEX [IX_Favorite_LastModification_CustomerID] ON [Customer].[Favorite]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Favorite]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_Favorite_CustomerID' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Favorite]')))
Begin	
	Print 'Drop Index IX_Favorite_CustomerID ON [Customer].[Favorite]'
	DROP INDEX [IX_Favorite_CustomerID] ON [Customer].[Favorite]
End	
GO

declare @constraint_name nvarchar(128) = N''
SELECT @constraint_name = constraint_name
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
AND TABLE_NAME = 'Favorite' AND TABLE_SCHEMA = 'Customer'

if isnull(len(@constraint_name),0) > 0
begin
	print 'Drop Primary key of Customer.Favorite'
	declare @csql nvarchar(1000) = N'ALTER TABLE Customer.Favorite DROP CONSTRAINT ['+@constraint_name+N']'
	exec sp_executesql @stmt = @cSql 
end	
GO

CREATE TABLE [Customer].[Favorite_tmp](
	[CustomerFavoriteID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[Type] [int] NULL,
	[Name] [nvarchar](128) NULL,
	[Data] [nvarchar](2000) NULL,
	[LastModification] [datetime] NOT NULL,
	[SerializedEncryptedValues] [varchar](max) NULL,
 CONSTRAINT [PK_Favorite] PRIMARY KEY CLUSTERED 
(
	[CustomerFavoriteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [fg1]
) ON [fg1] TEXTIMAGE_ON [fg1]
GO

Print 'insert into [Customer].[Favorite_tmp]'
set identity_insert [Customer].[Favorite_tmp] ON
insert into [Customer].[Favorite_tmp]
	  ([CustomerFavoriteID]
      ,[CustomerID]
      ,[Type]
      ,[Name]
      ,[Data]
      ,[LastModification]
      ,[SerializedEncryptedValues])
SELECT [CustomerFavoriteID]
      ,[CustomerID]
      ,[Type]
      ,[Name]
      ,[Data]
      ,[LastModification]
      ,[SerializedEncryptedValues]
  FROM [Customer].[Favorite]
GO
set identity_insert [Customer].[Favorite_tmp] OFF
GO

sp_rename 'Customer.Favorite', 'Favorite_old'
GO
sp_rename 'Customer.Favorite_tmp', 'Favorite'
GO

ALTER TABLE [Customer].[Favorite] ADD  CONSTRAINT [DF_Favorite_LastModifiction]  DEFAULT (getutcdate()) FOR [LastModification]
GO

ALTER TABLE [Customer].[Favorite]  WITH CHECK ADD  CONSTRAINT [FK_Favorite_FavoriteType] FOREIGN KEY([Type])
REFERENCES [Dictionary].[FavoriteType] ([FavoriteTypeID])
GO

ALTER TABLE [Customer].[Favorite] CHECK CONSTRAINT [FK_Favorite_FavoriteType]
GO

ALTER TABLE [Customer].[Favorite]  WITH CHECK ADD  CONSTRAINT [FK_Favorite_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO

ALTER TABLE [Customer].[Favorite] CHECK CONSTRAINT [FK_Favorite_Profile]
GO

Print 'Create index [IX_Favorite_LastModification_CustomerID] ON [Customer].[Favorite]'
CREATE NONCLUSTERED INDEX [IX_Favorite_LastModification_CustomerID] ON [Customer].[Favorite]
(
	[LastModification] ASC,
	[CustomerID] ASC
)
INCLUDE ( 	[CustomerFavoriteID],
	[Type],
	[Name],
	[Data],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	on customer_index
GO

/* we didn't create this index yet, code has been changed
CREATE NONCLUSTERED INDEX [IX_Favorite_CustomerID] ON [Customer].[Favorite]
(
	[CustomerID] ASC
)
include ( Type, Name, Data, Lastmodification, SerilizedEncryptedValues )
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
on customer_index
GO
*/
---------------------------------------------------------------------------------------------
/* AuthToken */

if exists (select 1 from sys.objects where name = 'FK_AuthToken_Profile' and type='F')
Begin
	print 'Drop FK_AuthToken_Profile foreign key'
	ALTER TABLE [Customer].[AuthToken] DROP CONSTRAINT [FK_AuthToken_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'DF_AuthTOken_LastModification' and type='D')
Begin
	print 'Drop DF_AuthTOken_LastModification constraint'
	ALTER TABLE [Customer].[AuthToken] DROP CONSTRAINT [DF_AuthTOken_LastModification]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Favorite]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_AuthToken_AuthTokenID' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Favorite]')))
Begin	
	Print 'Drop Index IX_AuthToken_AuthTokenID ON [Customer].[Favorite]'
	DROP INDEX [IX_AuthToken_AuthTokenID] ON [Customer].[Favorite]
End	
GO

declare @constraint_name nvarchar(128) = N''
SELECT @constraint_name = constraint_name
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
AND TABLE_NAME = 'AuthToken' AND TABLE_SCHEMA = 'Customer'

if isnull(len(@constraint_name),0) > 0
begin
	print 'Drop Primary key of Customer.AuthToken'
	declare @csql nvarchar(1000) = N'ALTER TABLE Customer.AuthToken DROP CONSTRAINT ['+@constraint_name+N']'
	exec sp_executesql @stmt = @cSql 
end	
GO

CREATE TABLE [Customer].[AuthToken_tmp](
	[CustomerID] [int] NOT NULL,
	[AuthTokenID] [uniqueidentifier] NOT NULL,
	[Provider] [nvarchar](128) NULL,
	[LastModification] [datetime] NOT NULL ,
	[SerializedEncryptedValues] [varchar](2000) NULL,
 CONSTRAINT [PK_AuthToken] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC,
	[AuthTokenID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON fg1
) ON fg1
GO

Print 'insert into [Customer].[AuthToken_tmp]'
insert into [Customer].[AuthToken_tmp]
	 ([CustomerID]
      ,[AuthTokenID]
      ,[Provider]
      ,[LastModification]
      ,[SerializedEncryptedValues])
SELECT [CustomerID]
      ,[AuthTokenID]
      ,[Provider]
      ,[LastModification]
      ,[SerializedEncryptedValues]
  FROM [Customer].[AuthToken]

sp_rename 'Customer.AuthToken', 'AuthToken_old'
GO
sp_rename 'Customer.AuthToken_tmp', 'AuthToken'
GO

ALTER TABLE [Customer].[AuthToken] ADD  CONSTRAINT [DF_AuthTOken_LastModification]  DEFAULT (getutcdate()) FOR [LastModification]
GO

ALTER TABLE [Customer].[AuthToken]  WITH CHECK ADD  CONSTRAINT [FK_AuthToken_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[AuthToken] CHECK CONSTRAINT [FK_AuthToken_Profile]
GO

Print 'Create index [IX_AuthToken_AuthTokenID] ON [Customer].[AuthToken]'
CREATE NONCLUSTERED INDEX [IX_AuthToken_AuthTokenID] ON [Customer].[AuthToken]
(
	[AuthTokenID] ASC
)
INCLUDE ( 	[CustomerID],
	[Provider],
	[LastModification],
	[SerializedEncryptedValues]) 
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	on customer_index
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[AuthToken]') AND [type]='U')) AND 
	NOT (EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'AuthToken_Customerid_NU_NC' 
	AND [object_id]=OBJECT_ID(N'[Customer].[AuthToken]')))
Begin	
	Print 'Create Index AuthToken_Customerid_NU_NC ON [Customer].[AuthToken]'
	CREATE NONCLUSTERED INDEX [AuthToken_Customerid_NU_NC] ON [Customer].[AuthToken]
	(
		[CustomerID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	ON customer_index
End	
GO
-----------------------------------------------------------------------------------
/* Address */

if exists (select 1 from sys.objects where name = 'FK_Address_Profile' and type='F')
Begin
	print 'Drop FK_Address_Profile foreign key'
	ALTER TABLE [Customer].[Address] DROP CONSTRAINT [FK_Address_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_Profile_AddressPhoneLookup' and type='F')
Begin
	print 'Drop FK_Profile_AddressPhoneLookup foreign key'
	ALTER TABLE [Customer].[AddressPhoneLookup] DROP CONSTRAINT [FK_Profile_AddressPhoneLookup]
End	
GO

declare @constraint_name nvarchar(128) = N''
SELECT @constraint_name = constraint_name
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
AND TABLE_NAME = 'Address' AND TABLE_SCHEMA = 'Customer'

if isnull(len(@constraint_name),0) > 0
begin
	print 'Drop Primary key of Customer.Address'
	declare @csql nvarchar(1000) = N'ALTER TABLE Customer.Address DROP CONSTRAINT ['+@constraint_name+N']'
	exec sp_executesql @stmt = @cSql 
end	
GO

CREATE TABLE [Customer].[Address_tmp](
	[CustomerAddressID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[AddressTypeID] [nvarchar](10) NULL,
	[AllowPromotions] [bit] NULL,
	[Preference] [int] NULL,
	[Address] [nvarchar](max) NULL,
	[Phones] [nvarchar](max) NULL,
	[Version] [nvarchar](50) NULL,
	[SerializedEncryptedValues] [varchar](2000) NULL,
 CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED 
(
	[CustomerAddressID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [fg1]
) ON [fg1]
GO

Print 'insert into [Customer].[Address_tmp]'
set identity_insert [Customer].[Address_tmp] ON
insert into [Customer].[Address_tmp]
([CustomerAddressID]
      ,[CustomerID]
      ,[AddressTypeID]
      ,[AllowPromotions]
      ,[Preference]
      ,[Address]
      ,[Phones]
      ,[Version]
      ,[SerializedEncryptedValues])
SELECT [CustomerAddressID]
      ,[CustomerID]
      ,[AddressTypeID]
      ,[AllowPromotions]
      ,[Preference]
      ,[Address]
      ,[Phones]
      ,[Version]
      ,[SerializedEncryptedValues]
  FROM [Customer].[Address]
set identity_insert [Customer].[Address_tmp] OFF
GO

sp_rename 'Customer.Address', 'Address_old'
GO
sp_rename 'Customer.Address_tmp', 'Address'
GO

ALTER TABLE [Customer].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[Address] CHECK CONSTRAINT [FK_Address_Profile]
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Address]') AND [type]='U')) AND 
	NOT (EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'Address_CustomeridCustomeraddressid_INCL7_NU_NC' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Address]')))
Begin	
	Print 'Create Index Address_CustomeridCustomeraddressid_INCL7_NU_NC ON [Customer].[Address]'
	CREATE NONCLUSTERED INDEX [Address_CustomeridCustomeraddressid_INCL7_NU_NC] ON [Customer].[Address]
	(
		[CustomerID] ASC,
		[CustomerAddressID] ASC
	)
	INCLUDE ( 	[AddressTypeID],
		[AllowPromotions],
		[Preference],
		[Address],
		[Phones],
		[Version],
		[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	on customer_index
End
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Address]') AND [type]='U')) AND 
	NOT (EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'Address_CustomerID_NU_NC' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Address]')))
Begin	
	Print 'Create Index Address_CustomerID_NU_NC ON [Customer].[Address]'

	CREATE NONCLUSTERED INDEX [Address_CustomerID_NU_NC] ON [Customer].[Address]
	(
		[CustomerID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90)
--	ON Customer_Index
End	
GO

ALTER TABLE [Customer].[AddressPhoneLookup]  WITH CHECK ADD  CONSTRAINT [FK_Profile_AddressPhoneLookup] FOREIGN KEY([CustomerAddressID])
REFERENCES [Customer].[Address] ([CustomerAddressID])
GO
ALTER TABLE [Customer].[AddressPhoneLookup] CHECK CONSTRAINT [FK_Profile_AddressPhoneLookup]
GO
------------------------------------------------------------------------------------------
/* profile */
/* two noname def constraint have been named */

if exists (select 1 from sys.objects where name = 'FK_TINData_Profile' and type='F')
Begin
	print 'Drop FK_TINData_Profile foreign key'
	ALTER TABLE [Customer].[TINData] DROP CONSTRAINT [FK_TINData_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_SocialNetwork_Profile' and type='F')
Begin
	print 'Drop FK_SocialNetwork_Profile foreign key'
	ALTER TABLE [Customer].[SocialNetwork] DROP CONSTRAINT [FK_SocialNetwork_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_Preference_Profile' and type='F')
Begin
	print 'Drop FK_Preference_Profile foreign key'
	ALTER TABLE [Customer].[Preference] DROP CONSTRAINT [FK_Preference_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_OptIn_Profile' and type='F')
Begin
	print 'Drop FK_OptIn_Profile foreign key'
	ALTER TABLE [Customer].[Optin] DROP CONSTRAINT [FK_OptIn_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_MyLocation_Profile' and type='F')
Begin
	print 'Drop FK_MyLocation_Profile foreign key'
	ALTER TABLE [Customer].[MyLocation] DROP CONSTRAINT [FK_MyLocation_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_MobileDevices_Profile' and type='F')
Begin
	print 'Drop FK_MobileDevices_Profile foreign key'
	ALTER TABLE [Customer].[MobileDevices] DROP CONSTRAINT [FK_MobileDevices_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_LoyaltyMembershipNumber_Profile' and type='F')
Begin
	print 'Drop FK_LoyaltyMembershipNumber_Profile foreign key'
	ALTER TABLE [Customer].[LoyaltyMembershipNumber] DROP CONSTRAINT [FK_LoyaltyMembershipNumber_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_Favorite_Profile' and type='F')
Begin
	print 'Drop FK_Favorite_Profile foreign key'
	ALTER TABLE [Customer].[Favorite] DROP CONSTRAINT [FK_Favorite_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_DynamicOptIn_Profile' and type='F')
Begin
	print 'Drop FK_DynamicOptIn_Profile foreign key'
	ALTER TABLE [Customer].[DynamicOptin] DROP CONSTRAINT [FK_DynamicOptIn_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_CustomerPaymentMethod_Profile' and type='F')
Begin
	print 'Drop FK_CustomerPaymentMethod_Profile foreign key'
	ALTER TABLE [Customer].[CustomerPaymentMethod] DROP CONSTRAINT [FK_CustomerPaymentMethod_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_CustomerLookup_Profile' and type='F')
Begin
	print 'Drop FK_CustomerLookup_Profile foreign key'
	ALTER TABLE [Customer].[CustomerLookup] DROP CONSTRAINT [FK_CustomerLookup_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_Profile_CustomerLookup' and type='F')
Begin
	print 'Drop FK_Profile_CustomerLookup foreign key'
	ALTER TABLE [Customer].[CustomerLookup] DROP CONSTRAINT [FK_Profile_CustomerLookup]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_AuthToken_Profile' and type='F')
Begin
	print 'Drop FK_AuthToken_Profile foreign key'
	ALTER TABLE [Customer].[AuthToken] DROP CONSTRAINT [FK_AuthToken_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_Address_Profile' and type='F')
Begin
	print 'Drop FK_Address_Profile foreign key'
	ALTER TABLE [Customer].[Address] DROP CONSTRAINT [FK_Address_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_Acceptance_Profile' and type='F')
Begin
	print 'Drop FK_Acceptance_Profile foreign key'
	ALTER TABLE [Customer].[Acceptance] DROP CONSTRAINT [FK_Acceptance_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_Subscription_Profile' and type='F')
Begin
	print 'Drop FK_Subscription_Profile foreign key'
	ALTER TABLE [Customer].[Subscription] DROP CONSTRAINT [FK_Subscription_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_Feedback_Profile' and type='F')
Begin
	print 'Drop FK_Feedback_Profile foreign key'
	ALTER TABLE [Notification].[Feedback] DROP CONSTRAINT [FK_Feedback_Profile]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Profile]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_Profile_MarketName_LastModification' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Profile]')))
Begin	
	Print 'Drop Index IX_Profile_MarketName_LastModification ON [Customer].[Profile]'
	DROP INDEX [IX_Profile_MarketName_LastModification] ON [Customer].[Profile]
End
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Profile]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_Profile_RegistrationHashCode' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Profile]')))
Begin	
	Print 'Drop Index IX_Profile_RegistrationHashCode ON [Customer].[Profile]'
	DROP INDEX [IX_Profile_RegistrationHashCode] ON [Customer].[Profile]
End
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Profile]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_Profile_PhoneVerificationHashCode' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Profile]')))
Begin	
	Print 'Drop Index IX_Profile_PhoneVerificationHashCode ON [Customer].[Profile]'
	DROP INDEX [IX_Profile_PhoneVerificationHashCode] ON [Customer].[Profile]
End
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Profile]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'Profile_MarketnameLastmodificationCustomerid_NU_NC' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Profile]')))
Begin	
	Print 'Drop Index Profile_MarketnameLastmodificationCustomerid_NU_NC ON [Customer].[Profile]'
	DROP INDEX [Profile_MarketnameLastmodificationCustomerid_NU_NC] ON [Customer].[Profile]
End
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Profile]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'Profile_PhoneverificationhashcodeCustomerid_INCL1_NU_NC' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Profile]')))
Begin	
	Print 'Drop Index Profile_PhoneverificationhashcodeCustomerid_INCL1_NU_NC ON [Customer].[Profile]'
	DROP INDEX [Profile_PhoneverificationhashcodeCustomerid_INCL1_NU_NC] ON [Customer].[Profile]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Profile]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'Profile_RegistrationhashcodeCustomerid_INCL1_NU_NC' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Profile]')))
Begin	
	Print 'Drop Index Profile_RegistrationhashcodeCustomerid_INCL1_NU_NC ON [Customer].[Profile]'
	DROP INDEX [Profile_RegistrationhashcodeCustomerid_INCL1_NU_NC] ON [Customer].[Profile]
End
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Profile]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_Profile_DCSID' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Profile]')))
Begin	
	Print 'Drop Index IX_Profile_DCSID ON [Customer].[Profile]'
	DROP INDEX [IX_Profile_DCSID] ON [Customer].[Profile]
End	
GO

/* drop all default constraints on profile table !*/
declare @constraint_name nvarchar(128) = N''
declare curDefConstraints cursor for
	select name from sys.objects where parent_object_id = object_id('customer.Profile') 
	and type ='D'
open curDefConstraints
fetch next from curDefConstraints into @constraint_name
while @@fetch_status = 0
begin
	print 'Drop '+@constraint_name +' of Customer.Profile'
	declare @csql nvarchar(1000) = N'ALTER TABLE Customer.Profile DROP CONSTRAINT ['+@constraint_name+N']'
--	print @csql
	fetch next from curDefConstraints into @constraint_name
	exec sp_executesql @stmt = @cSql 
end	
close curDefConstraints
deallocate curDefConstraints
GO

declare @constraint_name nvarchar(128) = N''
SELECT @constraint_name = constraint_name
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
AND TABLE_NAME = 'Profile' AND TABLE_SCHEMA = 'Customer'

if isnull(len(@constraint_name),0) > 0
begin
	print 'Drop Primary key of Customer.Profile'
	declare @csql nvarchar(1000) = N'ALTER TABLE Customer.Profile DROP CONSTRAINT ['+@constraint_name+N']'
	exec sp_executesql @stmt = @cSql 
end	
GO

CREATE TABLE [Customer].[Profile_tmp](
	[CustomerID] [int] IDENTITY(1,1) NOT NULL,
	[ExternalID] [int] NULL,
	[FirstName] [nvarchar](128) NULL,
	[MiddleName] [nvarchar](128) NULL,
	[LastName] [nvarchar](128) NULL,
	[Title] [nvarchar](128) NULL,
	[YearOfBirth] [int] NULL,
	[DefaultPhoneNumber] [nvarchar](128) NULL,
	[TaxNumber] [nvarchar](128) NULL,
	[CancellationReason] [nvarchar](max) NULL,
	[CreationDate] [datetime] NOT NULL,
	[Remark] [nvarchar](max) NULL,
	[LastModification] [datetime] NOT NULL DEFAULT (getutcdate()),
	[NickName] [nvarchar](128) NULL,
	[GenderID] [tinyint] NULL,
	[EthnicityID] [tinyint] NULL,
	[MarketName] [nvarchar](128) NULL,
	[MonthOfBirth] [tinyint] NULL,
	[DayOfBirth] [tinyint] NULL,
	[StatusID] [tinyint] NULL,
	[DisplayNamePreference] [nvarchar](32) NULL,
	[ZipCode] [nvarchar](32) NULL,
	[EmailAddress] [nvarchar](128) NULL,
	[ActivationTime] [datetime] NULL,
	[RegistrationHashCode] [nvarchar](256) NULL,
	[IsGuest] [bit] NOT NULL DEFAULT ((0)),
	[EArchCardLastLoadAmount] [decimal](18, 2) NULL,
	[DefaultAddressType] [nvarchar](10) NULL,
	[OrderSource] [nvarchar](32) NULL,
	[PlatformSource] [nvarchar](32) NULL,
	[PhoneVerificationHashCode] [nvarchar](256) NULL,
	[PhoneStatus] [int] NULL,
	[IsValuedCustomer] [bit] NOT NULL DEFAULT ((0)),
	[SerializedEncryptedValues] [varchar](3700) NULL,
	[SpecialAttentionRequired] [bit] NOT NULL DEFAULT ((0)),
	[SpecialAttentionRequiredReason] [nvarchar](128) NULL,
	[MSAlarmEnabled] [bit] NULL,
	EmailVerificationHashCode nvarchar(256) null,
	EmailStatus int null,
	ExternalPaymentId nvarchar(128) null,
	DCSID nchar(50) null
 CONSTRAINT [PK_Profile] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [FG1]
) ON [FG1]
GO

Print 'Insert Into [Customer].[Profile_tmp]'
set identity_insert [Customer].[Profile_tmp] ON
Insert Into [Customer].[Profile_tmp] (
	[CustomerID]
	,[ExternalID]
	,[FirstName]
	,[MiddleName]
	,[LastName]
	,[Title]
	,[YearOfBirth]
	,[DefaultPhoneNumber]
	,[TaxNumber]
	,[CancellationReason]
	,[CreationDate]
	,[Remark]
	,[LastModification]
	,[NickName]
	,[GenderID]
	,[EthnicityID]
	,[MarketName]
	,[MonthOfBirth]
	,[DayOfBirth]
	,[StatusID]
	,[DisplayNamePreference]
	,[ZipCode]
	,[EmailAddress]
	,[ActivationTime]
	,[RegistrationHashCode]
	,[IsGuest]
	,[EArchCardLastLoadAmount]
	,[DefaultAddressType]
	,[OrderSource]
	,[PlatformSource]
	,[PhoneVerificationHashCode]
	,[PhoneStatus]
	,[IsValuedCustomer]
	,[SerializedEncryptedValues] 
	,[SpecialAttentionRequired] 
	,[SpecialAttentionRequiredReason]
	,[MSAlarmEnabled] 
	,EmailVerificationHashCode
	,EmailStatus
	,ExternalPaymentId
	,DCSID)
SELECT 
	[CustomerID]
	,[ExternalID]
	,[FirstName]
	,[MiddleName]
	,[LastName]
	,[Title]
	,[YearOfBirth]
	,[DefaultPhoneNumber]
	,[TaxNumber]
	,[CancellationReason]
	,[CreationDate]
	,[Remark]
	,[LastModification]
	,[NickName]
	,[GenderID]
	,[EthnicityID]
	,[MarketName]
	,[MonthOfBirth]
	,[DayOfBirth]
	,[StatusID]
	,[DisplayNamePreference]
	,[ZipCode]
	,[EmailAddress]
	,[ActivationTime]
	,[RegistrationHashCode]
	,[IsGuest]
	,[EArchCardLastLoadAmount]
	,[DefaultAddressType]
	,[OrderSource]
	,[PlatformSource]
	,[PhoneVerificationHashCode]
	,[PhoneStatus]
	,[IsValuedCustomer]
	,[SerializedEncryptedValues] 
	,[SpecialAttentionRequired] 
	,[SpecialAttentionRequiredReason]
	,[MSAlarmEnabled] 
	,EmailVerificationHashCode
	,EmailStatus
	,ExternalPaymentId
	,DCSID
FROM [Customer].[Profile]
set identity_insert [Customer].[Profile_tmp] OFF
GO

sp_rename 'Customer.Profile', 'Profile_old'
GO
sp_rename 'Customer.Profile_tmp', 'Profile'
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Profile]') AND [type]='U')) AND 
	NOT (EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'Profile_Statusid_NU_NC' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Profile]')))
Begin	
	Print 'Create Index Profile_Statusid_NU_NC ON [Customer].[Profile]'
	CREATE NONCLUSTERED INDEX [Profile_Statusid_NU_NC] ON [Customer].[Profile]
	(
		[StatusID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90)
--	ON Customer_Index
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Profile]') AND [type]='U')) AND 
	NOT (EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'Profile_Creationdate_INCL1_NU_NC' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Profile]')))
Begin	
	Print 'Create Index Profile_Creationdate_INCL1_NU_NC ON [Customer].[Profile]'
	CREATE NONCLUSTERED INDEX [Profile_Creationdate_INCL1_NU_NC] ON [Customer].[Profile]
	(
		[CreationDate] ASC
	)
	INCLUDE ( 	[CustomerID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90)
--	ON Customer_Index
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Profile]') AND [type]='U')) AND 
	NOT (EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'Profile_Defaultphonenumber_NU_NC' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Profile]')))
Begin	
	Print 'Create Index Profile_Defaultphonenumber_NU_NC ON [Customer].[Profile]'
	CREATE NONCLUSTERED INDEX [Profile_Defaultphonenumber_NU_NC] ON [Customer].[Profile]
	(
		[DefaultPhoneNumber] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90)
--	ON Customer_Index
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Profile]') AND [type]='U')) AND 
	NOT (EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'Profile_Emailverificationhashcode_INCL1_NU_NC' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Profile]')))
Begin	
	Print 'Create Index Profile_Emailverificationhashcode_INCL1_NU_NC ON [Customer].[Profile]'
	CREATE NONCLUSTERED INDEX [Profile_Emailverificationhashcode_INCL1_NU_NC] ON [Customer].[Profile]
	(
		[EmailVerificationHashCode] ASC
	)
	INCLUDE ( 	[CustomerID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90)
--	ON Customer_Index
End	
GO

CREATE NONCLUSTERED INDEX [Profile_MarketnameLastmodificationCustomerid_NU_NC] ON [Customer].[Profile]
(
	[MarketName] ASC,
	[LastModification] ASC,
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	ON Customer_Index
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Profile]') AND [type]='U')) AND 
	NOT (EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'Profile_Registrationhascode_INCL1_NU_NC' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Profile]')))
Begin	
	Print 'Create Index Profile_Registrationhascode_INCL1_NU_NC ON [Customer].[Profile]'
	CREATE NONCLUSTERED INDEX [Profile_Registrationhascode_INCL1_NU_NC] ON [Customer].[Profile]
	(
		[RegistrationHashCode] ASC
	)
	INCLUDE ( 	[PhoneVerificationHashCode]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	ON Customer_Index
End	
GO

ALTER TABLE [Customer].[TINData]  WITH CHECK ADD  CONSTRAINT [FK_TINData_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[TINData] CHECK CONSTRAINT [FK_TINData_Profile]
GO

ALTER TABLE [Customer].[SocialNetwork]  WITH CHECK ADD  CONSTRAINT [FK_SocialNetwork_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[SocialNetwork] CHECK CONSTRAINT [FK_SocialNetwork_Profile]
GO

ALTER TABLE [Customer].[Preference]  WITH CHECK ADD  CONSTRAINT [FK_Preference_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[Preference] CHECK CONSTRAINT [FK_Preference_Profile]
GO

ALTER TABLE [Customer].[Optin]  WITH CHECK ADD  CONSTRAINT [FK_OptIn_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[Optin] CHECK CONSTRAINT [FK_OptIn_Profile]
GO

ALTER TABLE [Customer].[MyLocation]  WITH CHECK ADD  CONSTRAINT [FK_MyLocation_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[MyLocation] CHECK CONSTRAINT [FK_MyLocation_Profile]
GO

ALTER TABLE [Customer].[MobileDevices]  WITH CHECK ADD  CONSTRAINT [FK_MobileDevices_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[MobileDevices] CHECK CONSTRAINT [FK_MobileDevices_Profile]
GO

ALTER TABLE [Customer].[LoyaltyMembershipNumber]  WITH CHECK ADD  CONSTRAINT [FK_LoyaltyMembershipNumber_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[LoyaltyMembershipNumber] CHECK CONSTRAINT [FK_LoyaltyMembershipNumber_Profile]
GO

ALTER TABLE [Customer].[Favorite]  WITH CHECK ADD  CONSTRAINT [FK_Favorite_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[Favorite] CHECK CONSTRAINT [FK_Favorite_Profile]
GO

ALTER TABLE [Customer].[DynamicOptin]  WITH CHECK ADD  CONSTRAINT [FK_DynamicOptIn_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[DynamicOptin] CHECK CONSTRAINT [FK_DynamicOptIn_Profile]
GO

ALTER TABLE [Customer].[CustomerPaymentMethod]  WITH CHECK ADD  CONSTRAINT [FK_CustomerPaymentMethod_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[CustomerPaymentMethod] CHECK CONSTRAINT [FK_CustomerPaymentMethod_Profile]
GO

ALTER TABLE [Customer].[CustomerLookup]  WITH CHECK ADD  CONSTRAINT [FK_CustomerLookup_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[CustomerLookup] CHECK CONSTRAINT [FK_CustomerLookup_Profile]
GO

ALTER TABLE [Customer].[AuthToken]  WITH CHECK ADD  CONSTRAINT [FK_AuthToken_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[AuthToken] CHECK CONSTRAINT [FK_AuthToken_Profile]
GO

ALTER TABLE [Customer].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[Address] CHECK CONSTRAINT [FK_Address_Profile]
GO

ALTER TABLE [Customer].[Acceptance]  WITH CHECK ADD  CONSTRAINT [FK_Acceptance_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[Acceptance] CHECK CONSTRAINT [FK_Acceptance_Profile]
GO

ALTER TABLE [Customer].[Subscription]  WITH CHECK ADD  CONSTRAINT [FK_Subscription_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[Subscription] CHECK CONSTRAINT [FK_Subscription_Profile]
GO

ALTER TABLE [Notification].[Feedback]  WITH CHECK ADD  CONSTRAINT [FK_Feedback_Profile] FOREIGN KEY([CustomerId])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Notification].[Feedback] CHECK CONSTRAINT [FK_Feedback_Profile]
GO

ALTER TABLE [Customer].[Profile] ADD  CONSTRAINT [DF_Profile_LastModification]  DEFAULT (getutcdate()) FOR [LastModification]
GO

ALTER TABLE [Customer].[Profile] ADD  CONSTRAINT [DF_Profile_IsGuest]  DEFAULT ((0)) FOR [IsGuest]
GO

ALTER TABLE [Customer].[Profile] ADD  constraint [DF_Profile_IsValidCustomer] DEFAULT ((0)) FOR [IsValuedCustomer]
GO

ALTER TABLE [Customer].[Profile] ADD  constraint [DF_Profile_SpecialAttentionRequired] DEFAULT ((0)) FOR [SpecialAttentionRequired]
GO
/* kell ez? */
/* we didn't use these yet
CREATE UNIQUE NONCLUSTERED INDEX [IX_Profile_DCSID] ON [Customer].[Profile]
(
	[DCSID] ASC
)
WHERE ([DCSID] IS NOT NULL AND [DCSID]<>'')
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [Customer_Index]
GO

CREATE NONCLUSTERED INDEX [IX_Profile_DefaultPhoneNumber] ON [Customer].[Profile]
(
	[DefaultPhoneNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [Customer_Index]
GO

CREATE NONCLUSTERED INDEX [IX_Profile_EmailAddress] ON [Customer].[Profile]
(
	[EmailAddress] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [Customer_Index]
GO
CREATE NONCLUSTERED INDEX [Profile_Creationdate_INCL1_NU_NC] ON [Customer].[Profile]
(
	[CreationDate] ASC
)
INCLUDE ( 	[CustomerID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [Customer_Index]
GO
CREATE NONCLUSTERED INDEX [Profile_Defaultphonenumber_NU_NC] ON [Customer].[Profile]
(
	[DefaultPhoneNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [Customer_Index]
GO
CREATE NONCLUSTERED INDEX [Profile_Emailverificationhashcode_INCL1_NU_NC] ON [Customer].[Profile]
(
	[EmailVerificationHashCode] ASC
)
INCLUDE ( 	[CustomerID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [Customer_Index]
GO
CREATE NONCLUSTERED INDEX [Profile_Phoneverificationhashcode_INCL1_NU_NC] ON [Customer].[Profile]
(
	[PhoneVerificationHashCode] ASC
)
INCLUDE ( 	[CustomerID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [Customer_Index]
GO
CREATE NONCLUSTERED INDEX [Profile_Registrationhascode_INCL1_NU_NC] ON [Customer].[Profile]
(
	[RegistrationHashCode] ASC
)
INCLUDE ( 	[PhoneVerificationHashCode]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [Customer_Index]
GO
CREATE NONCLUSTERED INDEX [Profile_StatusID_INC1_NU_NC] ON [Customer].[Profile]
(
	[StatusID] ASC
)
INCLUDE ( 	[CreationDate]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [Customer_Index]
GO
CREATE NONCLUSTERED INDEX [Profile_Statusid_NU_NC] ON [Customer].[Profile]
(
	[StatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [Customer_Index]
GO
*/
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
/* OptIn */
if exists (select 1 from sys.objects where name = 'FK_OptIn_Profile' and type='F')
Begin
	print 'Drop FK_OptIn_Profile foreign key'
	ALTER TABLE [Customer].[Optin] DROP CONSTRAINT [FK_OptIn_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'DF_OptIn_LastModification' and type='D')
Begin
	print 'Drop DF_OptIn_LastModification constraint'
	ALTER TABLE [Customer].[Optin] DROP CONSTRAINT [DF_OptIn_LastModification]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Optin]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_OptIn_CustomerID' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Optin]')))
Begin	
	Print 'Drop Index IX_OptIn_CustomerID ON [Customer].[Optin]'
	DROP INDEX [IX_OptIn_CustomerID] ON [Customer].[Optin]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Optin]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_OptIn_LastModification_CustomerID' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Optin]')))
Begin	
	Print 'Drop Index IX_OptIn_LastModification_CustomerID ON [Customer].[Optin]'
	DROP INDEX [IX_OptIn_LastModification_CustomerID] ON [Customer].[Optin]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Optin]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'OptIn_CustomerID_INCL5_NU_NC' 
	AND [object_id]=OBJECT_ID(N'[Customer].[Optin]')))
Begin	
	Print 'Drop Index OptIn_CustomerID_INCL5_NU_NC ON [Customer].[Optin]'
	DROP INDEX [OptIn_CustomerID_INCL5_NU_NC] ON [Customer].[Optin]
End	
GO

declare @constraint_name nvarchar(128) = N''
SELECT @constraint_name = constraint_name
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
AND TABLE_NAME = 'Optin' AND TABLE_SCHEMA = 'Customer'

if isnull(len(@constraint_name),0) > 0
begin
	print 'Drop Primary key of Customer.Optin'
	declare @csql nvarchar(1000) = N'ALTER TABLE Customer.Optin DROP CONSTRAINT ['+@constraint_name+N']'
	exec sp_executesql @stmt = @cSql 
end	
GO

CREATE TABLE [Customer].[Optin_tmp](
	[CustomerOptinID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[OptinTypeID] [tinyint] NULL,
	[OptinFlag] [bit] NULL,
	[LastModification] [datetime] NOT NULL ,
	[SerializedEncryptedValues] [varchar](2000) NULL,
	OptinType nchar(50) null,
 CONSTRAINT [PK_Optin] PRIMARY KEY CLUSTERED 
(
	[CustomerOptinID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [fg1]
) ON [fg1] 
GO

Print 'insert into [Customer].[Optin_tmp]'
set identity_insert [Customer].[Optin_tmp] ON
insert into [Customer].[Optin_tmp]
([CustomerOptinID]
      ,[CustomerID]
      ,[OptinTypeID]
      ,[OptinFlag]
      ,[LastModification]
      ,[SerializedEncryptedValues]
	  ,OptinType)
SELECT [CustomerOptinID]
      ,[CustomerID]
      ,[OptinTypeID]
      ,[OptinFlag]
      ,[LastModification]
      ,[SerializedEncryptedValues]
	  ,OptinType
  FROM [Customer].[Optin]
set identity_insert [Customer].[Optin_tmp] OFF
GO

sp_rename 'Customer.Optin', 'Optin_old'
GO
sp_rename 'Customer.Optin_tmp', 'Optin'
GO

ALTER TABLE [Customer].[OptIn] ADD  CONSTRAINT [DF_OptIn_LastModification]  DEFAULT (getutcdate()) FOR [LastModification]
GO

ALTER TABLE [Customer].[Optin]  WITH CHECK ADD  CONSTRAINT [FK_OptIn_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[Optin] CHECK CONSTRAINT [FK_OptIn_Profile]
GO

Print 'Create index [IX_OptIn_CustomerID] ON [Customer].[OptIn]'
CREATE NONCLUSTERED INDEX [IX_OptIn_CustomerID] ON [Customer].[OptIn]
(
	[CustomerID] ASC
)
INCLUDE ( 	[CustomerOptinID],
	[OptInTypeID],
	[OptinFlag],
	[LastModification],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) 
--	ON [Customer_Index]
GO

Print 'Create index [IX_OptIn_LastModification_CustomerID] ON [Customer].[OptIn]'
CREATE NONCLUSTERED INDEX [IX_OptIn_LastModification_CustomerID] ON [Customer].[OptIn]
(
	[LastModification] ASC,
	[CustomerID] ASC
)
INCLUDE ( 	[CustomerOptinID],
	[OptInTypeID],
	[OptinFlag],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) 
--	ON [Customer_Index]
GO

Print 'Create index [OptIn_CustomerID_INCL5_NU_NC] ON [Customer].[OptIn]'
CREATE NONCLUSTERED INDEX [OptIn_CustomerID_INCL5_NU_NC] ON [Customer].[OptIn]
(
	[CustomerID] ASC
)
INCLUDE ( 	[CustomerOptinID],
	[OptinFlag],
	[LastModification],
	[SerializedEncryptedValues],
	[OptinType]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) 
--	ON [Customer_Index]
GO

/*CREATE NONCLUSTERED INDEX [Optin_Customerid_INCL1_NU_NC] ON [Customer].[Optin]
(
	[CustomerID] ASC
)
INCLUDE ( 	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
CREATE NONCLUSTERED INDEX [IX_Optin_CustomerID] ON [Customer].[Optin]
(
	[CustomerID] ASC
)
INCLUDE ( 	[CustomerOptinID],
	[OptinType],
	[OptinFlag],
	[LastModification],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
	on customer_index
GO
*/
----------------------------------------------------------------------------
/* TINData */
if exists (select 1 from sys.objects where name = 'FK_TINData_Profile' and type='F')
Begin
	print 'Drop FK_TINData_Profile foreign key'
	ALTER TABLE [Customer].[TINData] DROP CONSTRAINT [FK_TINData_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'DF_TINData_LastModification' and type='D')
Begin
	print 'Drop DF_TINData_LastModification constraint'
	ALTER TABLE [Customer].[TINData] DROP CONSTRAINT [DF_TINData_LastModification]
End	
GO

declare @constraint_name nvarchar(128) = N''
SELECT @constraint_name = constraint_name
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
AND TABLE_NAME = 'TINData' AND TABLE_SCHEMA = 'Customer'

if isnull(len(@constraint_name),0) > 0
begin
	print 'Drop Primary key of Customer.TINData'
	declare @csql nvarchar(1000) = N'ALTER TABLE Customer.TINData DROP CONSTRAINT ['+@constraint_name+N']'
	exec sp_executesql @stmt = @cSql 
end	
GO

CREATE TABLE [Customer].[TINData_tmp](
	[CustomerID] [int] NOT NULL,
	[TINNumber] [nvarchar](128) NULL,
	[TINFirstName] [nvarchar](128) NULL,
	[TINLastName] [nvarchar](128) NULL,
	[TINAddress] [nvarchar](1000) NULL,
	[TINPostalCode] [nvarchar](32) NULL,
	[LastModification] [datetime] NOT NULL,
	[SerializedEncryptedValues] [varchar](2000) NULL,
 CONSTRAINT [PK_TINData] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [FG1]
) ON [FG1]
GO

Print 'INSERT INTO [Customer].[TINData_tmp]'
INSERT INTO [Customer].[TINData_tmp]
           ([CustomerID]
           ,[TINNumber]
           ,[TINFirstName]
           ,[TINLastName]
           ,[TINAddress]
           ,[TINPostalCode]
           ,[LastModification]
           ,[SerializedEncryptedValues])
select [CustomerID]
           ,[TINNumber]
           ,[TINFirstName]
           ,[TINLastName]
           ,[TINAddress]
           ,[TINPostalCode]
           ,[LastModification]
           ,[SerializedEncryptedValues]
from Customer.TINData		   
GO

sp_rename 'Customer.TINData', 'TINData_old'
GO

sp_rename 'Customer.TINData_tmp', 'TINData'
GO

ALTER TABLE [Customer].[TINData] ADD  CONSTRAINT [DF_TINData_LastModification]  DEFAULT (getutcdate()) FOR [LastModification]
GO

ALTER TABLE [Customer].[TINData]  WITH CHECK ADD  CONSTRAINT [FK_TINData_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[TINData] CHECK CONSTRAINT [FK_TINData_Profile]
GO
----------------------------------------------------------------------
/* FeedBack */

if exists (select 1 from sys.objects where name = 'FK_Feedback_FeedbackType' and type='F')
Begin
	print 'Drop FK_Feedback_FeedbackType foreign key'
	ALTER TABLE [Notification].[Feedback] DROP CONSTRAINT [FK_Feedback_FeedbackType]
End	
GO

if exists (select 1 from sys.objects where name = 'FK_Feedback_Profile' and type='F')
Begin
	print 'Drop FK_Feedback_Profile foreign key'
	ALTER TABLE [Notification].[Feedback] DROP CONSTRAINT [FK_Feedback_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'DF_Feedback_LastModification' and type='D')
Begin
	print 'Drop DF_Feedback_LastModification constraint'
	ALTER TABLE [Notification].[Feedback] DROP CONSTRAINT [DF_Feedback_LastModification]
End	
GO

declare @constraint_name nvarchar(128) = N''
SELECT @constraint_name = constraint_name
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
AND TABLE_NAME = 'Feedback' AND TABLE_SCHEMA = 'Notification'

if isnull(len(@constraint_name),0) > 0
begin
	print 'Drop Primary key of Notification.Feedback'
	declare @csql nvarchar(1000) = N'ALTER TABLE Notification.Feedback DROP CONSTRAINT ['+@constraint_name+N']'
	exec sp_executesql @stmt = @cSql 
end	
GO

CREATE TABLE [Notification].[Feedback_tmp](
	[FeedbackId] [int] IDENTITY(1,1) NOT NULL,
	[FeedbackTypeID] [tinyint] NULL,
	[CustomerId] [int] NOT NULL,
	[Rating] [tinyint] NULL,
	[Comments] [nvarchar](500) NULL,
	[Received] [datetime] NULL,
	[SendDate] [datetime] NULL,
	[LastModification] [datetime] NOT NULL,
	[SerializedEncryptedValues] [varchar](2000) NULL,
 CONSTRAINT [PK_Feedback] PRIMARY KEY CLUSTERED 
(
	[FeedbackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [FG1]
) ON [FG1]
GO

Print 'INSERT INTO [Notification].[Feedback_tmp]'
set identity_insert [Notification].[Feedback_tmp] ON
INSERT INTO [Notification].[Feedback_tmp]
           (FeedbackId
		   ,[FeedbackTypeID]
           ,[CustomerId]
           ,[Rating]
           ,[Comments]
           ,[Received]
           ,[SendDate]
           ,[LastModification]
           ,[SerializedEncryptedValues])
SELECT 		   
           FeedbackId
		   ,[FeedbackTypeID]
           ,[CustomerId]
           ,[Rating]
           ,[Comments]
           ,[Received]
           ,[SendDate]
           ,[LastModification]
           ,[SerializedEncryptedValues]
FROM [Notification].[Feedback]		   
set identity_insert [Notification].[Feedback_tmp] OFF
Go

sp_rename 'Notification.Feedback', 'Feedback_old'
GO
sp_rename 'Notification.Feedback_tmp', 'Feedback'
GO

ALTER TABLE [Notification].[Feedback] ADD  CONSTRAINT [DF_Feedback_LastModification]  DEFAULT (getutcdate()) FOR [LastModification]
GO

ALTER TABLE [Notification].[Feedback]  WITH CHECK ADD  CONSTRAINT [FK_Feedback_FeedbackType] FOREIGN KEY([FeedbackTypeID])
REFERENCES [Dictionary].[FeedbackType] ([FeedbackTypeID])
GO
ALTER TABLE [Notification].[Feedback] CHECK CONSTRAINT [FK_Feedback_FeedbackType]
GO

ALTER TABLE [Notification].[Feedback]  WITH CHECK ADD  CONSTRAINT [FK_Feedback_Profile] FOREIGN KEY([CustomerId])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Notification].[Feedback] CHECK CONSTRAINT [FK_Feedback_Profile]
GO
-------------------------------------------------------------------------
/* SocialNetwork */
if exists (select 1 from sys.objects where name = 'FK_SocialNetwork_Profile' and type='F')
Begin
	print 'Drop FK_SocialNetwork_Profile foreign key'
	ALTER TABLE [Customer].[SocialNetwork] DROP CONSTRAINT [FK_SocialNetwork_Profile]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[SocialNetwork]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_SocialNetwork_CustomerID' 
	AND [object_id]=OBJECT_ID(N'[Customer].[SocialNetwork]')))
Begin	
	Print 'Drop Index IX_SocialNetwork_CustomerID ON [Customer].[SocialNetwork]'
	DROP INDEX [IX_SocialNetwork_CustomerID] ON [Customer].[SocialNetwork]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[SocialNetwork]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_SocialNetwork_InternalID_CustomerID' 
	AND [object_id]=OBJECT_ID(N'[Customer].[SocialNetwork]')))
Begin	
	Print 'Drop Index IX_SocialNetwork_InternalID_CustomerID ON [Customer].[SocialNetwork]'
	DROP INDEX [IX_SocialNetwork_InternalID_CustomerID] ON [Customer].[SocialNetwork]
End	
GO

declare @constraint_name nvarchar(128) = N''
SELECT @constraint_name = constraint_name
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
AND TABLE_NAME = 'SocialNetwork' AND TABLE_SCHEMA = 'Customer'

if isnull(len(@constraint_name),0) > 0
begin
	print 'Drop Primary key of Customer.SocialNetwork'
	declare @csql nvarchar(1000) = N'ALTER TABLE Customer.SocialNetwork DROP CONSTRAINT ['+@constraint_name+N']'
	exec sp_executesql @stmt = @cSql 
end	
GO

CREATE TABLE [Customer].[SocialNetwork_tmp](
	[CustomerSocialNetworkID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[InternalID] [nvarchar](255) NULL,
	[SocialNetworkID] [int] NULL,
	[SerializedEncryptedValues] [varchar](2000) NULL,
 CONSTRAINT [PK_SocialNetwork] PRIMARY KEY CLUSTERED 
(
	[CustomerSocialNetworkID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [FG1]
) ON [FG1]
GO

Print 'INSERT INTO [Customer].[SocialNetwork_tmp]'
set identity_insert [Customer].[SocialNetwork_tmp] ON
INSERT INTO [Customer].[SocialNetwork_tmp]
           (CustomerSocialNetworkID
		   ,[CustomerID]
           ,[InternalID]
           ,[SocialNetworkID]
           ,[SerializedEncryptedValues])
SELECT
           CustomerSocialNetworkID
		   ,[CustomerID]
           ,[InternalID]
           ,[SocialNetworkID]
           ,[SerializedEncryptedValues]
FROM Customer.SocialNetwork		   
GO
set identity_insert [Customer].[SocialNetwork_tmp] OFF
GO
sp_rename 'Customer.SocialNetwork', 'SocialNetwork_old'
GO
sp_rename 'Customer.SocialNetwork_tmp', 'SocialNetwork'
GO

Print 'Create index [IX_SocialNetwork_CustomerID] ON [Customer].[SocialNetwork]'
CREATE NONCLUSTERED INDEX [IX_SocialNetwork_CustomerID] ON [Customer].[SocialNetwork]
(
	[CustomerID] ASC
)
INCLUDE ( 	[CustomerSocialNetworkID],
	[InternalID],
	[SocialNetworkID],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = On, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) 
--	ON [Customer_Index]
GO

Print 'Create index [IX_SocialNetwork_InternalID_CustomerID] ON [Customer].[SocialNetwork]'
CREATE NONCLUSTERED INDEX [IX_SocialNetwork_InternalID_CustomerID] ON [Customer].[SocialNetwork]
(
	[InternalID] ASC,
	[CustomerID] ASC
)
INCLUDE ( 	[CustomerSocialNetworkID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = On, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) 
--	ON [Customer_Index]
GO

ALTER TABLE [Customer].[SocialNetwork]  WITH CHECK ADD  CONSTRAINT [FK_SocialNetwork_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[SocialNetwork] CHECK CONSTRAINT [FK_SocialNetwork_Profile]
GO
----------------------------------------------------------------------------
/* SocialNetworkAccessToken */

if exists (select 1 from sys.objects where name = 'DF_SocialNetworkAccessToken_LastModification' and type='D')
Begin
	print 'Drop DF_SocialNetworkAccessToken_LastModification constraint'
	ALTER TABLE [Customer].[SocialNetworkAccessToken] DROP CONSTRAINT [DF_SocialNetworkAccessToken_LastModification]
End	
GO

declare @constraint_name nvarchar(128) = N''
SELECT @constraint_name = constraint_name
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
AND TABLE_NAME = 'SocialNetworkAccessToken' AND TABLE_SCHEMA = 'Customer'

if isnull(len(@constraint_name),0) > 0
begin
	print 'Drop Primary key of Customer.SocialNetworkAccessToken'
	declare @csql nvarchar(1000) = N'ALTER TABLE Customer.SocialNetworkAccessToken DROP CONSTRAINT ['+@constraint_name+N']'
	exec sp_executesql @stmt = @cSql 
end	
GO

CREATE TABLE [Customer].[SocialNetworkAccessToken_tmp](
	[SocialNetworkID] [nvarchar](128) NOT NULL,
	[InternalID] [nvarchar](256) NOT NULL,
	[AccessToken] [nvarchar](512) NULL,
	[RefreshToken] [nvarchar](512) NULL,
	[LastModification] [datetime] NULL,
	[SerializedEncryptedValues] [varchar](max) NULL,
 CONSTRAINT [PK_SocialNetworkAccessToken] PRIMARY KEY NONCLUSTERED 
(
	[SocialNetworkID] ASC,
	[InternalID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [FG1]
) ON [FG1] TEXTIMAGE_ON [FG1]
GO

ALTER TABLE [Customer].[SocialNetworkAccessToken] ADD  CONSTRAINT [DF_SocialNetworkAccessToken_LastModification]  DEFAULT (getutcdate()) FOR [LastModification]
GO

Print 'INSERT INTO [Customer].[SocialNetworkAccessToken_tmp]'
INSERT INTO [Customer].[SocialNetworkAccessToken_tmp]
           ([SocialNetworkID]
           ,[InternalID]
           ,[AccessToken]
           ,[RefreshToken]
           ,[LastModification]
           ,[SerializedEncryptedValues])
SELECT
           [SocialNetworkID]
           ,[InternalID]
           ,[AccessToken]
           ,[RefreshToken]
           ,[LastModification]
           ,[SerializedEncryptedValues]
FROM [Customer].[SocialNetworkAccessToken]		   
GO

sp_rename 'Customer.SocialNetworkAccessToken', 'SocialNetworkAccessToken_old'
GO
sp_rename 'Customer.SocialNetworkAccessToken_tmp', 'SocialNetworkAccessToken'
GO
--------------------------------------------------------------------------
/* LoyaltyMembershipNumber */
if exists (select 1 from sys.objects where name = 'FK_LoyaltyMembershipNumber_Profile' and type='F')
Begin
	print 'Drop FK_LoyaltyMembershipNumber_Profile foreign key'
	ALTER TABLE [Customer].[LoyaltyMembershipNumber] DROP CONSTRAINT [FK_LoyaltyMembershipNumber_Profile]
End	
GO

if exists (select 1 from sys.objects where name = 'DF_LoyaltyMembershipNumber_LastModificationUTC' and type='D')
Begin
	print 'Drop DF_LoyaltyMembershipNumber_LastModificationUTC constraint'
	ALTER TABLE [Customer].[LoyaltyMembershipNumber] DROP CONSTRAINT [DF_LoyaltyMembershipNumber_LastModificationUTC]
End	
GO

declare @constraint_name nvarchar(128) = N''
SELECT @constraint_name = constraint_name
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
AND TABLE_NAME = 'LoyaltyMembershipNumber' AND TABLE_SCHEMA = 'Customer'

if isnull(len(@constraint_name),0) > 0
begin
	print 'Drop Primary key of Customer.LoyaltyMembershipNumber'
	declare @csql nvarchar(1000) = N'ALTER TABLE Customer.LoyaltyMembershipNumber DROP CONSTRAINT ['+@constraint_name+N']'
	exec sp_executesql @stmt = @cSql 
end	
GO

CREATE TABLE [Customer].[LoyaltyMembershipNumber_tmp](
	[CustomerLoyaltyMembershipNumberID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[LoyaltyMembershipNumber] [nvarchar](128) NOT NULL,
	[LoyaltyMembershipType] [nvarchar](16) NOT NULL,
	[LastModificationUTC] [datetime] NOT NULL,
	[SerializedEncryptedValues] [varchar](2000) NULL,
 CONSTRAINT [PK_LoyaltyMembershipNumber] PRIMARY KEY CLUSTERED 
(
	[CustomerLoyaltyMembershipNumberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [FG1]
) ON [FG1]
GO

Print 'INSERT INTO [Customer].[LoyaltyMembershipNumber_tmp]'
set identity_insert [Customer].[LoyaltyMembershipNumber_tmp] ON
INSERT INTO [Customer].[LoyaltyMembershipNumber_tmp]
           (CustomerLoyaltyMembershipNumberID
		   ,[CustomerID]
           ,[LoyaltyMembershipNumber]
           ,[LoyaltyMembershipType]
           ,[LastModificationUTC]
           ,[SerializedEncryptedValues])
SELECT
           CustomerLoyaltyMembershipNumberID
		   ,CustomerID
           ,[LoyaltyMembershipNumber]
           ,[LoyaltyMembershipType]
           ,[LastModificationUTC]
           ,[SerializedEncryptedValues]
FROM [Customer].[LoyaltyMembershipNumber]		   
set identity_insert [Customer].[LoyaltyMembershipNumber_tmp] OFF
GO

sp_rename 'Customer.LoyaltyMembershipNumber', 'LoyaltyMembershipNumber_old'
GO
sp_rename 'Customer.LoyaltyMembershipNumber_tmp', 'LoyaltyMembershipNumber'
GO

ALTER TABLE [Customer].[LoyaltyMembershipNumber] ADD  CONSTRAINT [DF_LoyaltyMembershipNumber_LastModificationUTC]  DEFAULT (getutcdate()) FOR [LastModificationUTC]
GO

ALTER TABLE [Customer].[LoyaltyMembershipNumber]  WITH CHECK ADD  CONSTRAINT [FK_LoyaltyMembershipNumber_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])
GO
ALTER TABLE [Customer].[LoyaltyMembershipNumber] CHECK CONSTRAINT [FK_LoyaltyMembershipNumber_Profile]
GO
----------------------------------------------------------------------------
/* Authentication */
if exists (select 1 from sys.objects where name = 'FK_AuthToInfo_Authentication' and type='F')
Begin
	print 'Drop FK_AuthToInfo_Authentication foreign key'
	ALTER TABLE [Authentication].[AuthToInfo] DROP CONSTRAINT [FK_AuthToInfo_Authentication]
End	
GO

if exists (select 1 from sys.objects where name = 'DF_Authentication_LoginSourceType' and type='D')
Begin
	print 'Drop DF_Authentication_LoginSourceType constraint'
	ALTER TABLE [Authentication].[Authentication] DROP CONSTRAINT [DF_Authentication_LoginSourceType]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Authentication].[Authentication]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'UQ_User_Market_Name' 
	AND [object_id]=OBJECT_ID(N'[Authentication].[Authentication]')))
Begin	
	Print 'Drop Index UQ_User_Market_Name ON [Authentication].[Authentication]'
	ALTER TABLE [Authentication].[Authentication] DROP CONSTRAINT [UQ_User_Market_Name]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Authentication].[Authentication]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_Authentication_UserName_MarketName' 
	AND [object_id]=OBJECT_ID(N'[Authentication].[Authentication]')))
Begin	
	Print 'Drop Index IX_Authentication_UserName_MarketName ON [Authentication].[Authentication]'
	DROP INDEX [IX_Authentication_UserName_MarketName] ON [Authentication].[Authentication]
End	
GO

declare @constraint_name nvarchar(128) = N''
SELECT @constraint_name = constraint_name
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
AND TABLE_NAME = 'Authentication' AND TABLE_SCHEMA = 'Authentication'

if isnull(len(@constraint_name),0) > 0
begin
	print 'Drop Primary key of Authentication.Authentication'
	declare @csql nvarchar(1000) = N'ALTER TABLE Authentication.Authentication DROP CONSTRAINT ['+@constraint_name+N']'
	exec sp_executesql @stmt = @cSql 
end	
GO

CREATE TABLE [Authentication].[Authentication_tmp](
	[Token] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](128) NULL,
	[LastAuthenticationTime] [datetime] NULL,
	[LoginSourceType] [nvarchar](50) NULL,
	[AuthenticationRetries] [int] NOT NULL,
	[SerializedEncryptedValues] [varchar](2000) NULL,
	[MarketName] [nvarchar](128) NULL,
 CONSTRAINT [PK_Authentication] PRIMARY KEY CLUSTERED 
(
	[Token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [FG1],
 CONSTRAINT [UQ_User_Market_Name] UNIQUE NONCLUSTERED 
(
	[UserName] ASC,
	[MarketName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) 
-- ON [Authentication_index]
) ON [FG1]
GO

ALTER TABLE [Authentication].[Authentication] ADD  CONSTRAINT [DF_Authentication_LoginSourceType]  DEFAULT ('0') FOR [LoginSourceType]
GO

Print 'INSERT INTO [Authentication].[Authentication_tmp]'
INSERT INTO [Authentication].[Authentication_tmp]
           ([Token]
           ,[UserName]
           ,[LastAuthenticationTime]
           ,[LoginSourceType]
           ,[AuthenticationRetries]
           ,[SerializedEncryptedValues]
           ,[MarketName])
SELECT
           [Token]
           ,[UserName]
           ,[LastAuthenticationTime]
           ,[LoginSourceType]
           ,[AuthenticationRetries]
           ,[SerializedEncryptedValues]
           ,[MarketName]
FROM [Authentication].[Authentication]		   
GO

sp_rename 'Authentication.Authentication', 'Authentication_old'
GO
sp_rename 'Authentication.Authentication_tmp', 'Authentication'
GO

Print 'Create index [IX_Authentication_UserName_MarketName] ON [Authentication].[Authentication]'
CREATE NONCLUSTERED INDEX [IX_Authentication_UserName_MarketName] ON [Authentication].[Authentication]
(
	[UserName] ASC,
	[MarketName] ASC
)
INCLUDE 
( 	[LastAuthenticationTime],
	[LoginSourceType],
	[AuthenticationRetries],
	[SerializedEncryptedValues]
) 
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) 
--	ON [Authentication_Index]
GO

Print 'Create unique index [UQ_User_Market_Name] on [Authentication].[Authentication]'
ALTER TABLE [Authentication].[Authentication] ADD  CONSTRAINT [UQ_User_Market_Name] UNIQUE NONCLUSTERED 
(
	[UserName] ASC,
	[MarketName] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) 
--	ON [Authentication_Index]
GO

ALTER TABLE [Authentication].[AuthToInfo]  WITH CHECK ADD  CONSTRAINT [FK_AuthToInfo_Authentication] FOREIGN KEY([AuthTokenID])
REFERENCES [Authentication].[Authentication] ([Token])
GO
ALTER TABLE [Authentication].[AuthToInfo] CHECK CONSTRAINT [FK_AuthToInfo_Authentication]
GO
------------------------------------------------------------------------------
/* AuthInfo */
if exists (select 1 from sys.objects where name = 'FK_AuthToInfo_AuthInfo' and type='F')
Begin
	print 'Drop FK_AuthToInfo_AuthInfo foreign key'
	ALTER TABLE [Authentication].[AuthToInfo] DROP CONSTRAINT [FK_AuthToInfo_AuthInfo]
End	
GO

if exists (select 1 from sys.objects where name = 'DF_AuthInfo_PasswordEncrypterType' and type='D')
Begin
	print 'Drop DF_AuthInfo_PasswordEncrypterType constraint'
	ALTER TABLE [Authentication].[AuthInfo] DROP CONSTRAINT [DF_AuthInfo_PasswordEncrypterType]
End	
GO

declare @constraint_name nvarchar(128) = N''
SELECT @constraint_name = constraint_name
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
AND TABLE_NAME = 'AuthInfo' AND TABLE_SCHEMA = 'Authentication'

if isnull(len(@constraint_name),0) > 0
begin
	print 'Drop Primary key of Authentication.AuthInfo'
	declare @csql nvarchar(1000) = N'ALTER TABLE Authentication.AuthInfo DROP CONSTRAINT ['+@constraint_name+N']'
	exec sp_executesql @stmt = @cSql 
end	
GO

CREATE TABLE [Authentication].[AuthInfo_tmp](
	[AuthInfoID] [int] IDENTITY(1,1) NOT NULL,
	[Password] [nvarchar](128) NULL,
	[RegistrationTime] [datetime] NULL,
	[MustChangePassword] [bit] NULL,
	[LastPasswordChangeTime] [datetime] NULL,
	[SerializedEncryptedValues] [varchar](2000) NULL,
	[PasswordEncrypterType] [int] NULL,
 CONSTRAINT [PK_AuthInfo] PRIMARY KEY CLUSTERED 
(
	[AuthInfoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [FG1]
) ON [FG1]
GO

ALTER TABLE [Authentication].[AuthInfo] ADD  CONSTRAINT [DF_AuthInfo_PasswordEncrypterType]  DEFAULT ((5)) FOR [PasswordEncrypterType]
GO

Print 'INSERT INTO [Authentication].[AuthInfo_tmp]'
set identity_insert [Authentication].[AuthInfo_tmp] ON
INSERT INTO [Authentication].[AuthInfo_tmp]
           (AuthInfoID
		   ,[Password]
           ,[RegistrationTime]
           ,[MustChangePassword]
           ,[LastPasswordChangeTime]
           ,[SerializedEncryptedValues]
           ,[PasswordEncrypterType])
SELECT 
           (AuthInfoID
		   ,[Password]
           ,[RegistrationTime]
           ,[MustChangePassword]
           ,[LastPasswordChangeTime]
           ,[SerializedEncryptedValues]
           ,[PasswordEncrypterType])
FROM [Authentication].[AuthInfo]		   
GO
set identity_insert [Authentication].[AuthInfo_tmp] OFF
GO

sp_rename 'Authentication.AuthInfo', 'AuthInfo_old'
GO
sp_rename 'Authentication.AuthInfo_tmp', 'AuthInfo'
GO

ALTER TABLE [Authentication].[AuthToInfo]  WITH CHECK ADD  CONSTRAINT [FK_AuthToInfo_AuthInfo] FOREIGN KEY([AuthInfoID])
REFERENCES [Authentication].[AuthInfo] ([AuthInfoID])
GO
ALTER TABLE [Authentication].[AuthToInfo] CHECK CONSTRAINT [FK_AuthToInfo_AuthInfo]
GO
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

/* in order db !!!!! */
use <EcpOrderDB>
go

if exists (select 1 from sys.objects where name = 'DF_SerializedData_LastModification' and type='D')
Begin
	print 'Drop DF_SerializedData_LastModification constraint'
	ALTER TABLE [OrderTaking].[SerializedData] DROP CONSTRAINT [DF_SerializedData_LastModification]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[OrderTaking].[SerializedData]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_SerializedData_OrderID_OrderProtoBuf' 
	AND [object_id]=OBJECT_ID(N'[OrderTaking].[SerializedData]')))
Begin	
	Print 'Drop Index IX_SerializedData_OrderID_OrderProtoBuf ON [OrderTaking].[SerializedData]'
	DROP INDEX [IX_SerializedData_OrderID_OrderProtoBuf] ON [OrderTaking].[SerializedData]
End	
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[OrderTaking].[SerializedData]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_SerializedData_OrderId' 
	AND [object_id]=OBJECT_ID(N'[OrderTaking].[SerializedData]')))
Begin	
	Print 'Drop Index IX_SerializedData_OrderId ON [OrderTaking].[SerializedData]'
	DROP INDEX [IX_SerializedData_OrderId] ON [OrderTaking].[SerializedData]
End	
GO

declare @constraint_name nvarchar(128) = N''
SELECT @constraint_name = constraint_name
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
AND TABLE_NAME = 'SerializedData' AND TABLE_SCHEMA = 'OrderTaking'

if isnull(len(@constraint_name),0) > 0
begin
	print 'Drop Primary key of OrderTaking.SerializedData'
	declare @csql nvarchar(1000) = N'ALTER TABLE OrderTaking.SerializedData DROP CONSTRAINT ['+@constraint_name+N']'
	exec sp_executesql @stmt = @cSql 
end	
GO

CREATE TABLE [OrderTaking].[SerializedData_tmp](
	[OrderId] [uniqueidentifier] NOT NULL,
	[MarketID] [char](2) NOT NULL,
	[OrderProtoBuf] [varbinary](max) NULL,
	[LastModification] [datetime] NOT NULL ,
	[SerializedDataType] [nvarchar](500) NOT NULL,
	[SerializedProdInfo] [varchar](max) NULL,
	[SerializedOrderViewInput] [varchar](max) NULL,
	[SerializedEncryptedValues] [varchar](max) NULL,
 CONSTRAINT [PK_SerializedData] PRIMARY KEY NONCLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [fg1]
) ON [fg1] TEXTIMAGE_ON [fg1]
GO

Print 'insert into [OrderTaking].[SerializedData_tmp]'
insert into [OrderTaking].[SerializedData_tmp]
	  ([OrderId]
      ,[MarketID]
      ,[OrderProtoBuf]
      ,[LastModification]
      ,[SerializedDataType]
      ,[SerializedProdInfo]
      ,[SerializedOrderViewInput]
      ,[SerializedEncryptedValues])
SELECT [OrderId]
      ,[MarketID]
      ,[OrderProtoBuf]
      ,[LastModification]
      ,[SerializedDataType]
      ,[SerializedProdInfo]
      ,[SerializedOrderViewInput]
      ,[SerializedEncryptedValues]
  FROM [OrderTaking].[SerializedData]

sp_rename 'OrderTaking.SerializedData', 'SerializedData_old'
GO
sp_rename 'OrderTaking.SerializedData_tmp', 'SerializedData'
GO

ALTER TABLE [OrderTaking].[SerializedData] ADD  CONSTRAINT [DF_SerializedData_LastModification]  DEFAULT (getutcdate()) FOR [LastModification]
GO

Print 'Create index [IX_SerializedData_OrderId] ON [OrderTaking].[SerializedData]'
CREATE NONCLUSTERED INDEX [IX_SerializedData_OrderId] ON [OrderTaking].[SerializedData]
(
	[OrderId] ASC
)
INCLUDE ( 	[MarketID],
	[OrderProtoBuf],
	[LastModification],
	[SerializedDataType],
	[SerializedProdInfo],
	[SerializedOrderViewInput],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) 
	ON [FG2]
GO

Print 'Create index [IX_SerializedData_OrderId_OrderProtoBuf] ON [OrderTaking].[SerializedData]'
CREATE NONCLUSTERED INDEX [IX_SerializedData_OrderId_OrderProtoBuf] ON [OrderTaking].[SerializedData]
(
	[OrderId] ASC
)
INCLUDE ( 	[OrderProtoBuf]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) 
ON [FG2]
GO

IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[OrderTaking].[SerializedData]') AND [type]='U')) AND 
	NOT (EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'SerializedData_Orderid_NU_NC' 
	AND [object_id]=OBJECT_ID(N'[OrderTaking].[SerializedData]')))
Begin	
	Print 'Create Index SerializedData_Orderid_NU_NC ON [OrderTaking].[SerializedData]'
	CREATE CLUSTERED INDEX [SerializedData_Orderid_NU_NC] ON [OrderTaking].[SerializedData]
	(
		[OrderId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80)
	on FG2
End	
GO
