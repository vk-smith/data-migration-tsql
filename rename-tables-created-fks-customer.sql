USE <EcpCustomer>;


EXEC sp_rename 'Customer.MyLocation_tmp', 'MyLocation';
EXEC sp_rename 'PK_MyLocation_tmp', 'PK_MyLocation';
EXEC sp_rename 'Customer.MobileDevices_tmp', 'MobileDevices';
EXEC sp_rename 'PK_MobileDevices_tmp', 'PK_MobileDevices';
EXEC sp_rename 'Customer.DynamicOptin_tmp', 'DynamicOptin';
EXEC sp_rename 'PK_DynamicOptIn_tmp', 'PK_DynamicOptIn';
EXEC sp_rename 'Customer.Acceptance_tmp', 'Acceptance';
EXEC sp_rename 'PK_Acceptance_tmp', 'PK_Acceptance';
EXEC sp_rename 'Customer.Subscription_tmp', 'Subscription';
EXEC sp_rename 'PK_Subscription_tmp', 'PK_Subscription';
EXEC sp_rename 'Customer.Preference_tmp', 'Preference';
EXEC sp_rename 'PK_Preference_tmp', 'PK_Preference';
EXEC sp_rename 'DF_Preference_tmp_LastModification', 'DF_Preference_LastModification';
EXEC sp_rename 'Customer.CustomerPaymentMethod_tmp', 'CustomerPaymentMethod';
EXEC sp_rename 'PK_CustomerPaymentMethod_tmp', 'PK_CustomerPaymentMethod';
EXEC sp_rename 'Customer.Favorite_tmp', 'Favorite';
EXEC sp_rename 'PK_Favorite_tmp', 'PK_Favorite';
EXEC sp_rename 'Customer.AuthToken_tmp', 'AuthToken';
EXEC sp_rename 'PK_AuthToken_tmp', 'PK_AuthToken';
EXEC sp_rename 'DF_AuthTOken_tmp_LastModification', 'DF_AuthTOken_LastModification';
EXEC sp_rename 'Customer.Address_tmp', 'Address';
EXEC sp_rename 'PK_Address_tmp', 'PK_Address';
EXEC sp_rename 'Customer.TINData_tmp', 'TINData';
EXEC sp_rename 'PK_TINData_tmp', 'PK_TINData';
EXEC sp_rename 'DF_TINData_tmp_LastModification', 'DF_TINData_LastModification';
EXEC sp_rename 'Customer.Profile_tmp', 'Profile';
EXEC sp_rename 'PK_Profile_tmp', 'PK_Profile';
EXEC sp_rename 'Customer.Optin_tmp', 'Optin';
EXEC sp_rename 'PK_Optin_tmp', 'PK_Optin';
EXEC sp_rename 'Notification.Feedback_tmp', 'Feedback';
EXEC sp_rename 'PK_Feedback_tmp', 'PK_Feedback';
EXEC sp_rename 'DF_Feedback_tmp_LastModification', 'DF_Feedback_LastModification';
EXEC sp_rename 'Customer.SocialNetwork_tmp', 'SocialNetwork';
EXEC sp_rename 'PK_SocialNetwork_tmp', 'PK_SocialNetwork';
EXEC sp_rename 'Customer.SocialNetworkAccessToken_tmp', 'SocialNetworkAccessToken';
EXEC sp_rename 'PK_SocialNetworkAccessToken_tmp', 'PK_SocialNetworkAccessToken';
EXEC sp_rename 'Customer.LoyaltyMembershipNumber_tmp', 'LoyaltyMembershipNumber';
EXEC sp_rename 'PK_LoyaltyMembershipNumber_tmp', 'PK_LoyaltyMembershipNumber';
EXEC sp_rename 'DF_LoyaltyMembershipNumber_tmp_LastModificationUTC', 'DF_LoyaltyMembershipNumber_LastModificationUTC';
EXEC sp_rename 'Authentication.Authentication_tmp', 'Authentication';
EXEC sp_rename 'PK_Authentication_tmp', 'PK_Authentication';
EXEC sp_rename 'DF_Authentication_tmp_LoginSourceType', 'DF_Authentication_LoginSourceType';
EXEC sp_rename 'Authentication.AuthInfo_tmp', 'AuthInfo';
EXEC sp_rename 'PK_AuthInfo_tmp', 'PK_AuthInfo';
EXEC sp_rename 'DF_AuthInfo_tmp_PasswordEncrypterType', 'DF_AuthInfo_PasswordEncrypterType';




ALTER TABLE [Authentication].[AuthToInfo]  WITH NOCHECK ADD CONSTRAINT [FK_AuthToInfo_Authentication] FOREIGN KEY ([AuthTokenID]) REFERENCES [Authentication].[Authentication]([Token]);
ALTER TABLE [Authentication].[AuthToInfo] WITH NOCHECK ADD CONSTRAINT [FK_AuthToInfo_AuthInfo] FOREIGN KEY ([AuthInfoID]) REFERENCES [Authentication].[AuthInfo]([AuthInfoID]);
ALTER TABLE [Notification].[Feedback] WITH NOCHECK ADD CONSTRAINT [FK_Feedback_FeedbackType] FOREIGN KEY ([FeedbackTypeID]) REFERENCES [Dictionary].[FeedbackType]([FeedbackTypeID]);
ALTER TABLE [Customer].[Favorite] WITH NOCHECK ADD CONSTRAINT [FK_Favorite_FavoriteType] FOREIGN KEY ([Type]) REFERENCES [Dictionary].[FavoriteType]([FavoriteTypeID]);
ALTER TABLE [Customer].[AddressPhoneLookup] WITH NOCHECK ADD CONSTRAINT [FK_Profile_AddressPhoneLookup] FOREIGN KEY ([CustomerAddressID]) REFERENCES [Customer].[Address]([CustomerAddressID]);
ALTER TABLE [Customer].[MyLocation] WITH NOCHECK ADD CONSTRAINT [FK_MyLocation_Profile] FOREIGN KEY ([CustomerID]) REFERENCES [Customer].[Profile]([CustomerID]);
ALTER TABLE [Customer].[TINData] WITH NOCHECK ADD CONSTRAINT [FK_TINData_Profile] FOREIGN KEY ([CustomerID]) REFERENCES [Customer].[Profile]([CustomerID]);
ALTER TABLE [Notification].[Feedback] WITH NOCHECK ADD CONSTRAINT [FK_Feedback_Profile] FOREIGN KEY ([CustomerId]) REFERENCES [Customer].[Profile]([CustomerID]);
ALTER TABLE [Customer].[Address] WITH NOCHECK ADD CONSTRAINT [FK_Address_Profile] FOREIGN KEY ([CustomerID]) REFERENCES [Customer].[Profile]([CustomerID]);
ALTER TABLE [Customer].[CustomerLookup] WITH NOCHECK ADD CONSTRAINT [FK_CustomerLookup_Profile] FOREIGN KEY ([CustomerID]) REFERENCES [Customer].[Profile]([CustomerID]);
ALTER TABLE [Customer].[CustomerPaymentMethod] WITH NOCHECK ADD CONSTRAINT [FK_CustomerPaymentMethod_Profile] FOREIGN KEY ([CustomerID]) REFERENCES [Customer].[Profile]([CustomerID]);
ALTER TABLE [Customer].[AuthToken] WITH NOCHECK ADD CONSTRAINT [FK_AuthToken_Profile] FOREIGN KEY ([CustomerID]) REFERENCES [Customer].[Profile]([CustomerID]);
ALTER TABLE [Customer].[LoyaltyMembershipNumber] WITH NOCHECK ADD CONSTRAINT [FK_LoyaltyMembershipNumber_Profile] FOREIGN KEY ([CustomerID]) REFERENCES [Customer].[Profile]([CustomerID]);
ALTER TABLE [Customer].[Favorite] WITH NOCHECK ADD CONSTRAINT [FK_Favorite_Profile] FOREIGN KEY ([CustomerID]) REFERENCES [Customer].[Profile]([CustomerID]);
ALTER TABLE [Customer].[SocialNetwork] WITH NOCHECK ADD CONSTRAINT [FK_SocialNetwork_Profile] FOREIGN KEY ([CustomerID]) REFERENCES [Customer].[Profile]([CustomerID]);
ALTER TABLE [Customer].[Preference] WITH NOCHECK ADD CONSTRAINT [FK_Preference_Profile] FOREIGN KEY ([CustomerID]) REFERENCES [Customer].[Profile]([CustomerID]);
ALTER TABLE [Customer].[MobileDevices] WITH NOCHECK ADD CONSTRAINT [FK_MobileDevices_Profile] FOREIGN KEY ([CustomerID]) REFERENCES [Customer].[Profile]([CustomerID]);
ALTER TABLE [Customer].[DynamicOptin] WITH NOCHECK ADD CONSTRAINT [FK_DynamicOptIn_Profile] FOREIGN KEY ([CustomerID]) REFERENCES [Customer].[Profile]([CustomerID]);
ALTER TABLE [Customer].[Optin] WITH NOCHECK ADD CONSTRAINT [FK_OptIn_Profile] FOREIGN KEY ([CustomerID]) REFERENCES [Customer].[Profile]([CustomerID]);
ALTER TABLE [Customer].[Acceptance] WITH NOCHECK ADD CONSTRAINT [FK_Acceptance_Profile] FOREIGN KEY ([CustomerID]) REFERENCES [Customer].[Profile]([CustomerID]);
ALTER TABLE [Customer].[Subscription] WITH NOCHECK ADD CONSTRAINT [FK_Subscription_Profile] FOREIGN KEY ([CustomerID]) REFERENCES [Customer].[Profile]([CustomerID]);


-- The query optimizer does not consider constraints that are defined WITH NOCHECK. Such constraints are ignored until they are re-enabled by using ALTER TABLE <table> WITH CHECK CHECK CONSTRAINT ALL.

ALTER TABLE [Customer].[MyLocation] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[MobileDevices] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[DynamicOptin] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[Acceptance] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[Subscription] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[Preference] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[CustomerPaymentMethod] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[Favorite] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[AuthToken] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[Address] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[TINData] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[Profile] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[Optin] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Notification].[Feedback] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[SocialNetwork] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[SocialNetworkAccessToken] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Customer].[LoyaltyMembershipNumber] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Authentication].[Authentication] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Authentication].[AuthInfo] WITH CHECK CHECK CONSTRAINT ALL;