# Overview

* Migrate Sql Server data between source and target tables
* The target table is using varchar instead of nvarchar data types
* The objective is to measure the performance and to make the migration process resumable

## [Customer Preference](./customer-preference.md)

## Suggestions for ddl

* [Customer].[Preference] table seems to have an unnecessary index: [Preference_Customerid_INCL1_NU_NC]
* there is [IX_Preference_CustomerID] that would/could serve the same purpose
* Few tables seem to contain two indexes on CustomerID: one with included columns and one without them
* Maybe there is a usage stats that justify the two indices, from ddl it is not as obvious


