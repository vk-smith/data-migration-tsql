# Order of execution

## Rename to _old
1. remove-fks-rename-old-tables-Notification.sql
2. remove-fks-rename-old-tables-customer.sql
3. remove-fks-rename-old-tables-Authentication.sql
4. remove-fks-rename-old-tables-order.sql

## Remove _tmp suffix
1. rename-tmp-tables-create-fks-customer.sql
2. rename-tmp-tables-create-fks-customer.sql
3. rename-tmp-tables-create-fks-Authentication.sql
4. rename-tmp-tables-create-fks-order.sql

## Drop _old tables
1. drop-old-tables-Authentication.sql
2. drop-old-tables-customer.sql
3. drop-old-tables-Notification.sql
4. drop-old-tables-order.sql