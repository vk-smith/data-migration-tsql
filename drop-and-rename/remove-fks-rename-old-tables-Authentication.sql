USE <EcpCustomer>;


ALTER TABLE [Authentication].[AuthToInfo]  DROP CONSTRAINT [FK_AuthToInfo_Authentication]
ALTER TABLE [Authentication].[AuthToInfo] DROP CONSTRAINT [FK_AuthToInfo_AuthInfo]



EXEC sp_rename 'Authentication.Authentication', 'Authentication_old';
EXEC sp_rename 'Authentication.PK_Authentication', 'PK_Authentication_old';
EXEC sp_rename 'Authentication.DF_Authentication_LoginSourceType', 'DF_Authentication_LoginSourceType_old';
EXEC sp_rename 'Authentication.AuthInfo', 'AuthInfo_old';
EXEC sp_rename 'Authentication.PK_AuthInfo', 'PK_AuthInfo_old';
EXEC sp_rename 'Authentication.DF_AuthInfo_PasswordEncrypterType', 'DF_AuthInfo_PasswordEncrypterType_old';




