USE <EcpOrderDB>

EXEC sp_rename 'OrderTaking.SerializedData', 'SerializedData_old';
EXEC sp_rename 'OrderTaking.PK_SerializedData', 'PK_SerializedData_old';
EXEC sp_rename 'OrderTaking.DF_SerializedDataLastModification', 'DF_SerializedData_LastModification_old';