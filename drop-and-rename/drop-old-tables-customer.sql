USE <EcpCustomer>;

DROP TABLE [Customer].[MyLocation_old];
DROP TABLE [Customer].[MobileDevices_old];
DROP TABLE [Customer].[DynamicOptin_old];
DROP TABLE [Customer].[Acceptance_old];
DROP TABLE [Customer].[Subscription] 
DROP TABLE [Customer].[Preference_old];
DROP TABLE [Customer].[CustomerPaymentMethod_old];
DROP TABLE [Customer].[Favorite_old];
DROP TABLE [Customer].[AuthToken_old];
DROP TABLE [Customer].[Address_old];
DROP TABLE [Customer].[TINData_old];
DROP TABLE [Customer].[Profile_old];
DROP TABLE [Customer].[Optin_old];
DROP TABLE [Customer].[SocialNetwork_old];
DROP TABLE [Customer].[SocialNetworkAccessToken_old]; 
DROP TABLE [Customer].[LoyaltyMembershipNumber_old];
