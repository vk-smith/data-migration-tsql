USE <EcpOrderDB>

EXEC sp_rename 'OrderTaking.SerializedData_tmp', 'SerializedData';
EXEC sp_rename 'OrderTaking.PK_SerializedData_tmp', 'PK_SerializedData';
EXEC sp_rename 'OrderTaking.DF_SerializedData_tmp_LastModification', 'DF_SerializedData_LastModification';