USE <EcpCustomer>;



EXEC sp_rename 'Notification.Feedback_tmp', 'Feedback';
EXEC sp_rename 'Notification.PK_Feedback_tmp', 'PK_Feedback';
EXEC sp_rename 'Notification.DF_Feedback_tmp_LastModification', 'DF_Feedback_LastModification';




ALTER TABLE [Notification].[Feedback] WITH NOCHECK ADD CONSTRAINT [FK_Feedback_FeedbackType] FOREIGN KEY ([FeedbackTypeID]) REFERENCES [Dictionary].[FeedbackType]([FeedbackTypeID]);
-- run next after Customer.Profile_tmp table will be renamed to Customer.Profile
ALTER TABLE [Notification].[Feedback] WITH NOCHECK ADD CONSTRAINT [FK_Feedback_Profile] FOREIGN KEY ([CustomerId]) REFERENCES [Customer].[Profile]([CustomerID]);

-- The query optimizer does not consider constraints that are defined WITH NOCHECK. Such constraints are ignored until they are re-enabled by using ALTER TABLE <table> WITH CHECK CHECK CONSTRAINT ALL.

ALTER TABLE [Notification].[Feedback] WITH CHECK CHECK CONSTRAINT ALL;
