USE <EcpCustomer>;

ALTER TABLE [Customer].[Favorite] DROP CONSTRAINT [FK_Favorite_FavoriteType]
ALTER TABLE [Customer].[AddressPhoneLookup] DROP CONSTRAINT [FK_Profile_AddressPhoneLookup]
ALTER TABLE [Customer].[MyLocation] DROP CONSTRAINT [FK_MyLocation_Profile]
ALTER TABLE [Customer].[TINData] DROP CONSTRAINT [FK_TINData_Profile]
-- ALTER TABLE [Notification].[Feedback] DROP CONSTRAINT [FK_Feedback_Profile] --uncomment if run this script before remove-fks-rename-old-tables-Notification.sql 
ALTER TABLE [Customer].[Address] DROP CONSTRAINT [FK_Address_Profile]
ALTER TABLE [Customer].[CustomerLookup] DROP CONSTRAINT [FK_CustomerLookup_Profile] 
ALTER TABLE [Customer].[CustomerPaymentMethod] DROP CONSTRAINT [FK_CustomerPaymentMethod_Profile] 
ALTER TABLE [Customer].[AuthToken] DROP CONSTRAINT [FK_AuthToken_Profile] 
ALTER TABLE [Customer].[LoyaltyMembershipNumber] DROP CONSTRAINT [FK_LoyaltyMembershipNumber_Profile] 
ALTER TABLE [Customer].[Favorite] DROP CONSTRAINT [FK_Favorite_Profile] 
ALTER TABLE [Customer].[SocialNetwork] DROP CONSTRAINT [FK_SocialNetwork_Profile] 
ALTER TABLE [Customer].[Preference] DROP CONSTRAINT [FK_Preference_Profile] 
ALTER TABLE [Customer].[MobileDevices] DROP CONSTRAINT [FK_MobileDevices_Profile] 
ALTER TABLE [Customer].[DynamicOptin] DROP CONSTRAINT [FK_DynamicOptIn_Profile] 
ALTER TABLE [Customer].[Optin] DROP CONSTRAINT [FK_OptIn_Profile] 
ALTER TABLE [Customer].[Acceptance] DROP CONSTRAINT [FK_Acceptance_Profile] 
ALTER TABLE [Customer].[Subscription] DROP CONSTRAINT [FK_Subscription_Profile] 



EXEC sp_rename 'Customer.MyLocation', 'MyLocation_old';
EXEC sp_rename 'Customer.PK_MyLocation', 'PK_MyLocation_old';
EXEC sp_rename 'Customer.MobileDevices', 'MobileDevices_old';
EXEC sp_rename 'Customer.PK_MobileDevices', 'PK_MobileDevices_old';
EXEC sp_rename 'Customer.DynamicOptin', 'DynamicOptin_old';
EXEC sp_rename 'Customer.PK_DynamicOptIn', 'PK_DynamicOptIn_old';
EXEC sp_rename 'Customer.Acceptance', 'Acceptance_old';
EXEC sp_rename 'Customer.PK_Acceptance', 'PK_Acceptance_old';
EXEC sp_rename 'Customer.Subscription', 'Subscription_old';
EXEC sp_rename 'Customer.PK_Subscription', 'PK_Subscription_old';
EXEC sp_rename 'Customer.Preference', 'Preference_old';
EXEC sp_rename 'Customer.PK_Preference', 'PK_Preference_old';
EXEC sp_rename 'Customer.DF_Preference_LastModification', 'DF_Preference_LastModification_old';
EXEC sp_rename 'Customer.CustomerPaymentMethod', 'CustomerPaymentMethod_old';
EXEC sp_rename 'Customer.PK_CustomerPaymentMethod', 'PK_CustomerPaymentMethod_old';
EXEC sp_rename 'Customer.Favorite', 'Favorite_old';
EXEC sp_rename 'Customer.PK_Favorite', 'PK_Favorite_old';
EXEC sp_rename 'Customer.AuthToken', 'AuthToken_old';
EXEC sp_rename 'Customer.PK_AuthToken', 'PK_AuthToken_old';
EXEC sp_rename 'Customer.DF_AuthTOken_LastModification', 'DF_AuthTOken_LastModification_old';
EXEC sp_rename 'Customer.Address', 'Address_old';
EXEC sp_rename 'Customer.PK_Address', 'PK_Address_old';
EXEC sp_rename 'Customer.TINData', 'TINData_old';
EXEC sp_rename 'Customer.PK_TINData', 'PK_TINData_old';
EXEC sp_rename 'Customer.DF_TINData_LastModification', 'DF_TINData_LastModification_old';
EXEC sp_rename 'Customer.Profile', 'Profile_old';
EXEC sp_rename 'Customer.PK_Profile', 'PK_Profile_old';
EXEC sp_rename 'Customer.Optin', 'Optin_old';
EXEC sp_rename 'Customer.PK_Optin', 'PK_Optin_old';
EXEC sp_rename 'Customer.SocialNetwork', 'SocialNetwork_old';
EXEC sp_rename 'Customer.PK_SocialNetwork', 'PK_SocialNetwork_old';
EXEC sp_rename 'Customer.SocialNetworkAccessToken', 'SocialNetworkAccessToken_old';
EXEC sp_rename 'Customer.PK_SocialNetworkAccessToken', 'PK_SocialNetworkAccessToken_old';
EXEC sp_rename 'Customer.LoyaltyMembershipNumber', 'LoyaltyMembershipNumber_old';
EXEC sp_rename 'Customer.PK_LoyaltyMembershipNumber', 'PK_LoyaltyMembershipNumber_old';
EXEC sp_rename 'Customer.DF_LoyaltyMembershipNumber_LastModificationUTC', 'DF_LoyaltyMembershipNumber_LastModificationUTC_old';





