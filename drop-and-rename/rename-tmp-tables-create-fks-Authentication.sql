USE <EcpCustomer>;


EXEC sp_rename 'Authentication.Authentication_tmp', 'Authentication';
EXEC sp_rename 'Authentication.PK_Authentication_tmp', 'PK_Authentication';
EXEC sp_rename 'Authentication.DF_Authentication_tmp_LoginSourceType', 'DF_Authentication_LoginSourceType';
EXEC sp_rename 'Authentication.AuthInfo_tmp', 'AuthInfo';
EXEC sp_rename 'Authentication.PK_AuthInfo_tmp', 'PK_AuthInfo';
EXEC sp_rename 'Authentication.DF_AuthInfo_tmp_PasswordEncrypterType', 'DF_AuthInfo_PasswordEncrypterType';




ALTER TABLE [Authentication].[AuthToInfo]  WITH NOCHECK ADD CONSTRAINT [FK_AuthToInfo_Authentication] FOREIGN KEY ([AuthTokenID]) REFERENCES [Authentication].[Authentication]([Token]);
ALTER TABLE [Authentication].[AuthToInfo] WITH NOCHECK ADD CONSTRAINT [FK_AuthToInfo_AuthInfo] FOREIGN KEY ([AuthInfoID]) REFERENCES [Authentication].[AuthInfo]([AuthInfoID]);

-- The query optimizer does not consider constraints that are defined WITH NOCHECK. Such constraints are ignored until they are re-enabled by using ALTER TABLE <table> WITH CHECK CHECK CONSTRAINT ALL.


--ALTER TABLE [Authentication].[Authentication] WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE [Authentication].[AuthInfo] WITH CHECK CHECK CONSTRAINT ALL;