# Overview

* Migrate customer preference data between source and target tables
* The target table is using varchar instead of nvarchar data types
* The objective is to measure the performance and to make the migration process resumable

# Design
* copy_table_by_chunks.sql script which copies [Customer].[Preference] data to the [Customer].[Preference_tmp] table
* the script assumes the [Customer].[Preference] table has a Primary Key and uses the key to copy the data by chunks
* also it supports resuming the process from the last copied id in case of interruption.

# Results of execution
```sql
Create table [Customer].[Preference_tmp]
2017-12-28 17:50:26 insert into [Customer].[Preference_tmp] 1000000 rows starting from [CustomerPreferenceID] = 0

(1000000 rows affected)
2017-12-28 17:50:32 insert into [Customer].[Preference_tmp] 1000000 rows starting from [CustomerPreferenceID] = 16878364

(1000000 rows affected)

...

2017-12-28 18:31:01 insert into [Customer].[Preference_tmp] 1000000 rows starting from [CustomerPreferenceID] = 453816471

(1000000 rows affected)
2017-12-28 18:31:19 insert into [Customer].[Preference_tmp] 1000000 rows starting from [CustomerPreferenceID] = 454816471

(930748 rows affected)
```

# Measured performance

* Copy data: 41 minutes 10 seconds
* Create index [IX_Preference_CustomerID] ON [Customer].[Preference_tmp]: 1 hour 14 minutes 1 second.
* Create index [Preference_Customerid_INCL1_NU_NC] ON [Customer].[Preference_tmp]: 25 minutes 40 seconds.
* Create index [IX_Preference_LastModification_CustomerID] ON [Customer].[Preference_tmp]: 1 hour 11 minutes 50 seconds.
