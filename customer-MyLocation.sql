DECLARE @Start_CustomerMyLocationID int
		,@SizeOfChunk int = 1000000 -- the number of rows to be copied in one transaction

IF NOT EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[MyLocation_tmp]') AND [type]='U')
Begin
 print 'Create table [Customer].[MyLocation_tmp]'
CREATE TABLE [Customer].[MyLocation_tmp](
	[CustomerMyLocationID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[Name] [nvarchar](128) NULL,
	[POSStoreNumber] [nvarchar](128) NULL,
	[IsValid] [bit] NOT NULL,
	[LastModification] [datetime] NOT NULL DEFAULT (getutcdate()),
	[SerializedEncryptedValues] [varchar](2000) NULL,
 CONSTRAINT [PK_MyLocation_tmp] PRIMARY KEY CLUSTERED 
(
	[CustomerMyLocationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)


End

-- find out the last copied row:
SET @Start_CustomerMyLocationID = (SELECT ISNULL(MAX(CustomerMyLocationID), 0) FROM [Customer].[MyLocation_tmp]);


-- copying rows ordered by [CustomerMyLocationID]:
WHILE @Start_CustomerMyLocationID < (SELECT MAX(CustomerMyLocationID) FROM [Customer].[MyLocation])
BEGIN

Print CONVERT(char(20), GETDATE(), 120) + 'insert into [Customer].[MyLocation_tmp] ' + CAST(@SizeOfChunk AS varchar(11)) + ' rows starting from [CustomerMyLocationID] = ' + CAST(@Start_CustomerMyLocationID AS varchar(11));
set identity_insert [Customer].[MyLocation_tmp] ON
begin tran t1
insert into [Customer].[MyLocation_tmp]
	  ([CustomerMyLocationID]
      ,[CustomerID]
      ,[Name]
      ,[POSStoreNumber]
      ,[IsValid]
      ,[LastModification]
      ,[SerializedEncryptedValues])
SELECT [CustomerMyLocationID]
      ,[CustomerID]
      ,[Name]
      ,[POSStoreNumber]
      ,[IsValid]
      ,[LastModification]
      ,[SerializedEncryptedValues]
  FROM [Customer].[MyLocation]
 WHERE [CustomerMyLocationID] > @Start_CustomerMyLocationID
  ORDER BY [CustomerMyLocationID] 
  OFFSET 0 ROWS FETCH NEXT @SizeOfChunk ROWS ONLY
commit tran t1
set identity_insert [Customer].[MyLocation_tmp] OFF
SET @Start_CustomerMyLocationID = (SELECT MAX([CustomerMyLocationID]) FROM [Customer].[MyLocation_tmp])
END;

