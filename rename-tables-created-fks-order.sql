USE <EcpOrderDB>

EXEC sp_rename 'OrderTaking.SerializedData_tmp', 'SerializedData';
EXEC sp_rename 'PK_SerializedData_tmp', 'PK_SerializedData';
EXEC sp_rename 'DF_SerializedData_tmp_LastModification', 'DF_SerializedData_LastModification';