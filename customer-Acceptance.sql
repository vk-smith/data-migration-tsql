DECLARE @Start_CustomerAcceptanceID int
		,@SizeOfChunk int = 1000000 -- the number of rows to be copied in one transaction

IF NOT EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Acceptance_tmp]') AND [type]='U')
Begin
 print 'Create table [Customer].[Acceptance_tmp]'
CREATE TABLE [Customer].[Acceptance_tmp](
	[CustomerAcceptanceID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[AcceptanceTypeID] [tinyint] NULL,
	[Description] [nvarchar](max) NULL,
	[AcceptedAt] [datetime] NULL,
	[AcceptanceFlag] [bit] NULL,
	[LastModification] [datetime] NOT NULL DEFAULT (getutcdate()),
	[SerializedEncryptedValues] [varchar](2000) NULL,
	[ApplicationID] [int] NULL,
 CONSTRAINT [PK_Acceptance_tmp] PRIMARY KEY CLUSTERED 
(
	[CustomerAcceptanceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)


End

-- find out the last copied row:
SET @Start_CustomerAcceptanceID = (SELECT ISNULL(MAX(CustomerAcceptanceID), 0) FROM [Customer].[Acceptance_tmp]);


-- copying rows ordered by [CustomerAcceptanceID]:
WHILE @Start_CustomerAcceptanceID < (SELECT MAX(CustomerAcceptanceID) FROM [Customer].[Acceptance])
BEGIN

Print CONVERT(char(20), GETDATE(), 120) + 'insert into [Customer].[Acceptance_tmp] ' + CAST(@SizeOfChunk AS varchar(11)) + ' rows starting from [CustomerAcceptanceID] = ' + CAST(@Start_CustomerAcceptanceID AS varchar(11));
set identity_insert [Customer].[Acceptance_tmp] ON
begin tran t1
insert into [Customer].[Acceptance_tmp]
([CustomerAcceptanceID]
      ,[CustomerID]
      ,[AcceptanceTypeID]
      ,[Description]
      ,[AcceptedAt]
      ,[AcceptanceFlag]
      ,[LastModification]
      ,[SerializedEncryptedValues]
      ,[ApplicationID])
SELECT [CustomerAcceptanceID]
      ,[CustomerID]
      ,[AcceptanceTypeID]
      ,[Description]
      ,[AcceptedAt]
      ,[AcceptanceFlag]
      ,[LastModification]
      ,[SerializedEncryptedValues]
      ,[ApplicationID]
  FROM [Customer].[Acceptance]
 WHERE [CustomerAcceptanceID] > @Start_CustomerAcceptanceID
  ORDER BY [CustomerAcceptanceID] 
  OFFSET 0 ROWS FETCH NEXT @SizeOfChunk ROWS ONLY
commit tran t1
set identity_insert [Customer].[Acceptance_tmp] OFF
SET @Start_CustomerAcceptanceID = (SELECT MAX([CustomerAcceptanceID]) FROM [Customer].[Acceptance_tmp])
END;



Print CONVERT(char(20), GETDATE(), 120) + 'Create Index IX_Acceptance_CustomerID on Customer.Acceptance_tmp'
CREATE NONCLUSTERED INDEX [IX_Acceptance_CustomerID] ON [Customer].[Acceptance_tmp]
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	on customer_index
Print CONVERT(char(20), GETDATE(), 120) + 'Index IX_Acceptance_CustomerID on Customer.Acceptance_tmp created'


Print CONVERT(char(20), GETDATE(), 120) + 'Create index Acceptance_CustomeridCustomeracceptanceid_INCL7_NU_NC on Customer.Acceptance_tmp'
CREATE NONCLUSTERED INDEX [Acceptance_CustomeridCustomeracceptanceid_INCL7_NU_NC] ON [Customer].[Acceptance_tmp]
(
	[CustomerID] ASC,
	[CustomerAcceptanceID] ASC
)
INCLUDE ( 	[AcceptanceTypeID],
	[Description],
	[AcceptedAt],
	[AcceptanceFlag],
	[LastModification],
	[SerializedEncryptedValues],
	[ApplicationID]) 
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	on customer_index
Print CONVERT(char(20), GETDATE(), 120) + 'Index Acceptance_CustomeridCustomeracceptanceid_INCL7_NU_NC on Customer.Acceptance_tmp created'