DECLARE @Start_CustomerID int
		,@Start_DeviceTokenHash nvarchar(32)
		,@SizeOfChunk int = 1000000 -- the number of rows to be copied in one transaction

IF NOT EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[MobileDevices_tmp]') AND [type]='U')
Begin
 print 'Create table [Customer].[MobileDevices_tmp]'
CREATE TABLE [Customer].[MobileDevices_tmp](
	[CustomerID] [int] NOT NULL,
	[DeviceTokenHash] [nvarchar](32) NOT NULL,
	[DeviceToken] [nvarchar](max) NULL,
	[DeviceOperatingSystem] [nvarchar](64) NULL,
	[LastModification] [datetime] NOT NULL,
	[SerializedEncryptedValues] [varchar](2000) NULL,
 CONSTRAINT [PK_MobileDevices_tmp] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC,
	[DeviceTokenHash] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)


End

-- find out the last copied row:
SET @Start_CustomerID = (SELECT ISNULL(MAX([CustomerID]), 0) FROM [Customer].[MobileDevices_tmp]);
SET @Start_DeviceTokenHash = (SELECT  ISNULL(MAX([DeviceTokenHash]), '') FROM [Customer].[MobileDevices_tmp] WHERE [CustomerID] = @Start_CustomerID)


-- copying rows ordered by [CustomerID] and [DeviceTokenHash]:
WHILE @Start_CustomerID < (SELECT MAX([CustomerID]) FROM [Customer].[MobileDevices]) 
		OR (@Start_CustomerID = (SELECT MAX([CustomerID]) FROM [Customer].[MobileDevices]) AND @Start_DeviceTokenHash < (SELECT MAX([DeviceTokenHash]) FROM [Customer].[MobileDevices] WHERE [CustomerID] = @Start_CustomerID))

BEGIN

Print CONVERT(char(20), GETDATE(), 120) + 'insert into [Customer].[MobileDevices_tmp] ' + CAST(@SizeOfChunk AS varchar(11)) + ' rows starting from [CustomerID] = ' + CAST(@Start_CustomerID AS varchar(11));
begin tran t1
insert into [Customer].[MobileDevices_tmp]
	  ([CustomerID]
      ,[DeviceTokenHash]
      ,[DeviceToken]
      ,[DeviceOperatingSystem]
      ,[LastModification]
      ,[SerializedEncryptedValues])
SELECT distinct [CustomerID]
      ,[DeviceTokenHash]
      ,[DeviceToken]
      ,[DeviceOperatingSystem]
      ,[LastModification]
      ,[SerializedEncryptedValues]
  FROM [Customer].[MobileDevices]
 WHERE [CustomerID] > @Start_CustomerID
	OR ([CustomerID] = @Start_CustomerID AND [DeviceTokenHash] > @Start_DeviceTokenHash)
  ORDER BY [CustomerID], [DeviceTokenHash] 
  OFFSET 0 ROWS FETCH NEXT @SizeOfChunk ROWS ONLY
commit tran t1

SET @Start_CustomerID = (SELECT MAX([CustomerID]) FROM [Customer].[MobileDevices_tmp]);
SET @Start_DeviceTokenHash = (SELECT  MAX([DeviceTokenHash]) FROM [Customer].[MobileDevices_tmp] WHERE [CustomerID] = @Start_CustomerID)
END;


ALTER TABLE [Customer].[MobileDevices_tmp] ADD  DEFAULT (getutcdate()) FOR [LastModification]


Print CONVERT(char(20), GETDATE(), 120) + 'Create index IX_MobileDevices_LastModification_CustomerID on Customer.MobileDevices_tmp'
CREATE NONCLUSTERED INDEX [IX_MobileDevices_LastModification_CustomerID] ON [Customer].[MobileDevices_tmp]
(
	[LastModification] ASC,
	[CustomerID] ASC
)
INCLUDE ( 	[DeviceTokenHash],
	[DeviceToken],
	[DeviceOperatingSystem],
	[SerializedEncryptedValues]) 
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	ON Customer_Index
Print CONVERT(char(20), GETDATE(), 120) + 'Index IX_MobileDevices_LastModification_CustomerID on Customer.MobileDevices_tmp created'


