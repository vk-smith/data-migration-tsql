/*
USE [perf8EcpCustomer]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [Customer].[AuthToken](
	[CustomerID] [int] NOT NULL,
	[AuthTokenID] [uniqueidentifier] NOT NULL,
	[Provider] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastModification] [datetime] NOT NULL,
	[SerializedEncryptedValues] [nvarchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_AuthToken] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC,
	[AuthTokenID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90)
)


SET ANSI_PADDING ON


CREATE NONCLUSTERED INDEX [IX_AuthToken_AuthTokenID] ON [Customer].[AuthToken]
(
	[AuthTokenID] ASC
)
INCLUDE ( 	[CustomerID],
	[Provider],
	[LastModification],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90)

ALTER TABLE [Customer].[AuthToken] ADD  CONSTRAINT [DF_AuthTOken_LastModification]  DEFAULT (getutcdate()) FOR [LastModification]

ALTER TABLE [Customer].[AuthToken]  WITH CHECK ADD  CONSTRAINT [FK_AuthToken_Profile] FOREIGN KEY([CustomerID])
REFERENCES [Customer].[Profile] ([CustomerID])

ALTER TABLE [Customer].[AuthToken] CHECK CONSTRAINT [FK_AuthToken_Profile]


GO
*/

DECLARE @Start_CustomerID int
		,@Start_AuthTokenID uniqueidentifier
		,@SizeOfChunk int = 1000000 -- the number of rows to be copied in one transaction

IF NOT EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[AuthToken_tmp]') AND [type]='U')
Begin
 print 'Create table [Customer].[AuthToken_tmp]'
CREATE TABLE [Customer].[AuthToken_tmp](
	[CustomerID] [int] NOT NULL,
	[AuthTokenID] [uniqueidentifier] NOT NULL,
	[Provider] [nvarchar](128) NULL,
	[LastModification] [datetime] NOT NULL ,
	[SerializedEncryptedValues] [varchar](2000) NULL,
 CONSTRAINT [PK_AuthToken_tmp] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC,
	[AuthTokenID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


End

-- find out the last copied row:
SET @Start_CustomerID = (SELECT ISNULL(MAX([CustomerID]), 0) FROM [Customer].[AuthToken_tmp]);
SET @Start_AuthTokenID = (SELECT  ISNULL(MAX([AuthTokenID]), '00000000-0000-0000-0000-000000000000') FROM [Customer].[AuthToken_tmp] WHERE [CustomerID] = @Start_CustomerID)


-- copying rows ordered by [CustomerID] and [AuthTokenID]:
WHILE @Start_CustomerID < (SELECT MAX([CustomerID]) FROM [Customer].[AuthToken]) 
		OR (@Start_CustomerID = (SELECT MAX([CustomerID]) FROM [Customer].[AuthToken]) AND @Start_AuthTokenID < (SELECT MAX([AuthTokenID]) FROM [Customer].[AuthToken] WHERE [CustomerID] = @Start_CustomerID))

BEGIN

Print CONVERT(char(20), GETDATE(), 120) + 'insert into [Customer].[AuthToken_tmp] ' + CAST(@SizeOfChunk AS varchar(11)) + ' rows starting from [CustomerID] = ' + CAST(@Start_CustomerID AS varchar(11));
begin tran t1
insert into [Customer].[AuthToken_tmp]
	 ([CustomerID]
      ,[AuthTokenID]
      ,[Provider]
      ,[LastModification]
      ,[SerializedEncryptedValues])
SELECT [CustomerID]
      ,[AuthTokenID]
      ,[Provider]
      ,[LastModification]
      ,[SerializedEncryptedValues]
  FROM [Customer].[AuthToken]
 WHERE [CustomerID] > @Start_CustomerID
	OR ([CustomerID] = @Start_CustomerID AND [AuthTokenID] > @Start_AuthTokenID)
  ORDER BY [CustomerID], [AuthTokenID] 
  OFFSET 0 ROWS FETCH NEXT @SizeOfChunk ROWS ONLY
commit tran t1

SET @Start_CustomerID = (SELECT MAX([CustomerID]) FROM [Customer].[AuthToken_tmp]);
SET @Start_AuthTokenID = (SELECT  MAX([AuthTokenID]) FROM [Customer].[AuthToken_tmp] WHERE [CustomerID] = @Start_CustomerID)
END;


ALTER TABLE [Customer].[AuthToken_tmp] ADD  CONSTRAINT [DF_AuthTOken_tmp_LastModification]  DEFAULT (getutcdate()) FOR [LastModification]


Print CONVERT(char(20), GETDATE(), 120) + 'Create index [IX_AuthToken_AuthTokenID] ON [Customer].[AuthToken_tmp]'
CREATE NONCLUSTERED INDEX [IX_AuthToken_AuthTokenID] ON [Customer].[AuthToken_tmp]
(
	[AuthTokenID] ASC
)
INCLUDE ( 	[CustomerID],
	[Provider],
	[LastModification],
	[SerializedEncryptedValues]) 
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	on customer_index
Print CONVERT(char(20), GETDATE(), 120) + 'Index [IX_AuthToken_AuthTokenID] ON [Customer].[AuthToken_tmp] created'


Print CONVERT(char(20), GETDATE(), 120) + 'Create Index AuthToken_Customerid_NU_NC ON [Customer].[AuthToken_tmp]'
	CREATE NONCLUSTERED INDEX [AuthToken_Customerid_NU_NC] ON [Customer].[AuthToken_tmp]
	(
		[CustomerID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	ON customer_index
Print CONVERT(char(20), GETDATE(), 120) + 'Index AuthToken_Customerid_NU_NC ON [Customer].[AuthToken_tmp] created'