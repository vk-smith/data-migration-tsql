DECLARE @Start_CustomerFavoriteID int
		,@SizeOfChunk int = 1000000 -- the number of rows to be copied in one transaction

IF NOT EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[Favorite_tmp]') AND [type]='U')
Begin
 print 'Create table [Customer].[Favorite_tmp]'
CREATE TABLE [Customer].[Favorite_tmp](
	[CustomerFavoriteID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[Type] [int] NULL,
	[Name] [nvarchar](128) NULL,
	[Data] [nvarchar](2000) NULL,
	[LastModification] [datetime] NOT NULL,
	[SerializedEncryptedValues] [varchar](max) NULL,
 CONSTRAINT [PK_Favorite_tmp] PRIMARY KEY CLUSTERED 
(
	[CustomerFavoriteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [fg1]
) ON [fg1] TEXTIMAGE_ON [fg1]


End

-- find out the last copied row:
SET @Start_CustomerFavoriteID = (SELECT ISNULL(MAX(CustomerFavoriteID), 0) FROM [Customer].[Favorite_tmp]);


-- copying rows ordered by [CustomerFavoriteID]:
WHILE @Start_CustomerFavoriteID < (SELECT MAX(CustomerFavoriteID) FROM [Customer].[Favorite])
BEGIN

Print CONVERT(char(20), GETDATE(), 120) + 'insert into [Customer].[Favorite_tmp] ' + CAST(@SizeOfChunk AS varchar(11)) + ' rows starting from [CustomerFavoriteID] = ' + CAST(@Start_CustomerFavoriteID AS varchar(11));
set identity_insert [Customer].[Favorite_tmp] ON
begin tran t1
insert into [Customer].[Favorite_tmp]
	  ([CustomerFavoriteID]
      ,[CustomerID]
      ,[Type]
      ,[Name]
      ,[Data]
      ,[LastModification]
      ,[SerializedEncryptedValues])
SELECT [CustomerFavoriteID]
      ,[CustomerID]
      ,[Type]
      ,[Name]
      ,[Data]
      ,[LastModification]
      ,[SerializedEncryptedValues]
  FROM [Customer].[Favorite]
 WHERE [CustomerFavoriteID] > @Start_CustomerFavoriteID
  ORDER BY [CustomerFavoriteID] 
  OFFSET 0 ROWS FETCH NEXT @SizeOfChunk ROWS ONLY
commit tran t1
set identity_insert [Customer].[Favorite_tmp] OFF
SET @Start_CustomerFavoriteID = (SELECT MAX(CustomerFavoriteID) FROM [Customer].[Favorite_tmp])
END;



Print CONVERT(char(20), GETDATE(), 120) + 'Create index [IX_Favorite_LastModification_CustomerID] ON [Customer].[Favorite_tmp]'
CREATE NONCLUSTERED INDEX [IX_Favorite_LastModification_CustomerID] ON [Customer].[Favorite_tmp]
(
	[LastModification] ASC,
	[CustomerID] ASC
)
INCLUDE ( 	[CustomerFavoriteID],
	[Type],
	[Name],
	[Data],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	on customer_index
Print CONVERT(char(20), GETDATE(), 120) + 'Index [IX_Favorite_LastModification_CustomerID] ON [Customer].[Favorite_tmp] created'

