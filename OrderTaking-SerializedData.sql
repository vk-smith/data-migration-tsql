DECLARE @Start_OrderId uniqueidentifier
DECLARE @SizeOfChunk int = 1000000 -- the number of rows to be copied in one transaction

if exists (select 1 from sys.objects where name = 'DF_SerializedData_LastModification' and type='D')
Begin
	print 'Drop DF_SerializedData_LastModification constraint'
	ALTER TABLE [OrderTaking].[SerializedData] DROP CONSTRAINT [DF_SerializedData_LastModification]
End	


IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[OrderTaking].[SerializedData]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_SerializedData_OrderID_OrderProtoBuf' 
	AND [object_id]=OBJECT_ID(N'[OrderTaking].[SerializedData]')))
Begin	
	Print 'Drop Index IX_SerializedData_OrderID_OrderProtoBuf ON [OrderTaking].[SerializedData]'
	DROP INDEX [IX_SerializedData_OrderID_OrderProtoBuf] ON [OrderTaking].[SerializedData]
End	


IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[OrderTaking].[SerializedData]') AND [type]='U')) AND 
	(EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'IX_SerializedData_OrderId' 
	AND [object_id]=OBJECT_ID(N'[OrderTaking].[SerializedData]')))
Begin	
	Print 'Drop Index IX_SerializedData_OrderId ON [OrderTaking].[SerializedData]'
	DROP INDEX [IX_SerializedData_OrderId] ON [OrderTaking].[SerializedData]
End	



IF NOT EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[OrderTaking].[SerializedData_tmp]') AND [type]='U')
Begin
 print 'Create table [OrderTaking].[SerializedData_tmp]'
CREATE TABLE [OrderTaking].[SerializedData_tmp](
	[OrderId] [uniqueidentifier] NOT NULL,
	[MarketID] [char](2) NOT NULL,
	[OrderProtoBuf] [varbinary](max) NULL,
	[LastModification] [datetime] NOT NULL ,
	[SerializedDataType] [nvarchar](500) NOT NULL,
	[SerializedProdInfo] [varchar](max) NULL,
	[SerializedOrderViewInput] [varchar](max) NULL,
	[SerializedEncryptedValues] [varchar](max) NULL,
	[OrderSeqId] [int] NOT NULL
 CONSTRAINT [PK_SerializedData1] PRIMARY KEY NONCLUSTERED 
(
	[OrderSeqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [fg1]
) ON [fg1] TEXTIMAGE_ON [fg1]

ALTER TABLE [OrderTaking].[SerializedData_tmp] ADD  CONSTRAINT [DF_SerializedData_tmp_LastModification]  DEFAULT (getutcdate()) FOR [LastModification]

End

Print 'Create index [IX_SerializedData_OrderId] ON [OrderTaking].[SerializedData]'
CREATE NONCLUSTERED INDEX [IX_SerializedData_OrderId] ON [OrderTaking].[SerializedData]
(
	[OrderId] ASC
)
INCLUDE ( 	[MarketID],
	[OrderProtoBuf],
	[LastModification],
	[SerializedDataType],
	[SerializedProdInfo],
	[SerializedOrderViewInput],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) 
	ON [FG2]


Print 'Create index [IX_SerializedData_OrderId_OrderProtoBuf] ON [OrderTaking].[SerializedData]'
CREATE NONCLUSTERED INDEX [IX_SerializedData_OrderId_OrderProtoBuf] ON [OrderTaking].[SerializedData]
(
	[OrderId] ASC
)
INCLUDE ( 	[OrderProtoBuf]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) 
ON [FG2]


IF (EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[OrderTaking].[SerializedData]') AND [type]='U')) AND 
	NOT (EXISTS (SELECT * FROM sys.indexes WHERE [name]=N'SerializedData_tmp_Orderid_NU_NC' 
	AND [object_id]=OBJECT_ID(N'[OrderTaking].[SerializedData_tmp]')))
Begin	
	Print 'Create Index SerializedData_tmp_Orderid_NU_NC ON [OrderTaking].[SerializedData_tma]'
	CREATE CLUSTERED INDEX [SerializedData_tmp_Orderid_NU_NC] ON [OrderTaking].[SerializedData_tmp]
	(
		[OrderId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80)
	on FG2
End	


-- find out the last copied row:
SET @Start_OrderId = (SELECT ISNULL(MAX(OrderId), '00000000-0000-0000-0000-000000000000') FROM [OrderTaking].[SerializedData_tmp]);


-- copying rows ordered by [OrderId]:
WHILE @Start_OrderId < (SELECT MAX(OrderId) FROM [OrderTaking].[SerializedData])
BEGIN

Print CONVERT(char(20), GETDATE(), 120) + 'insert into [OrderTaking].[SerializedData_tmp] ' + CAST(@SizeOfChunk AS varchar(11)) + ' rows starting from [OrderId] = ' + CAST(@Start_OrderId AS varchar(36));

begin tran t1
insert into [OrderTaking].[SerializedData_tmp]
	  ([OrderId]
      ,[MarketID]
      ,[OrderProtoBuf]
      ,[LastModification]
      ,[SerializedDataType]
      ,[SerializedProdInfo]
      ,[SerializedOrderViewInput]
      ,[SerializedEncryptedValues]
	  ,[OrderSeqId])
SELECT [OrderId]
      ,[MarketID]
      ,[OrderProtoBuf]
      ,[LastModification]
      ,[SerializedDataType]
      ,[SerializedProdInfo]
      ,[SerializedOrderViewInput]
      ,[SerializedEncryptedValues]
	  ,[OrderSeqId]
  FROM [OrderTaking].[SerializedData]
 WHERE [OrderId] > @Start_OrderId
  ORDER BY [OrderId] 
  OFFSET 0 ROWS FETCH NEXT @SizeOfChunk ROWS ONLY
commit tran t1

SET @Start_OrderId = (SELECT MAX(OrderId) FROM [OrderTaking].[SerializedData_tmp])
END;


Print CONVERT(char(20), GETDATE(), 120) + 'Create index [IX_SerializedData_tmp_OrderId] ON [OrderTaking].[SerializedData_tmp]'
CREATE NONCLUSTERED INDEX [IX_SerializedData_tmp_OrderId] ON [OrderTaking].[SerializedData_tmp]
(
	[OrderId] ASC
)
INCLUDE ( 	[MarketID],
	[OrderProtoBuf],
	[LastModification],
	[SerializedDataType],
	[SerializedProdInfo],
	[SerializedOrderViewInput],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) 
--	ON [FG2]
Print CONVERT(char(20), GETDATE(), 120) + 'Index [IX_SerializedData_OrderId] ON [OrderTaking].[SerializedData_tmp] created'


Print CONVERT(char(20), GETDATE(), 120) + 'Create index [IX_SerializedData_tmp_OrderId_OrderProtoBuf] ON [OrderTaking].[SerializedData_tmp]'
CREATE NONCLUSTERED INDEX [IX_SerializedData_tmp_OrderId_OrderProtoBuf] ON [OrderTaking].[SerializedData_tmp]
(
	[OrderId] ASC
)
INCLUDE ( 	[OrderProtoBuf]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) 
--ON [FG2]
Print CONVERT(char(20), GETDATE(), 120) + 'Index UQ_User_Market_Name ON [OrderTaking].[SerializedData_tmp] created'
