DECLARE @Start_CustomerPaymentMethodID int
		,@SizeOfChunk int = 1000000 -- the number of rows to be copied in one transaction

IF NOT EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[CustomerPaymentMethod_tmp]') AND [type]='U')
Begin
 print 'Create table [Customer].[CustomerPaymentMethod_tmp]'
CREATE TABLE [Customer].[CustomerPaymentMethod_tmp](
	[CustomerPaymentMethodID] [int] IDENTITY(1,1) NOT NULL,
	[PaymentMethod] [nvarchar](128) NULL,
	[CustomerID] [int] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[LastModification] [datetime] NOT NULL,
	[OneTimePayment] [bit] NOT NULL,
	[IsDefault] [bit] NOT NULL,
	[NickName] [nvarchar](128) NULL,
	[PaymentIntegrator] [nvarchar](128) NULL,
	[Expiration] [nvarchar](128) NULL,
	[IsValid] [bit] NOT NULL,
	[CardAlias] [nvarchar](128) NULL,
	[CardExpiration] [nvarchar](128) NULL,
	[CardToken] [nvarchar](128) NULL,
	[CardHolderName] [nvarchar](128) NULL,
	[CVVConfirmed] [bit] NULL,
	[PaymentMode] [nvarchar](128) NOT NULL,
	[AccountID] [nvarchar](128) NULL,
	[SerializedEncryptedValues] [varchar](2000) NULL,
	ExternalPaymentId nvarchar(128) null,
 CONSTRAINT [PK_CustomerPaymentMethod_tmp] PRIMARY KEY CLUSTERED 
(
	[CustomerPaymentMethodID] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [fg1]
)


End

-- find out the last copied row:
SET @Start_CustomerPaymentMethodID = (SELECT ISNULL(MAX(CustomerPaymentMethodID), 0) FROM [Customer].[CustomerPaymentMethod_tmp]);


-- copying rows ordered by [CustomerPaymentMethodID]:
WHILE @Start_CustomerPaymentMethodID < (SELECT MAX(CustomerPaymentMethodID) FROM [Customer].[CustomerPaymentMethod])
BEGIN

Print CONVERT(char(20), GETDATE(), 120) + 'insert into [Customer].[CustomerPaymentMethod_tmp] ' + CAST(@SizeOfChunk AS varchar(11)) + ' rows starting from [CustomerPaymentMethodID] = ' + CAST(@Start_CustomerPaymentMethodID AS varchar(11));
set identity_insert [Customer].[CustomerPaymentMethod_tmp] ON
begin tran t1
insert into [Customer].[CustomerPaymentMethod_tmp]
	  ([CustomerPaymentMethodID]
      ,[PaymentMethod]
      ,[CustomerID]
      ,[IsEnabled]
      ,[LastModification]
      ,[OneTimePayment]
      ,[IsDefault]
      ,[NickName]
      ,[PaymentIntegrator]
      ,[Expiration]
      ,[IsValid]
      ,[CardAlias]
      ,[CardExpiration]
      ,[CardToken]
      ,[CardHolderName]
      ,[CVVConfirmed]
      ,[PaymentMode]
      ,[AccountID]
      ,[SerializedEncryptedValues]
	  ,ExternalPaymentId)
Select [CustomerPaymentMethodID]
      ,[PaymentMethod]
      ,[CustomerID]
      ,[IsEnabled]
      ,[LastModification]
      ,[OneTimePayment]
      ,[IsDefault]
      ,[NickName]
      ,[PaymentIntegrator]
      ,[Expiration]
      ,[IsValid]
      ,[CardAlias]
      ,[CardExpiration]
      ,[CardToken]
      ,[CardHolderName]
      ,[CVVConfirmed]
      ,[PaymentMode]
      ,[AccountID]
      ,[SerializedEncryptedValues]
	  ,ExternalPaymentId
  FROM [Customer].[CustomerPaymentMethod]
 WHERE [CustomerPaymentMethodID] > @Start_CustomerPaymentMethodID
  ORDER BY [CustomerPaymentMethodID] 
  OFFSET 0 ROWS FETCH NEXT @SizeOfChunk ROWS ONLY
commit tran t1
set identity_insert [Customer].[CustomerPaymentMethod_tmp] OFF
SET @Start_CustomerPaymentMethodID = (SELECT MAX(CustomerPaymentMethodID) FROM [Customer].[CustomerPaymentMethod_tmp])
END;



Print CONVERT(char(20), GETDATE(), 120) + 'Create index [IX_CustomerPayment_LastModification_CustomerID] ON [Customer].[CustomerPaymentMethod_tmp]'
CREATE NONCLUSTERED INDEX [IX_CustomerPayment_LastModification_CustomerID] ON [Customer].[CustomerPaymentMethod_tmp]
(
	[LastModification] ASC,
	[CustomerID] ASC
)
INCLUDE ( 	[CustomerPaymentMethodID],
	[PaymentMethod],
	[IsEnabled],
	[OneTimePayment],
	[IsDefault],
	[NickName],
	[PaymentIntegrator],
	[Expiration],
	[IsValid],
	[CardAlias],
	[CardExpiration],
	[CardToken],
	[CardHolderName],
	[CVVConfirmed],
	[PaymentMode],
	[AccountID],
	[SerializedEncryptedValues]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	on customer_index
Print CONVERT(char(20), GETDATE(), 120) + 'Index [IX_CustomerPayment_LastModification_CustomerID] ON [Customer].[CustomerPaymentMethod_tmp] created'


Print CONVERT(char(20), GETDATE(), 120) + 'Create index [IX_CustomerPaymentMethod_CustomerID] ON [Customer].[CustomerPaymentMethod_tmp]'
CREATE NONCLUSTERED INDEX [IX_CustomerPaymentMethod_CustomerID] ON [Customer].[CustomerPaymentMethod_tmp]
(
	[CustomerID] ASC
)
INCLUDE ( 	[CustomerPaymentMethodID],
	[PaymentMethod],
	[IsEnabled],
	[LastModification],
	[OneTimePayment],
	[IsDefault],
	[NickName],
	[PaymentIntegrator],
	[Expiration],
	[IsValid],
	[CardAlias],
	[CardExpiration],
	[CardToken],
	[CardHolderName],
	[CVVConfirmed],
	[PaymentMode],
	[AccountID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	on customer_index
Print CONVERT(char(20), GETDATE(), 120) + 'Index [IX_CustomerPaymentMethod_CustomerID] ON [Customer].[CustomerPaymentMethod_tmp] created'


Print CONVERT(char(20), GETDATE(), 120) + 'Create Index CustomerPaymentMethod_OnetimepaymentIsvalid_INCL1_FLT_NU_NC ON [Customer].[CustomerPaymentMethod_tmp]'
	CREATE NONCLUSTERED INDEX [CustomerPaymentMethod_OnetimepaymentIsvalid_INCL1_FLT_NU_NC] ON [Customer].[CustomerPaymentMethod_tmp]
	(
		[OneTimePayment] ASC,
		[IsValid] ASC
	)
	INCLUDE ( 	[CustomerPaymentMethodID]) 
	WHERE ([OneTimePayment]=(1) AND [IsValid]=(0))
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--	on customer_index
Print CONVERT(char(20), GETDATE(), 120) + 'Index CustomerPaymentMethod_OnetimepaymentIsvalid_INCL1_FLT_NU_NC ON [Customer].[CustomerPaymentMethod_tmp] created'