DECLARE @Start_CustomerID int
		,@SizeOfChunk int = 1000000 -- the number of rows to be copied in one transaction

IF NOT EXISTS(SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[Customer].[TINData_tmp]') AND [type]='U')
Begin
 print 'Create table [Customer].[TINData_tmp]'
CREATE TABLE [Customer].[TINData_tmp](
	[CustomerID] [int] NOT NULL,
	[TINNumber] [nvarchar](128) NULL,
	[TINFirstName] [nvarchar](128) NULL,
	[TINLastName] [nvarchar](128) NULL,
	[TINAddress] [nvarchar](1000) NULL,
	[TINPostalCode] [nvarchar](32) NULL,
	[LastModification] [datetime] NOT NULL,
	[SerializedEncryptedValues] [varchar](2000) NULL,
 CONSTRAINT [PK_TINData_tmp] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90)
)


End

-- find out the last copied row:
SET @Start_CustomerID = (SELECT ISNULL(MAX(CustomerID), 0) FROM [Customer].[TINData_tmp]);


-- copying rows ordered by [CustomerID]:
WHILE @Start_CustomerID < (SELECT MAX(CustomerID) FROM [Customer].[TINData])
BEGIN

Print CONVERT(char(20), GETDATE(), 120) + 'insert into [Customer].[TINData_tmp] ' + CAST(@SizeOfChunk AS varchar(11)) + ' rows starting from [CustomerID] = ' + CAST(@Start_CustomerID AS varchar(11));
set identity_insert [Customer].[TINData_tmp] ON
begin tran t1
INSERT INTO [Customer].[TINData_tmp]
           ([CustomerID]
           ,[TINNumber]
           ,[TINFirstName]
           ,[TINLastName]
           ,[TINAddress]
           ,[TINPostalCode]
           ,[LastModification]
           ,[SerializedEncryptedValues])
select [CustomerID]
           ,[TINNumber]
           ,[TINFirstName]
           ,[TINLastName]
           ,[TINAddress]
           ,[TINPostalCode]
           ,[LastModification]
           ,[SerializedEncryptedValues]
from Customer.TINData	
 WHERE [CustomerID] > @Start_CustomerID
  ORDER BY [CustomerID] 
  OFFSET 0 ROWS FETCH NEXT @SizeOfChunk ROWS ONLY
commit tran t1
set identity_insert [Customer].[TINData_tmp] OFF
SET @Start_CustomerID = (SELECT MAX(CustomerID) FROM [Customer].[TINData_tmp])
END;

ALTER TABLE [Customer].[TINData_tmp] ADD  CONSTRAINT [DF_TINData_tmp_LastModification]  DEFAULT (getutcdate()) FOR [LastModification]